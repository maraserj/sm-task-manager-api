<?php

namespace App\Listeners;

use App\Events\CreatingUserWidgetsEvent;
use App\Models\Widget;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreatingUserWidgetsListener
{
    /**
     * @var Widget
     */
    private $widget;

    /**
     * Create the event listener.
     *
     * @param Widget $widget
     */
    public function __construct(Widget $widget)
    {
        //
        $this->widget = $widget;
    }

    /**
     * Handle the event.
     *
     * @param  CreatingUserWidgetsEvent  $event
     * @return void
     */
    public function handle(CreatingUserWidgetsEvent $event)
    {

        $widgets = $this->widget->all();


        foreach($widgets as $widget){
            $event->user->widgets()->create([
//                'user_id' => auth()->user(),
                'widget_id' => $widget->id,
                'selected' => 0
            ]);
        }
    }
}
