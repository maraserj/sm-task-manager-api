<?php

namespace App\Repositories;

use App\Models\Calendar;
use App\Models\CalendarEvent;
use Carbon\Carbon;

class CalendarEventsRepository extends BaseRepository
{
    protected $_fillable = [];

    /**
     * EventsRepository constructor.
     *
     * @param CalendarEvent $model
     */
    public function __construct(CalendarEvent $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->model->where('user_id', auth()->id())->get();
    }

    /**
     * @param $id
     * @param $calendarId
     * @param $params
     *
     * @return mixed
     */
    public function getUserWorkTime($id, $calendarId, $params)
    {
        $whereParams = [
            ['user_id', $id],
            ['is_for_task', false],
        ];

        if (isset($params['date'])) {
            $whereParams[] = ['date', '=', Carbon::parse($params['date'])->format('Y-m-d')];
        }

        if (isset($params['month'])) {
            $year = isset($params['year']) ? $params['year'] : Carbon::now()->year;
            $carbon = Carbon::create($year, $params['month']);
            $firstMonthDay = $carbon->firstOfMonth()->format('Y-m-d');
            $lastMonthDay = $carbon->lastOfMonth()->format('Y-m-d');
            $whereParams[] = ['date', '>=', $firstMonthDay];
            $whereParams[] = ['date', '<=', $lastMonthDay];
        }

        return $this->model->where($whereParams)->with(['calendar' => function ($query) use ($calendarId) {
            $query->where('isMain', $calendarId ?? 1);
        }])->get();
    }

    /**
     * @param $id
     * @param $calendarId
     *
     * @return mixed
     */
    public function getUserWorkTimeCount($id, $calendarId)
    {
        $params = [
            ['user_id', $id],
            ['is_for_task', false],
        ];

        if (isset($calendarId)) {
            $params[] = ['calendar_id', $calendarId];
        }

        return $this->model->where($params)->with(['calendar' => function ($query) {
            $query->where('isMain', 1);
        }])->pluck('hours')->sum();
    }

    /**
     * @param $id
     * @param $calendar_id
     * @param $params
     *
     * @return mixed
     */
    public function getUserTasksEvents($id, $calendar_id, $params)
    {
        $whereParams = [
            ['user_id', $id],
            ['is_for_task', true],
            ['calendar_id', $calendar_id],
        ];
        if (isset($params['month'])) {
            $year = isset($params['year']) ? $params['year'] : Carbon::now()->year;
            $carbon = Carbon::create($year, $params['month']);
            $firstMonthDay = $carbon->firstOfMonth()->format('Y-m-d');
            $lastMonthDay = $carbon->lastOfMonth()->format('Y-m-d');
            $whereParams[] = ['date', '>=', $firstMonthDay];
            $whereParams[] = ['date', '<=', $lastMonthDay];
        }

        return $this->model->where($whereParams)->has('tasks')->with(['tasks', 'tasks.project'])->get();
    }

    /**
     * @param $id
     * @param $calendar_id
     * @param $params
     *
     * @return mixed
     */
    public function getFilteredUserTasksEvents($id, $calendar_id, $params)
    {
        $filterEvent = [];
        $filterEvent[] = ['user_id', $id];
        $filterEvent[] = ['is_for_task', true];
        $filterEvent[] = ['calendar_id', $calendar_id];

        if (isset($params['date_to'])) {
            $filterEvent[] = ['date', '<=', Carbon::parse($params['date_to'])];
        }

        return $this->model->where($filterEvent)->whereHas('tasks', function ($query) use ($params) {
            if (isset($params['project'])) {
                $query->where('project_id', $params['project']);
            }
            if (isset($params['activity'])) {
                $query->where('activity_id', $params['activity']);
            }
        })->with(['tasks'])->get();
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'hours'       => 'required|numeric',
            'calendar_id' => 'required|numeric',
        ]);

        if ($validator->fails()) return $validator;
        else return null;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'hours'       => 'required|numeric',
            'calendar_id' => 'required|numeric',
        ]);

        if ($validator->fails()) return $validator;
        else return null;
    }

    /**
     * @param array $data
     *
     * @return BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        isset($data['user_id']) ?: $data['user_id'] = auth()->id();
        isset($data['available_hours']) ?: $data['available_hours'] = '{}';

        return parent::create(array_only($data, $this->_fillable));
    }

    public function createFromGoogle(array $data = [])
    {
        $data['holiday'] = true;
        $calendar = Calendar::getMainCalendar(auth()->id());
        $createData = [
            'user_id'         => auth()->id(),
            'calendar_id'     => $calendar->id,
            'date'            => Carbon::parse($data['startDateTime'])->format('Y-m-d'),
            'is_free'         => false,
        ];

        $event = $this->model->where($createData)->first();

        if (!$event) {
            $createData['available_hours'] = json_encode($data);
            $event = $this->model->create($createData);
        } else {
            $event->update([
                'is_free'         => false,
                'hours'           => $event->hours,
                'available_hours' => json_encode($data),
            ]);
        }

        return $event;
    }

    public function clearExistingEvents(array $events = [])
    {
        $model = $this->model->whereIn('available_hours->id', collect($events['data'])->pluck('id'))->first();
        var_dump(collect($events['data'])->pluck('id')[0]);
        dd(collect($events['data'])->pluck('id'));

        return $model;
    }

    /**
     * @param array $query
     *
     * @return mixed
     */
    public function where(array $query)
    {
        return $this->model->where($query);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        isset($data['user_id']) ?: $data['user_id'] = auth()->id();
        isset($data['available_hours']) ?: $data['available_hours'] = '{}';
        isset($data['dateStart']) ? $data['date'] = $data['dateStart'] : false;

        return parent::update($id, array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id): bool
    {
        $event = parent::find($id);
        if ($event && $event->delete())
            return true;
        else
            return false;
    }

}