<?php

namespace App\Http\Requests\CalendarEvent;

use App\Http\Requests\Request;

class StoreCalendarEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hours'       => 'required|numeric',
            'calendar_id' => 'required|numeric',
        ];
    }
}
