<?php

namespace App\Gateways;

use App\Repositories\CalendarEventsRepository;
use App\Transformers\GoogleCalendarTransformer;
use Carbon\Carbon;
use \Spatie\GoogleCalendar\Event as GoogleEvent;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\ {
    Collection as FractalCollection,
    Item as FractalItem
};
class GoogleCalendarGateway
{
    protected $repository;
    protected $fractal;
    protected $fractalItem;
    protected $fractalCollection;
    protected $transformer;

    public function __construct(CalendarEventsRepository $repository,
                                GoogleCalendarTransformer $transformer,
                                FractalCollection $fractalCollection,
                                FractalItem $fractalItem)
    {
        $this->repository = $repository;
        $this->fractalCollection = $fractalCollection;
        $this->transformer = $transformer;
        $this->fractalItem = $fractalItem;
        $this->fractal = new FractalManager();
    }

    public function getGoogleEvents($startDateTime = null, $stopDateTime = null,$calendarId)
    {
        $events = null;
        try {
            $events = GoogleEvent::get($startDateTime, $stopDateTime, [], $calendarId);
        } catch (\Exception $e) {
            $response = null;
        }


        if (!empty($events)) {
            $resource = new $this->fractalCollection($events, new $this->transformer);
            $response = $this->fractal->createData($resource)->toArray();
//            $response = $this->repository->clearExistingEvents($response);
        } else {
            $response = null;
        }


        return $response;
    }

    public function makeDayAsHoliday($calendarId, $eventId)
    {
        $response = null;
        try {
            $event = GoogleEvent::find($eventId, $calendarId);
            $dateStart = Carbon::parse($event->start && isset($event->start['dateTime']) ? $event->start['dateTime'] : $event->start->date)->format('Y-m-d');
            $dateEnd = Carbon::parse($event->end && isset($event->end['dateTime']) ? $event->end['dateTime'] : $event->end->date)->format('Y-m-d');
            $eventData['id'] = $event->id;
            $eventData['name'] = $event->name;
            $eventData['startDateTime'] = $dateStart;
            $eventData['endDateTime'] = $dateEnd;
            $eventData['isFree'] = false;
            $response = $this->repository->createFromGoogle($eventData);
        } catch (\Exception $e) {
            $response = $e->getMessage();
        }

        return $response;
    }

    public function create(array $data = [])
    {

    }

    public function show(int $id)
    {

    }

    public function update(array $data = [])
    {

    }

    public function delete(int $id)
    {

    }

}