<?php

namespace App\Repositories;

use App\Models\Project;
use File;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;


class FilesRepository extends BaseRepository
{
    public $avatarPath = '/images/avatars/';
    public $postImagesPath = '/images/posts/';
    public $blogPath = '/images/blog/';
    public $attachmentsPath = '/attachments';

    /**
     * @param     $email
     * @param     $image
     * @param int $width
     *
     * @return bool|string
     */
    public function uploadUserAvatar($email, $image, $width = 300)
    {
        $imageName = md5($email) . '_' . str_random(6) . '.jpg';
        $directory = Storage::exists('/public' . $this->avatarPath);
        if (!$directory) Storage::makeDirectory('/public' . $this->avatarPath);
        $path = $this->avatarPath . $imageName;
        $oldFile = Storage::exists('/public' . $path);
        if ($oldFile) Storage::delete('/public' . $path);
        $image = Image::make($image)->widen($width)->save(storage_path('/app/public' . $path));

        if ($image) return '/storage' . $path;
        else return false;
    }

    /**
     * @param array $images
     * @param       $post
     * @param int   $width
     *
     * @return void
     */
    public function uploadPhotos(array $images, $post, $width = 800): void
    {
        foreach ($images as $image) {
            $imageName = md5($post->id) . str_random(8) . '.jpg';
            $dirPath = public_path($this->postImagesPath . $post->id . '/');

            $directory = File::exists($dirPath);
            if (!$directory) File::makeDirectory($dirPath, 0700, true);

            $imgPath = $this->postImagesPath . $post->id . '/' . $imageName;
            $oldFile = File::exists($dirPath . $imageName);
            if ($oldFile) File::delete($dirPath . $imageName);
            Image::make($image)->widen($width)->save($dirPath . $imageName);
        }

    }

    public function uploadBlogImage($image, $width, $postId)
    {
        $imageName = str_replace($image->getClientOriginalExtension(), '', $image->getClientOriginalName());
        $directory = File::exists(public_path() . $this->blogPath);
        if (!$directory) File::makeDirectory(public_path() . $this->blogPath, 0700, true);
        $path = public_path() . $this->blogPath . $imageName . '_' . $postId . '.jpg';
        $oldFile = File::exists($path);
        if ($oldFile) File::delete($path);
        $image = Image::make($image)->widen($width)->save($path);
        $path = $this->blogPath . $imageName . '_' . $postId . '.jpg';

        if ($image) return $path;
        else return false;
    }


    public function uploadCommentAttachments(array $attachments = [], int $commentId = null)
    {
        $allAttachments = [];
        foreach ($attachments as $attachment) {
            $fileName = $attachment->getClientOriginalName();
            $directory = Storage::exists('/public' . $this->attachmentsPath);
            if (!$directory) Storage::makeDirectory('/public' . $this->attachmentsPath);

            $path = $this->attachmentsPath . $commentId . '/' . $fileName;

            Storage::putFileAs('/public' . $this->attachmentsPath . $commentId, $attachment, $fileName);

            if (Storage::exists('/public' . $this->attachmentsPath . $commentId . '/' . $fileName)) {
                $allAttachments[] = [
                    'path'     => '/storage' . $path,
                    'filename' => $fileName,
                ];
            }
        }

        return $allAttachments;
    }

    public function getAttachments($id = null, $attachmentType = 'project')
    {
        $this->attachmentsPath = $this->attachmentsPath . '/' . $attachmentType . '/';
        if (!empty($id)) {
            $attachments = Storage::disk('public')->exists($this->attachmentsPath . $id);
            $collection = collect(Storage::disk('public')->files($this->attachmentsPath . $id))->map(function ($item, $key) {
                return '/storage/'.$item;
            });
            return $attachments ? $collection : null;
        } else {
            return null;
        }
    }


    public function uploadAttachments($attachment, $modelId, $target = null)
    {
        $allAttachments = [];
        if ($target && $target == 'project') $this->attachmentsPath = $this->attachmentsPath . '/project/';
        else $this->attachmentsPath = $this->attachmentsPath . '/task/';

        $fileName = $attachment->getClientOriginalName();
        $mime = $attachment->getMimeType();
        $directory = Storage::exists('/public' . $this->attachmentsPath);
        if (!$directory) Storage::makeDirectory('/public' . $this->attachmentsPath);

        $path = $this->attachmentsPath . $modelId . '/' . $fileName;

        Storage::putFileAs('/public' . $this->attachmentsPath . $modelId, $attachment, $fileName);

        if (Storage::exists('/public' . $this->attachmentsPath . $modelId . '/' . $fileName)) {
            $allAttachments[] = [
                'path'          => '/storage' . $path,
                'original_name' => $fileName,
                'mime'          => $mime,
            ];
        }


        return $allAttachments;
    }

    public function deleteFile($path)
    {
        if ($path) {
            return Storage::disk('public')->delete(str_replace('/storage', '', $path));
        } else {
            return null;
        }
    }

    public function deleteAllCommentAttachments(int $commentId = null)
    {
        if ($commentId)
            Storage::deleteDirectory('/public' . $this->attachmentsPath . $commentId);
    }

    public function deleteAllAttachments(int $modelId = null)
    {
        if ($modelId)
            Storage::deleteDirectory('/public' . $this->attachmentsPath . $modelId);
    }


}