<?php

namespace App\Http\Requests\Direction;

use App\Http\Requests\Request;
use App\Models\Direction;

class UpdateDirectionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $direction = Direction::find($this->route('direction'));

        return $direction && $this->user()->can('update', $direction);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|max:100',
            'activity_id' => 'required|numeric',
        ];
    }
}
