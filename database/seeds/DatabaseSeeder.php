<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use \App\Models\UserInfo;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'role' => 'admin',
            'name' => 'admin',
            'email' => 'admin@local.com',
            'password' => bcrypt('qwerty'),
        ]);
//        UserInfo::firstOrCreate(['user_id' => $user->id]);
//
//        $calendar = \App\Models\CalendarType::create(['name' => 'Рабочий календарь', 'slug' => str_slug('Рабочий календарь')]);

//        factory(User::class, 50)->create();
    }
}
