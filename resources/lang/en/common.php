<?php

return [
    'errors' => [
        '404' => 'Not found',
        '401_bad_permissions' => 'Unauthorized',
        'bad_request' => 'Bad request',
        '500' => 'Internal server error',
        'permission_denied' => 'Permission denied',
    ],

    'activities' => [
        'created' => 'Activity created',
        'not_created' => 'Activity not created',
        'updated' => 'Activity updated',
        'not_updated' => 'Activity not updated',
        'deleted' => 'Activity deleted',
        'not_deleted' => 'Activity not deleted',
    ],

    'calendars' => [
        'created' => 'Calendar created',
        'not_created' => 'Calendar not created',
        'updated' => 'Calendar updated',
        'not_updated' => 'Calendar not updated',
        'deleted' => 'Calendar deleted',
        'not_deleted' => 'Calendar not deleted',
        'locked' => 'You can not delete the main calendar',
    ],


    'type_calendars' => [
        'created' => 'Type calendar created',
        'not_created' => 'Type calendar not created',
        'updated' => 'Type calendar updated',
        'not_updated' => 'Type calendar not updated',
        'deleted' => 'Type calendar deleted',
        'not_deleted' => 'Type calendar not deleted',
    ],

    'directions' => [
        'created' => 'Direction created',
        'not_created' => 'Direction not created',
        'updated' => 'Direction updated',
        'not_updated' => 'Direction not updated',
        'deleted' => 'Direction deleted',
        'not_deleted' => 'Direction not deleted',
    ],

    'events' => [
        'created' => 'Event created',
        'not_created' => 'Event not created',
        'updated' => 'Event updated',
        'not_updated' => 'Event not updated',
        'deleted' => 'Event deleted',
        'not_deleted' => 'Event not deleted',
    ],

    'projects' => [
        'created' => 'Project created',
        'not_created' => 'Project not created',
        'updated' => 'Project updated',
        'not_updated' => 'Project not updated',
        'deleted' => 'Project deleted',
        'not_deleted' => 'Project not deleted',
    ],

    'projects_members' => [
        'created' => 'Member success added to project',
        'not_created' => 'Member not added to project',
        'deleted' => 'Member success deleted from project',
        'not_deleted' => 'Member not deleted from project',
    ],

    'tasks' => [
        'created' => 'Task created',
        'not_created' => 'Task not created',
        'updated' => 'Task updated',
        'not_updated' => 'Task not updated',
        'deleted' => 'Task deleted',
        'not_deleted' => 'Task not deleted',
    ],

    'users' => [
        'created' => 'User created',
        'not_created' => 'User not created',
        'updated' => 'User updated',
        'not_updated' => 'User not updated',
        'deleted' => 'User deleted',
        'not_deleted' => 'User not deleted',
    ],
];