<?php
namespace App\Repositories;

use App\Events\TaskEventsNeedsRefresh;
use App\Models\CalendarEvent;
use App\Models\Direction;
use App\Models\Project;
use App\Models\ProjectMember;
use App\Models\Task;
use App\Models\User;
use Carbon\Carbon;

class ProjectsRepository extends BaseRepository
{
    protected $_fillable = [];
    protected $filterCollection;

    const FILTER_STATUSES = [
        'open' => 'open',
        'hold' => 'hold',
        'resolved' => 'resolved',
        'in_progress' => 'in_progress',
        'ready_to_test' => 'ready_to_test',
        'closed' => 'closed',
    ];
    const FILTER_PRIORITIES = [
        1,// Low
        2,// Normal
        3,// High
    ];

    /**
     * ProjectsRepository constructor.
     * @param Project $task
     */
    public function __construct(Project $task)
    {
        $this->_fillable = $task->getFillable();
        parent::__construct($task);
    }

    public function getWithNearDeadline($limit = 10)
    {
        $deadline = Carbon::now()->addWeeks(2);
        $today = Carbon::today();
        $statuses = [
            self::FILTER_STATUSES['open'],
            self::FILTER_STATUSES['hold'],
            self::FILTER_STATUSES['resolved'],
            self::FILTER_STATUSES['in_progress']
        ];
        $projects = $this->model->where('parent_id', 0)
            ->withCount('tasks')
            ->with(['tasks', 'tasks.author', 'children', 'members', 'lead'])
            ->where([['deadline', '<', $deadline], ['deadline', '>', $today], ['project_lead', '=', auth()->id()]])
            ->whereIn('status', $statuses)
//            ->whereHas('members', function ($query) {
//                $query->where('user_id', '=', auth()->id());
//            })
            ->orderBy('deadline')->limit($limit)->get();

        return $projects;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        $members = $this->model->where('parent_id', 0)
            ->withCount('tasks')
            ->with(['tasks', 'tasks.author', 'children'])
            ->whereHas('members', function ($query) {
                $query->where('user_id', '=', auth()->id());
            })->get();

        return $members;
    }

    private function sortProjects($data): array
    {
        $orders = [];
        $this->_fillable[] = 'id';
        foreach ($data as $key => $param) {
            if (in_array($key, $this->_fillable) && isset($param)) {
                $orders[] = [$key, $param];
            }
        }
        if (empty($orders)) {
            $orders[] = ['created_at', 'asc'];
        }

        return $orders;
    }

    public function getChildren($data, $id)
    {
        $project = $this->model->find($id);

        if ($project) {
            return $project->children()->with(['tasks'])->paginate($data['perPage'] ?? 10);
        } else {
            return null;
        }
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function getSortedProjects(array $data = [])
    {
        $filterParams = $data['filter'] ?? [];
        $filterParams['perPage'] = $data['perPage'] ?? 10;

        $filter = [];
        $filter[] = ['parent_id', '=', 0];
        $this->filterCollection = $this->model->withCount('tasks')->with(['tasks', 'tasks.author', 'lead'])->whereHas('members', function ($query) {
            $query->where('user_id', '=', auth()->id());
        });
        if (isset($filterParams)) {

            if (isset($filterParams['status']) && $filterParams['status'] != '') {
                if (key_exists($filterParams['status'], self::FILTER_STATUSES)) {
                    $filter[] = ['status', '=', $filterParams['status']];
                }
            }
            if (isset($filterParams['priority']) && $filterParams['priority'] != '') {
                if (key_exists($filterParams['priority'], self::FILTER_PRIORITIES)) {
                    $filter[] = ['priority', '=', $filterParams['priority']];
                }
            }
        }
        $this->filterCollection->where($filter);

        if (isset($data['sort'])) {
            $sorted = $this->sortProjects($data['sort']);
            foreach ($sorted as $sort) {
                $this->filterCollection->orderBy($sort[0], $sort[1]);
            }
        }


        return $this->filterCollection->paginate($filterParams['perPage']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findWithMembers($id)
    {
        return $this->model->where('id', $id)
            ->with(['members', 'comments', 'tasks', 'tasks.project', 'tasks.author', 'children', 'children.tasks'])
            ->withCount(['tasks', 'comments', 'children'])
            ->get();
    }

    /**
     * @param $id
     * @return Project|null
     */
    public function getProjectMembers($id)
    {
        $project = $this->model->find($id);

        if ($project)
            return $project->members;
        else
            return null;
    }

    /**
     * @param $id
     * @return User|null
     */
    public function getProjectMember($id)
    {
        $projectMember = ProjectMember::find($id);
        if ($projectMember) {
            $member = User::find($projectMember->user_id);
            if ($member)
                return $member;
            else
                return null;
        } else
            return null;
    }

    /**
     * @param $userId
     * @param $projectId
     *
     * @return ProjectMember|null|static
     */
    public function getProjectMemberByUserId($userId, $projectId)
    {
        $member = User::find($userId);
        if ($member && $projectId) {
            $projectMember = ProjectMember::where([['user_id', '=', $userId], ['project_id', '=', $projectId]])->first();
            if ($projectMember) {
                return $projectMember;
            } else {
                return null;
            }
        } else
            return null;
    }

    /**
     * @param array $data
     * @return ProjectMember|ProjectsRepository|bool
     */
    public function createProjectMember(array $data = [])
    {
        $member = ProjectMember::create($data);
        if ($member)
            return $member;
        else
            return false;
    }

    /**
     * @param $id
     * @param array $data
     * @return ProjectMember|bool
     */
    public function updateProjectMember($id, array $data = [])
    {
        $data['user_id'] = $data['user_id'] ?? auth()->id();
        $member = ProjectMember::find($id);
        if ($member->update($data))
            return $member;
        else
            return false;
    }

    /**
     * @param $data
     * @return bool
     */
    public function deleteProjectMember($data): bool
    {
        $member = ProjectMember::where($data)->first();
        if ($member && $member->delete())
            return true;
        else
            return false;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, $this->model->getCreateRules());

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $lastDayUserWorkTime = CalendarEvent::latestUserEventDate();
        $this->model->setUpdateRules(['deadline' => 'before:'.$lastDayUserWorkTime]);
        $validator = validator($data, $this->model->getUpdateRules());

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    public function setUpdateRules($rules)
    {
        return $this->model->setUpdateRules($rules);
    }

    public function getUpdateRules()
    {
        return $this->model->getUpdateRules();
    }

    public function setCreateRules($rules)
    {
        return $this->model->setCreateRules($rules);
    }

    public function getCreateRules()
    {
        return $this->model->getUpdateRules();
    }

    public function getLastUserEvent()
    {
        return CalendarEvent::latestUserEventDate();
    }

    /**
     * @param array $data
     * @return BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        if (isset($data['parent_id'])) {
            $parentProject = $this->model->find($data['parent_id']);
            $data['activity_id'] = $parentProject->activity_id;
            $data['direction_id'] = $parentProject->direction_id;

        } else {
            $activity_id = Direction::find($data['direction_id'])->activity_id;
            $data['activity_id'] = $activity_id;
        }

        $data['project_lead'] = $data['project_lead'] ?? auth()->id();
        $data['priority'] = $data['priority'] ?? 0;
        if (!isset($data['parent_id']) || $data['parent_id'] == '') {
            $data['parent_id'] = 0;
        }
        $project = parent::create(array_only($data, $this->_fillable));
        $project->members()->attach($project->id, ['user_id' => $data['project_lead'], 'project_id' => $project->id]);

        return $project;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        if (isset($data['direction_id'])) {
            $activity_id = Direction::find($data['direction_id'])->activity_id;
            $data['activity_id'] = $activity_id;
        }
        $data['project_lead'] = $data['project_lead'] ?? auth()->id();
        $data['priority'] = $data['priority'] ?? 0;
        if (!isset($data['parent_id']) || $data['parent_id'] == '') {
            $data['parent_id'] = 0;
        }
        $project = $this->find($id);

        if ($project && $project->status != self::FILTER_STATUSES['in_progress'] && isset($data['status']) && $data['status'] == self::FILTER_STATUSES['in_progress']) {
            $data['start_at'] = Carbon::now();
        } else if ($project && $project->status != self::FILTER_STATUSES['closed'] && isset($data['status']) && $data['status'] == self::FILTER_STATUSES['closed']) {
            $data['end_at'] = Carbon::now();
        }

        return parent::update($id, array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        $project = parent::find($id);
        if ($project){
            if($project->children()) {
                $project->children()->delete();
            }

            $project->delete();
            return true;
        } else
            return false;
    }
}