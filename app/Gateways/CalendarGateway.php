<?php

namespace App\Gateways;

use App\Models\CalendarEvent;
use App\Repositories\{ CalendarEventsRepository, CalendarsRepository };

class CalendarGateway extends Gateway
{
    protected $repository;

    /**
     * CalendarGateway constructor.
     *
     * @param CalendarsRepository $repository
     */
    public function __construct(CalendarsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $data = $this->repository->all();

        //TODO need to refactor
        $calendarEventsRepository = new CalendarEventsRepository(new CalendarEvent);
        foreach ($data as $calendar) {
            $calendar->hours = $calendarEventsRepository->getUserWorkTimeCount(auth()->id(), $calendar->id);
        }

        return $data;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        $calendar = $this->repository->create($data);

        return $calendar;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        $calendar = $this->repository->find($id);

        return $calendar;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update(array $data = [])
    {
        $calendar = $this->repository->update($data['id'], $data['request']);

        return $calendar;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name'    => 'required|max:100',
            'type_id' => 'required',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name'    => 'required|max:100',
            'type_id' => 'required',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }
}