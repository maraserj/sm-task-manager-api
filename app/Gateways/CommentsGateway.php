<?php

namespace App\Gateways;

use App\Repositories\CommentsRepository;

class CommentsGateway
{
    protected $repository;

    public function __construct(CommentsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index($limit = 8, $user = null)
    {
        $comments = $this->repository->allUsersComments($limit, $user);

        return $comments;
    }

    public function create(array $data = [])
    {
        $comment = $this->repository->create($data);

        return $comment;
    }

    public function show(int $id)
    {
        $comment = $this->repository->find($id);

        return $comment;
    }

    public function update(array $data = [])
    {
        $comment = $this->repository->update($data['id'], $data['request']);

        return $comment;
    }

    public function delete(int $id)
    {
        $comment = $this->repository->delete($id);

        if (!$comment)
            return false;
        else
            return true;
    }

    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, $this->repository->getCreateRules());

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    public function validateOnUpdate(int $id = null, array $data = [])
    {
        $validator = validator($data, $this->repository->getUpdateRules());

        if ($validator->fails())
            return $validator;
        else
            return null;
    }


}