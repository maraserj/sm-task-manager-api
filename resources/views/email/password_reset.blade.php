@extends('email.layouts.main')
@section('content')
    <td style="{{ $fontFamily }} {{ $style['email-body_cell'] }}">
        <!-- Greeting -->
        <h1 style="{{ $style['header-1'] }}">Hi, {{ $data['name'] }}!</h1>
        <h2 style="{{ $style['header-2'] }}">Thanks for leaving the order.</h2>
        <br>
        <p>We will contact you soon!</p>
    </td>
@stop