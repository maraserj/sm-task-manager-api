<?php

namespace App\Gateways;

use App\Repositories\{ CalendarEventsRepository, CalendarsRepository };
use App\Transformers\CalendarEventsTransformer;
use Illuminate\Http\JsonResponse;
use League\Fractal\Manager;

use League\Fractal\Resource\{
    Collection as FractalCollection,
    Item as FractalItem
};

class CalendarEventGateway extends Gateway
{
    protected $repository;
    protected $calendar;
    protected $filesRepository;
    protected $transformer;
    protected $fractal;
    protected $fractalCollection;
    protected $fractalItem;

    /**
     * CalendarEventGateway constructor.
     *
     * @param CalendarEventsRepository  $repository
     * @param CalendarsRepository       $calendar
     * @param CalendarEventsTransformer $transformer
     * @param FractalCollection         $fractalCollection
     * @param FractalItem               $fractalItem
     */
    public function __construct(CalendarEventsRepository $repository, CalendarsRepository $calendar, CalendarEventsTransformer $transformer, FractalCollection $fractalCollection, FractalItem $fractalItem)
    {
        $this->repository = $repository;
        $this->calendar = $calendar;
        $this->fractalCollection = $fractalCollection;
        $this->transformer = $transformer;
        $this->fractalItem = $fractalItem;
        $this->fractal = new Manager();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->repository->all();
    }

    /**
     * @param $userId
     * @param $calendarId
     * @param $request
     *
     * @return mixed
     */
    public function getWorkTime($userId, $calendarId, $request)
    {
        if (! $calendarId) {
            $main = $this->calendar->getMain();
            if (!$main) {
                return null;
            }
            $calendarId = $main->id;
        }
        if (isset($request['get_for_count'])) {
            $data = $this->repository->getUserWorkTimeCount($userId, $calendarId);
        } else {
            $data = $this->repository->getUserWorkTime($userId, $calendarId, $request);
        }

        return $data;
    }

    /**
     * @param       $id
     *
     * @param array $data
     *
     * @return null
     */
    public function getUserTasksEvents($id, array $data = [])
    {
        $userId = auth()->id();
        if (!$id) {
            $main = $this->calendar->getMain();
            if (!$main) {
                return null;
            }
            $id = $main->id;
        }

        if (isset($data['filter'])) {
            $data = $this->repository->getFilteredUserTasksEvents($userId, $id, $data['filter']);
        } else {
            $data = $this->repository->getUserTasksEvents($userId, $id, $data);
        }

        return $data;


//        $tasks = $this->repository->getUserTasksEvents(auth()->id());
//        $data = new $this->fractalCollection($tasks, new $this->transformer);
//        $this->fractal->parseIncludes(['tasks']);
//
//        if (!$data)
//            return $this->respondNotFound();
//        else
//            return $this->respondOk($this->fractal->createData($data)->toArray());
    }


    /**
     * @param array $data
     *
     * @return \App\Repositories\BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        $calendarEvent = $this->repository->create($data);

        return $calendarEvent;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        $calendarEvent = $this->repository->find($id);

        return $calendarEvent;
    }

    /**
     * @param array $data
     *
     * @return JsonResponse
     */
    public function update(array $data = [])
    {
        $calendarEvent = $this->repository->update($data['id'], $data['request']);

        return $calendarEvent;
    }

    public function delete(int $id)
    {
        if ($this->repository->delete($id))
            return true;
        else
            return false;

    }

}