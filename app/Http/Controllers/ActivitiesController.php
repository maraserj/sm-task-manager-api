<?php

namespace App\Http\Controllers;

use App\Events\RefreshTasksRanks;
use App\Gateways\ActivityGateway;
use App\Http\Requests\Activity\{
    StoreActivityRequest, UpdateActivityRequest
};
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class ActivitiesController extends Controller
{
    protected $gateway;

    /**
     * ActivitiesController constructor.
     *
     * @param ActivityGateway $gateway
     */
    public function __construct(ActivityGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->respondOK($this->gateway->getAll());
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirections($id): JsonResponse
    {
        $directions = $this->gateway->getDirections($id);

        if (!$directions)
            return $this->respondNotFound();
        else
            return $this->respondOK($directions);
    }

    /**
     * @param StoreActivityRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreActivityRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $activity = $this->gateway->create($data);

        if ($activity)
            return $this->respondCreated($activity, trans('common.activities.created'));
        else
            return $this->respondBadRequest(trans('common.activities.not_created'));
//        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $activity = $this->gateway->show($id);
        $this->authorize('view', $activity);

        if (!$activity)
            return $this->respondNotFound();
        else
            return $this->respondOk($activity);
    }

    /**
     * @param UpdateActivityRequest $request
     * @param                       $id
     *
     * @return JsonResponse
     */
    public function update(UpdateActivityRequest $request, $id): JsonResponse
    {
        $data['request'] = $request->all();
        $data['id'] = $id;

        $activity = $this->gateway->show($id);
        if ($activity && isset($request->priority) && $activity->priority != $request->priority) {
            $tasks = \Auth::user()->tasks;
            event(new RefreshTasksRanks($tasks));
        }

        $activity = $this->gateway->update($data);
        if ($activity)
            return $this->respondOK($activity, trans('common.activities.updated'));
        else
            return $this->respondBadRequest(trans('common.activities.not_updated'));
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $activity = $this->gateway->show($id);
        $this->authorize('delete', $activity);

        if ($this->gateway->delete($id))
            return $this->respondOK(trans('common.activities.deleted'));
        else
            return $this->respondNotFound();
    }
}
