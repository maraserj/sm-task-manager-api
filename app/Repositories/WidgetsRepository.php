<?php
namespace App\Repositories;

use App\Models\Widget;

class WidgetsRepository extends BaseRepository
{
    protected $_fillable;

    public function __construct(Widget $model) {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    public function all() {
        $widgets = $this->model->all();
        return $widgets;
    }
}