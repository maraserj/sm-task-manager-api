<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CalendarType
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarType whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CalendarType extends Model
{
    protected $table = 'calendar_types';

    protected $fillable = [
        'name',
        'slug',
    ];
}
