<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <a href="{{ route('admin.index') }}" class="btn btn-link">Task Manager</a>
        <div class="sidebar-header-controls">
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
            </button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li class="m-t-30 {{ request()->is('admin') ? 'active' : '' }}">
                <a href="{{ route('admin.index') }}" class="detailed">Главная</a>
                <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
            </li>
            <li class="{{ request()->is('admin/users*') ? 'active' : '' }}">
                <a href="{{ route('admin.users.index') }}" class="detailed">Пользователи</a>
                <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
            </li>
            <li class="{{ request()->is('admin/activities*') ? 'active' : '' }}">
                <a href="{{ route('admin.activities.index') }}" class="detailed">Сферы деятельности</a>
                <span class="icon-thumbnail"><i class="pg-mail"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>