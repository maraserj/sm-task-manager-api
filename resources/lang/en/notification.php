<?php

return [
    'email' => [
        'receive_mail' => 'You are receiving this email because we received a password reset request for your account.',
        'reset_password' => 'Reset password',
        'incorrect_email' => 'If you did not request a password reset, no further action is required.'
    ]
];