<?php

namespace App\Http\Controllers;

use App\Events\RefreshTasksRanks;
use App\Events\TaskEventsNeedsRefresh;
use App\Gateways\TaskGateway;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;

class TasksController extends Controller
{
    protected $gateway;
    protected $request;
    private $user;

    /**
     * ProjectsController constructor.
     *
     * @param TaskGateway $gateway
     * @param Request     $request
     */
    public function __construct(TaskGateway $gateway, Request $request)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });
        $this->gateway = $gateway;
        $this->request = $request;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->request->all();

        $tasks = $this->gateway->getAll($data);

        if (!$tasks)
            return $this->respondNotFound();
        else
            return $this->respondOk($tasks);
    }


    /**
     * @param Request $request
     * @param null    $id
     *
     * @return JsonResponse
     */
    public function getTasksEvents(Request $request, $id = null): JsonResponse
    {
        $tasks = $this->gateway->getTasksEvents($id);
        if (!$tasks)
            return $this->respondNotFound();
        else
            return $this->respondOk($tasks);
    }

    /**
     * @param null $id
     *
     * @return JsonResponse
     */
    public function getComments($id = null): JsonResponse
    {

        $comments = $this->gateway->getComments($id);

        if (!$comments)
            return $this->respondNotFound();
        else
            return $this->respondOk($comments);
    }

    /**
     * @param StoreTaskRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreTaskRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);
//        $project = $this->gateway->getProject($data['project_id']);
//        $this->authorize('create', $project);

//        $validator = $this->gateway->validateOnCreate($data);
//        if ($validator && $validator->fails()) {
//            return $this->respondInvalidation($validator->errors());
//        } else {
            $task = $this->gateway->create($data);
            if ($task) {

                if ($task->is_daily != true) {
                    event(new TaskEventsNeedsRefresh($task, $task->project));
                    event(new RefreshTasksRanks($this->user->tasks));
                }

                $task = $this->gateway->show($task->id);

                return $this->respondCreated($task, trans('common.tasks.created'));
            } else {
                return $this->respondBadRequest(trans('common.tasks.not_created'));
            }
//        }
    }

    /**
     * @param UpdateTaskRequest $request
     * @param                   $id
     *
     * @return JsonResponse
     */
    public function update(UpdateTaskRequest $request, $id): JsonResponse
    {
        $data['id'] = $id;
        $data['request'] = $request->except(['_token', '_method']);

//        $task = $this->gateway->find($data['id']);
//        if ($this->user->can('update', $task)) {
//            $validator = $this->gateway->validateOnUpdate($task, $data['request']);
//            if ($validator && $validator->fails()) {
//                return $this->respondInvalidation($validator->errors());
//            } else {
        $task = $this->gateway->update($data);
        if ($task) {
            if ($task->is_daily != true) {
                event(new TaskEventsNeedsRefresh($task, $task->project));
                event(new RefreshTasksRanks($this->user->tasks));
            }
            $task = $this->gateway->show($task->id);

            return $this->respondOK($task, trans('common.tasks.updated'));
        }
        else
            return $this->respondBadRequest(trans('common.tasks.not_updated'));
//            }
//        } else
//            return $this->respondDenied();
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $task = $this->gateway->find($id);
        $this->authorize('view', $task);
        $task = $this->gateway->show($id);

        if (!$task)
            return $this->respondNotFound();
        else
            return $this->respondOk($task);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $task = $this->gateway->find($id);
        $this->authorize('delete', $task);

        if ($this->gateway->delete($id))
            return $this->respondOK(trans('common.tasks.deleted'));
        else
            return $this->respondNotFound();
    }

    public function attachments($taskId)
    {
        $attachments = $this->gateway->getAttachments($taskId);
        if ($attachments) {
            return $this->respondOK($attachments);
        } else {
            return $this->respondNotFound();
        }
    }

    public function deleteAttachment($id, $attachmentId)
    {
        $project = $this->gateway->find($id);

        if ($project && $attachmentId) {
            $deleted = $this->gateway->deleteAttachments($project, $attachmentId);
            if ($deleted) {
                return $this->respondOK();
            } else {
                return $this->respondNotFound();
            }
        } else {
            return $this->respondNotFound();
        }
    }

    public function storeAttachments($id): JsonResponse
    {
        $task = $this->gateway->find($id);
        $this->authorize('view', $task);
        $data = $this->request->all();

        $attachments = $this->gateway->storeAttachments($task, $data);
        if ($attachments) {
            return $this->respondOK($attachments);
        } else {
            return $this->respondBadRequest();
        }
    }

    public function updateRanks(Request $request): JsonResponse
    {
        $data = $request->all();

        $response = $this->gateway->updateRanks($data);

        if ($response) {
            return $this->respondOK($response);
        } else {
            return $this->respondBadRequest();
        }
    }

    public function getTasksStatistics()
    {
        $statistics = $this->gateway->getTasksStatistics();
        return $this->respondOk($statistics);
    }

    public function getTasksStatisticsByActivity()
    {
        $statistics = $this->gateway->getTasksStatisticsByActivity();
        return $this->respondOK($statistics);
    }
}
