<?php

namespace App\Transformers;

use App\Models\Project;
use App\Models\User;
use Carbon\Carbon;
use Fractal;

class UserTransformer extends Transformer
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'projects'
    ];


    public function transform(User $user)
    {
        Carbon::setLocale('ru');
        return [
            'id' => (int) $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'avatar' => $user->avatar,
            'created_at' => $user->created_at->diffForHumans(),
        ];
    }

    public function includeProjects(User $user)
    {
        $members = $user->projects()->with([
            'activity',
            'direction',
            'tasks',
        ])->get();

        return $this->collection($members, new ProjectTransformer);
    }
}