<?php
namespace App\Repositories;

use App\Models\Direction;

class DirectionsRepository extends BaseRepository
{
    protected $_fillable = [];

    const DIRECTIONS = [
        [
            'name' => 'English video lessons',
            'activity_id' => 1
        ],
        [
            'name' => 'Football',
            'activity_id' => 2
        ],
        [
            'name' => 'Rock',
            'activity_id' => 3
        ],
        [
            'name' => 'Salsa',
            'activity_id' => 4
        ],
    ];

    /**
     * DirectionsRepository constructor.
     * @param Direction $model
     */
    public function __construct(Direction $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    /**
     * @return array
     */
    public function getBaseDirections():array
    {
        return self::DIRECTIONS;
    }

    public function all()
    {
        return $this->model->where('user_id', auth()->id())->withCount(['tasks' => function ($query) {
            $query->where('user_id', auth()->id());
        }])->with('activity')->withCount('projects')->get();
    }


    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'activity_id' => 'required|numeric',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'activity_id' => 'required|numeric',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        !isset($data['name']) ?: $data['slug'] = str_slug($data['name']);
        $data['user_id'] = $data['user_id'] ?? auth()->id();

        return parent::create(array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        !isset($data['name']) ?: $data['slug'] = str_slug($data['name']);
        $data['user_id'] = $data['user_id'] ?? auth()->id();

        return parent::update($id, array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        $direction = parent::find($id);
        if ($direction && $direction->delete()) {
            return true;
        } else
            return false;
    }
}