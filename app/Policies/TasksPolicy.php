<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use App\Models\Task;
use Illuminate\Auth\Access\HandlesAuthorization;

class TasksPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Task $task
     *
     * @return boolean
     */
    public function view(User $user, Task $task): bool
    {
        if ($user->can('admin'))
            return true;
        $members = $user->projects()->where('project_id', $task->project_id)->first();
        if ($members || $user->id === $task->author_id || $user->id === $task->assigned_id || $user->id === $task->project->project_lead)
            return true;
        else
            return false;
    }

    /**
     * @param User    $user
     * @param Project $project
     *
     * @return boolean
     */
    public function create(User $user, Project $project): bool
    {
        if ($user->can('admin'))
            return true;
        $members = $user->projects()->where('project_id', $project->id)->first();
        if ($members || $user->id === $project->project_lead)
            return true;
        else
            return false;
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Task $task
     *
     * @return boolean
     */
    public function update(User $user, Task $task): bool
    {
        if ($user->can('admin'))
            return true;
        $members = $user->projects()->where('project_id', $task->project_id)->first();
        if ($members || $user->id === $task->author_id || $user->id === $task->assigned_id || $user->id === $task->project->project_lead)
            return true;
        else
            return false;
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Task $task
     *
     * @return boolean
     */
    public function delete(User $user, Task $task): bool
    {
        if ($user->can('admin'))
            return true;
        $members = $user->projects()->where('project_id', $task->project_id)->first();
        if ($members || $user->id === $task->author_id || $user->id === $task->assigned_id || $user->id === $task->project->project_lead)
            return true;
        else
            return false;
    }
}
