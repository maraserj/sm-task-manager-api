<?php

namespace App\Http\Controllers;

use App\Gateways\CalendarTypesGateway;
use App\Http\Requests\CalendarType\StoreCalendarTypeRequest;
use App\Http\Requests\CalendarType\UpdateCalendarTypeRequest;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CalendarTypesController extends Controller
{
    protected $gateway;

    public function __construct(CalendarTypesGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->gateway->getAll();
        if (!$data)
            return $this->respondNotFound();
        else
            return $this->respondOk($data);
    }

    /**
     * @param StoreCalendarTypeRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCalendarTypeRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $calendarType = $this->gateway->create($data);
        if ($calendarType)
            return $this->respondCreated($calendarType, trans('common.type_calendars.created'));
        else
            return $this->respondBadRequest(trans('common.type_calendars.not_created'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $calendarType = $this->gateway->show($id);
        if (!$calendarType)
            return $this->respondNotFound();
        else
            return $this->respondOk($calendarType);
    }

    /**
     * @param UpdateCalendarTypeRequest $request
     * @param                           $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCalendarTypeRequest $request, $id): JsonResponse
    {
        $data['request'] = $request->all();
        $data['id'] = $id;

        $calendarType = $this->gateway->update($data);
        if ($calendarType)
            return $this->respondOK($calendarType, trans('common.type_calendars.updated'));
        else
            return $this->respondBadRequest(trans('common.type_calendars.not_updated'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $this->authorize('admin');

        if ($this->gateway->delete($id))
            return $this->respondOK(trans('common.type_calendars.deleted'));
        else
            return $this->respondNotFound();
    }
}
