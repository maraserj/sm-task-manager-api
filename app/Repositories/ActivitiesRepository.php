<?php
namespace App\Repositories;

use App\Models\Activity;

class ActivitiesRepository extends BaseRepository
{
    protected $_fillable;

    public function __construct(Activity $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    public function all()
    {
        $activities = $this->model->where('user_id', auth()->id())->with(['user'])->withCount(['tasks', 'directions', 'projects'])->get();

        return $activities;
    }

    public function getDirectionsByActivityId($id)
    {
        $activity = $this->model->find($id);
        if ($activity) {
            $directions = $activity->directions()->where('user_id', auth()->id())->withCount(['tasks' => function ($query) {
                $query->where('user_id', auth()->id());
            }])->with('activity')->get();
        } else {
            $directions = null;
        }

        return $directions;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'calendar_id' => 'required|numeric|not_in:0',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        !isset($data['name']) ?: $data['slug'] = str_slug($data['name']);
        $data['user_id'] = $data['user_id'] ?? auth()->id();

        return parent::create(array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        !isset($data['name']) ?: $data['slug'] = str_slug($data['name']);
        $data['user_id'] = $data['user_id'] ?? auth()->id();

        return parent::update($id, array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        $activity = parent::find($id);
        if ($activity && $activity->delete())
            return true;
        else
            return false;
    }
}