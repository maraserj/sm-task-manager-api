<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Full Screen Single Color Backround</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('ticker/css/normalize.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ticker/css/component.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('ticker/clock/css/clock.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ticker/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ticker/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ticker/css/dark-theme.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <script src="{{ asset('ticker/js/vendor/modernizr-2.6.2.min.js') }}"></script>
</head>
<style>
    body {
        font-family: 'Lato', sans-serif;
    }
</style>
<body class="singlecolor">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="javascript:if(confirm(%27http://browsehappy.com/  \n\nThis file was not retrieved by Teleport Pro, because it is addressed on a domain or path outside the boundaries set for its Starting Address.  \n\nDo you want to open it from the server?%27))window.location=%27http://browsehappy.com/%27">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
{{--<section class="main-menu-container">--}}
{{--<div class="show_toggle"><a href="#"></a></div>--}}
{{--<ul class="main-menu">--}}
{{--<li>--}}
{{--<a href="#" class="home-link">Home</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#" data-page="services">Services</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#" data-page="about">About</a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#" data-page="contacts">Contacts</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</section>--}}


<section class="mainarea">
    <div id="clock" class="active">
        <div class="clock-container">
            <div id="time-container-wrap">
                <div id="time-container">
                    <div class="numbers-container"></div>
                    <span id="ticker" class="clock-label">PM SOFT</span>
                    <span id="timelable" class="clock-label">TIME LEFT</span>
                    <span id="timeleft" class="clock-label">COUNTDOWN</span>
                    <figure id="canvas"></figure>
                </div>
            </div>
        </div>
        <h3>Be right back. </h3>
        {{--<form action="#" class="subscribe" id="subscribe">--}}
        {{--<input type="email" placeholder="Enter your email" class="email form_item requiredField" name="subscribe_email" />--}}
        {{--<input type="submit" class='form_submit' value="subscribe" />--}}
        {{--<div id="form_results"></div>--}}
        {{--</form>--}}
    </div>
    {{--<div class="mainarea-content">--}}
    {{--<div id="services" data-page="services" class="side-page side-left went-left">--}}
    {{--<div class="container">--}}
    {{--<h2 class="title">What we do</h2>--}}
    {{--<ul class="services">--}}
    {{--<li>--}}
    {{--<img src="{{ asset('ticker/img/icons/promotion-icon-dark.png') }}" alt="" />--}}
    {{--<p>--}}
    {{--Promotion--}}
    {{--</p>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<img src="{{ asset('ticker/img/icons/web-desing-icon-dark.png') }}" alt="" />--}}
    {{--<p>--}}
    {{--Web Design--}}
    {{--</p>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<img src="{{ asset('ticker/img/icons/photography-icon-dark.png') }}" alt="" />--}}
    {{--<p>--}}
    {{--Photography--}}
    {{--</p>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--<p>--}}
    {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.--}}
    {{--</p>--}}
    {{--<p>--}}
    {{--Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. <a href="#">Excepteur sint occaecat</a> cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae.--}}
    {{--</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div id="about" data-page="about" class="side-page active">--}}
    {{--<div class="container">--}}
    {{--<h2 class="title">Who we are</h2>--}}
    {{--<p>--}}
    {{--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eget nibh at libero fringilla adipiscing nec ut leo. Etiam nec purus arcu. Morbi sollicitudin at risus id malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam sed tincidunt arcu. Donec molestie ante sapien, sed molestie est euismod eget. Maecenas ac metus accumsan, scelerisque massa sed, porta est. Aliquam ut mollis mi. Cras id vulputate purus, ac sollicitudin ante.--}}
    {{--</p>--}}
    {{--<p>--}}
    {{--Integer condimentum eu lectus quis semper. Sed id metus magna. Morbi ultrices magna id euismod hendrerit. Pellentesque nec mattis odio, vitae laoreet metus. Sed eget sollicitudin est, vitae accumsan nisi. Fusce consequat imperdiet venenatis. Integer mollis hendrerit facilisis. Praesent vel mattis enim. Integer fringilla et urna vitae rutrum.--}}
    {{--</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div id="contacts" data-page="contacts" class="side-page side-right went-right">--}}
    {{--<div class="container">--}}
    {{--<h2 class="title">Get in touch</h2>--}}
    {{--<p>--}}
    {{--Lexington Avenue · New York, NY--}}
    {{--<br>--}}
    {{--P: (123) - 456 789 · email@domain.com--}}
    {{--</p>--}}
    {{--<form id='contacts_form' action="#" class="contact-list">--}}
    {{--<div class="field-row">--}}
    {{--<input class="form_item" type="text" id="name" name="name" placeholder="Name" />--}}
    {{--</div>--}}
    {{--<div class="field-row">--}}
    {{--<input class="form_item" type="email" id="email" name="email" placeholder="E-mail" />--}}
    {{--</div>--}}
    {{--<div class="field-row">--}}
    {{--<input class="form_item" type="text" id="message" name="message" placeholder="Message" />--}}
    {{--</div>--}}
    {{--<div class="field-row">--}}
    {{--<input type="submit" class="form_submit" value="Send Message" />--}}
    {{--</div>--}}
    {{--<div id="contact_results"></div>--}}
    {{--</form>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <a class="close" href="#"><img alt="" src="{{ asset('ticker/img/close-dark.png') }}"></a>
</section>

{{--<section class="social-container">--}}
{{--<ul class="social">--}}
{{--<li>--}}
{{--<a href="#"><img src="{{ asset('ticker/img/icons/twitter-icon-dark.png') }}" alt="" /></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#"><img src="{{ asset('ticker/img/icons/youtube-icon-dark.png') }}" alt="" /></a>--}}
{{--</li>--}}
{{--<li>--}}
{{--<a href="#"><img src="{{ asset('ticker/img/icons/facebook-icon-dark.png') }}" alt="" /></a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</section>--}}

<script src="{{ asset('ticker/js/vendor/jquery-1.11.1.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/vendor/min/classie.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/vendor/jquery.easing.1.3.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/vendor/jquery.cycle.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/plugins.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/main.js') }}" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('ticker/clock/js/svg.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/clock/js/svg.easing.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/clock/js/svg.clock.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/vendor/ThreeWebGL.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/js/vendor/ThreeExtras.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/clock/js/jquery.timers.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('ticker/clock/js/clock.js') }}" type="text/javascript" charset="utf-8"></script>

</body>
</html>
