<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_event', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('event_id')->nullable();
            $table->unsignedInteger('task_id')->nullable();
            $table->timestamps();
        });
        Schema::table('task_event', function (Blueprint $table) {
            $table->foreign('event_id')->references('id')->on('calendar_events')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('task_event', function (Blueprint $table) {
            $table->dropForeign('task_event_event_id_foreign');
            $table->dropForeign('task_event_task_id_foreign');
        });
        Schema::dropIfExists('task_calendar');
    }
}
