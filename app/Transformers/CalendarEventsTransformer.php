<?php

namespace App\Transformers;

use App\Models\CalendarEvent;
use Carbon\Carbon;
use Fractal;

class CalendarEventsTransformer extends Transformer
{
    protected $availableIncludes = [
        'tasks'
    ];

    /**
     * @param CalendarEvent $event
     * @return array
     */
    public function transform(CalendarEvent  $event)
    {
        Carbon::setLocale('ru');


        return [
            'id' => $event->id,
            'user_id' => $event->user_id,
            'calendar_id' => $event->calendar_id,
            'description' => $event->description,
            'date' => $event->date,
            'hours' => $event->hours,
            'available_hours' => $event->available_hours,
            'is_free' => $event->is_free,
            'is_for_task' => $event->is_for_task,
            'tasks' => $event->tasks,
        ];
    }

    /**
     * @param CalendarEvent $event
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTasks(CalendarEvent $event)
    {
        $tasks = $event->tasks()->with([
            'events', 'activity', 'direction', 'project',
        ])->get();

        return $this->collection($tasks, new TaskTransformer);
    }
}