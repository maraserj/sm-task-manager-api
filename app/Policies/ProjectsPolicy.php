<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the project.
     *
     * @param  \App\Models\User    $user
     * @param  \App\Models\Project $project
     *
     * @return boolean
     */
    public function view(User $user, Project $project)
    {
        if ($user->can('admin'))
            return true;
        $members = $user->projects()->where('project_id', $project->id)->first();
        if ($members || $user->id === $project->project_lead)
            return true;
        else
            return false;
    }

    /**
     * Determine whether the user can create projects.
     *
     * @return boolean
     */
    public function create()
    {
        return true;
    }

    /**
     * Determine whether the user can update the project.
     *
     * @param  \App\Models\User    $user
     * @param  \App\Models\Project $project
     *
     * @return boolean
     */
    public function update(User $user, Project $project)
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $project->project_lead);
    }

    /**
     * Determine whether the user can delete the project.
     *
     * @param  \App\Models\User    $user
     * @param  \App\Models\Project $project
     *
     * @return boolean
     */
    public function delete(User $user, Project $project)
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $project->project_lead);
    }
}
