<?php

use Illuminate\Database\Seeder;
use App\Models\Rank;

class RanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rank::truncate();

        $calendars = [1, 2, 3];
        $activities = [1, 2, 3];
        $directions = [1, 2, 3];
        $projects = [1, 2, 3];
        $tasks = [1, 2, 3];

        $rank = 1;

        foreach ($calendars as $calendar) {
            foreach ($activities as $activity) {
                foreach ($directions as $direction) {
                    foreach ($projects as $project) {
                        foreach ($tasks as $task) {
                            $rank++;
                        }
                    }
                }
            }
        }

        foreach ($calendars as $calendar) {
            foreach ($activities as $activity) {
                foreach ($directions as $direction) {
                    foreach ($projects as $project) {
                        foreach ($tasks as $task) {
                            Rank::create([
                                'calendar_priority' => $calendar,
                                'activity_priority' => $activity,
                                'direction_priority' => $direction,
                                'project_priority' => $project,
                                'task_priority' => $task,
                                'total_rank' => --$rank,
                            ]);
                        }
                    }
                }
            }
        }

    }
}
