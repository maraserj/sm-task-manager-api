<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('users', function (Blueprint $table) {
//            $table->foreign('id')->references('user_id')->on('user_infos')->onUpdate('cascade')->onDelete('cascade');
//        });
        Schema::table('user_infos', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('calendar_events', function (Blueprint $table) {
            $table->foreign('calendar_id')->references('id')->on('calendars')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('calendars', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('calendar_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('directions', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('activity_id')->references('id')->on('activities')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->foreign('activity_id')->references('id')->on('activities')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('direction_id')->references('id')->on('directions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('project_lead')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('tasks', function (Blueprint $table) {
            $table->foreign('activity_id')->references('id')->on('activities')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('direction_id')->references('id')->on('directions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('projects')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('assigned_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('author_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('project_members', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('projects')->onUpdate('cascade')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('users', function (Blueprint $table) {
//            $table->dropForeign('users_id_foreign');
//        });
        Schema::table('user_infos', function (Blueprint $table) {
            $table->dropForeign('user_infos_user_id_foreign');
        });
        Schema::table('calendar_events', function (Blueprint $table) {
            $table->dropForeign('calendar_events_calendar_id_foreign');
            $table->dropForeign('calendar_events_user_id_foreign');
        });
        Schema::table('calendars', function (Blueprint $table) {
            $table->dropForeign('calendars_type_id_foreign');
            $table->dropForeign('calendars_user_id_foreign');
        });
        Schema::table('activities', function (Blueprint $table) {
            $table->dropForeign('activities_user_id_foreign');
        });
        Schema::table('directions', function (Blueprint $table) {
            $table->dropForeign('directions_user_id_foreign');
            $table->dropForeign('directions_activity_id_foreign');
        });
        Schema::table('projects', function (Blueprint $table) {
            $table->dropForeign('projects_activity_id_foreign');
            $table->dropForeign('projects_direction_id_foreign');
            $table->dropForeign('projects_project_lead_foreign');
        });
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropForeign('tasks_activity_id_foreign');
            $table->dropForeign('tasks_direction_id_foreign');
            $table->dropForeign('tasks_project_id_foreign');
            $table->dropForeign('tasks_assigned_id_foreign');
            $table->dropForeign('tasks_author_id_foreign');
        });
        Schema::table('project_members', function (Blueprint $table) {
            $table->dropForeign('project_members_user_id_foreign');
            $table->dropForeign('project_members_project_id_foreign');
        });

    }
}
