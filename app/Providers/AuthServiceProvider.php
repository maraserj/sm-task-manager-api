<?php

namespace App\Providers;

use App\Models\{
    Activity, Calendar, CalendarEvent, Direction, Project, Task
};
use App\Policies\{
    ActivitiesPolicy, CalendarsPolicy, CalendarEventsPolicy, DirectionsPolicy, ProjectsPolicy, TasksPolicy
};
use Carbon\Carbon;
use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Activity::class => ActivitiesPolicy::class,
        Direction::class => DirectionsPolicy::class,
        Calendar::class => CalendarsPolicy::class,
        CalendarEvent::class => CalendarEventsPolicy::class,
        Project::class => ProjectsPolicy::class,
        Task::class => TasksPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));

        Gate::define('admin', function ($user) {
            return $user->role == 'admin';
        });
    }
}
