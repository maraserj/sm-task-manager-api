<?php

namespace App\Events;

use App\Models\{ Project, Task};
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\{ PrivateChannel, PresenceChannel, InteractsWithSockets};
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaskEventsNeedsRefresh
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $task;
    public $project;

    /**
     * Create a new event instance.
     *
     * @param Task    $task
     * @param Project $project
     */
    public function __construct(Task $task, Project $project)
    {
        $this->task = $task;
        $this->project = $project;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }


}
