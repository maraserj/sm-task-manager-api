<?php

namespace App\Http\Controllers;

use App\Gateways\GoogleCalendarGateway;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GoogleCalendarController extends Controller
{
    private $gateway;
    private $request;

    public function __construct(GoogleCalendarGateway $gateway, Request $request)
    {
        $this->gateway = $gateway;
        $this->request = $request;
    }

    public function index()
    {
        $calendarId = $this->request->get('calendarId');
        $dateStart = $this->request->get('dateStart', null);
        $dateStop = $this->request->get('dateStop', null);
        $dateStart = $dateStart ? Carbon::parse($dateStart) : null;
        $dateStop = $dateStop ? Carbon::parse($dateStop) : null;

        if (!empty($calendarId)) {
            $events = $this->gateway->getGoogleEvents($dateStart, $dateStop, $calendarId);
            if (empty($events)) {
                return $this->respondNotFound('Событий с таким ID календаря не найдено. Возможно ваш календарь не публичный.');
            }
            return $this->respondOK($events);
        } else {
            return $this->respondNotFound('Вы не указали ID календаря');
        }
    }

    public function create()
    {
        //
    }

    public function store()
    {
        $eventId = $this->request->get('eventId');
        $calendarId = $this->request->get('calendarId');

        if (!empty($eventId) && !empty($calendarId)) {
            $event = $this->gateway->makeDayAsHoliday($calendarId, $eventId);
            if (empty($event)) {
                return $this->respondNotFound('Такого события не найдено.');
            }
            return $this->respondOK($event);
        } else {
            return $this->respondNotFound('Вы не указали ID календаря');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
