<?php

namespace App\Repositories;

use App\Models\CalendarType;
use \Illuminate\Contracts\Validation\Validator;

class TypeCalendarsRepository extends BaseRepository
{
    protected $_fillable = [];

    /**
     * TypeCalendarsRepository constructor.
     *
     * @param CalendarType $model
     */
    public function __construct(CalendarType $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     *
     * @return BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        !isset($data['name']) ?: $data['slug'] = str_slug($data['name']);

        return parent::create(array_only($data, $this->_fillable));
    }

    /**
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        !isset($data['name']) ?: $data['slug'] = str_slug($data['name']);

        return parent::update($id, array_only($data, $this->_fillable));
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function delete($id): bool
    {
        $calendar = parent::find($id);
        if ($calendar && $calendar->delete()) {
            return true;
        } else
            return false;
    }

}