<?php
namespace App\Repositories;

use App\Models\UserWidget;

class UserWidgetsRepository extends BaseRepository
{
    protected $_fillable;

    public function __construct(UserWidget $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    public function all()
    {
        $userWidgets = $this->model->where('user_id', auth()->id());
        return $userWidgets;
    }

    public function updateWidgetState($widget)
    {
        return $this->model->where('user_id', auth()->id())
                ->where('widget_id', $widget->widget_id)
                ->update( ['selected' => $widget->selected]);
    }


}


