<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * App\Models\Task
 *
 * @property int $id
 * @property string|null $key
 * @property int|null $activity_id
 * @property int|null $direction_id
 * @property int|null $project_id
 * @property int $parent_id
 * @property int|null $assigned_id
 * @property int|null $author_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $estimate
 * @property int $priority
 * @property int|null $order
 * @property int|null $rank
 * @property \Carbon\Carbon|null $date_start
 * @property \Carbon\Carbon|null $deadline
 * @property int $urgent
 * @property int $is_daily
 * @property string $status
 * @property int|null $min_parts_for_task
 * @property int|null $max_parts_for_task
 * @property \Carbon\Carbon|null $start_at
 * @property \Carbon\Carbon|null $end_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Activity $activity
 * @property-read \App\Models\User $assigned
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attachment[] $attachments
 * @property-read \App\Models\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \App\Models\Direction $direction
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CalendarEvent[] $events
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $members
 * @property-read \App\Models\Project $project
 * @property-read \App\Models\Task $subTask
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereAssignedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereDirectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereEstimate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereIsDaily($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMaxPartsForTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereMinPartsForTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUrgent($value)
 * @mixin \Eloquent
 */
class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'parent_id',
        'activity_id',
        'direction_id',
        'project_id',
        'assigned_id',
        'author_id',
        'key',
        'title',
        'description',
        'estimate',
        'priority',
        'urgent',
        'is_daily',
        'order',
        'rank',
        'date_start',
        'deadline',
        'status',
        'min_parts_for_task',
        'max_parts_for_task',
        'start_at',
        'end_at',
    ];
    protected $dates = [
        'date_start',
        'deadline',
        'start_at',
        'end_at',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activity(): HasOne
    {
        return $this->hasOne(Activity::class, 'id', 'activity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function direction(): HasOne
    {
        return $this->hasOne(Direction::class, 'id', 'direction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function project(): HasOne
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function assigned(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'assigned_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subTask(): HasOne
    {
        return $this->hasOne(Task::class, 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events(): BelongsToMany
    {
        return $this->belongsToMany(CalendarEvent::class, 'task_event', 'task_id', 'event_id')->withTimestamps();
    }

    /**
     * @return MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return MorphMany
     */
    public function attachments(): MorphMany
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'task_members', 'task_id', 'user_id')->withTimestamps();
    }

    /**
     * @param $id
     * @return array|Model|null|\stdClass|static
     */
    public function getLatestTaskInProject($id)
    {
        return self::where('project_id', $id)->latest()->first();
    }
}
