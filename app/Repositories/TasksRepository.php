<?php

namespace App\Repositories;

use App\Events\TaskEventsNeedsRefresh;
use App\Models\{
    Activity, Calendar, CalendarEvent, Direction, Project, Rank, Task, TaskMember, User
};
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TasksRepository extends BaseRepository
{
    protected $_fillable = [];
    protected $calendarEvent;
    protected $calendar;


    const FILTER_PERIODS = [
        'today',
        'week',
        'two_weeks',
        'month',
        'year'
    ];
    const FILTER_STATUSES = [
        'todo' => 'To Do',
        'in_progress' => 'In progress',
        'done' => 'Done',
        'closed' => 'Closed',
    ];
    const FILTER_PRIORITIES = [
        1,// Low
        2,// Normal
        3,// High
    ];

    /**
     * @var Task $model
     */
    protected $model;
    protected $filterCollection;

    /**
     * TasksRepository constructor.
     * @param Task          $task
     * @param CalendarEvent $calendarEvent
     * @param Calendar      $calendar
     */
    public function __construct(Task $task, CalendarEvent $calendarEvent, Calendar $calendar)
    {
        $this->_fillable = $task->getFillable();
        $this->calendarEvent = $calendarEvent;
        $this->calendar = $calendar;
        $this->model = $task;
        parent::__construct($task);
    }

    public function getTasksStatuses()
    {
        return self::FILTER_STATUSES;
    }

    public function getFilteredTasks(array $params = [], $childrenProjects = null)
    {
        $sortParams = $params['sort'] ?? [];
        $filterParams = $params['filter'] ?? [];
        if (!isset($params['perPage'])) $filterParams['perPage'] = 10;

        $filter = [];
        if (isset($params['deadline'])) {
            $filter[] = ['deadline', '=', Carbon::parse($params['deadline'])->format('Y-m-d')];
        }
        if (isset($params['is_daily'])) {
            $filter[] = ['is_daily', '=', true];
        }
//        $filter[] = [function ($query) {
//            $query->where('author_id', auth()->id())->orWhere('assigned_id', auth()->id());
//        }];
        $this->filterCollection = $this->model->with(['events', 'project', 'author'])->whereHas('project.members', function ($query) {
            $query->where('user_id', auth()->id());
        });
        if (isset($filterParams)) {


//            if (isset($filterParams['project_id']) && $filterParams['project_id'] != '') {
//                if (isset($childrenProjects)) {
//                    foreach ($childrenProjects->pluck('id')->prepend($filterParams['project_id'])->all() as $childrenProject) {
//                        $filter [] = ['project_id', '=', (int)$childrenProject];
//                    }
//                } else {
//                    $filter [] = ['project_id', '=', $filterParams['project_id']];
//                }
//
//            }
            if (isset($filterParams['project_id']) && $filterParams['project_id'] != '') {
                $filter [] = ['project_id', '=', $filterParams['project_id']];
            }

            if (isset($filterParams['status']) && $filterParams['status'] != '') {
                if (in_array($filterParams['status'], self::FILTER_STATUSES)) {
                    $filter[] = ['status', '=', $filterParams['status']];
                }
            }
            if (isset($filterParams['priority']) && $filterParams['priority'] != '') {
                if (in_array($filterParams['priority'], self::FILTER_PRIORITIES)) {
                    $filter[] = ['priority', '=', $filterParams['priority']];
                }
            }
            if (isset($filterParams['period']) && $filterParams['period'] != '') {
                if (! empty($filter)) {
                    $this->filterCollection->where($filter)->whereHas('events', function($query) use ($filterParams) {
                        if (in_array($filterParams['period'], self::FILTER_PERIODS)) {
                            $query->where($this->getFilteredPeriod($filterParams['period']));
                        }
                    });
                } else {
                    $this->filterCollection->whereHas('events', function($query) use ($filterParams) {
                        if (in_array($filterParams['period'], self::FILTER_PERIODS)) {
                            $query->where($this->getFilteredPeriod($filterParams['period']));
                        }
                    });
                }

            } else {
                $this->filterCollection->where($filter);
            }
        }
        $response = $this->filterCollection->with($this->withAll());

        if (isset($sortParams)) {
            $sorted = $this->sortTasks($sortParams);
            foreach ($sorted as $sort) {
                $response->orderBy($sort[0], $sort[1]);
            }
        }


        if (isset($params['limit'])) {
            return $response->limit((int)$params['limit'])->get();
        } else {
            return $response->paginate($filterParams['perPage']);
        }
    }

    private function sortTasks ($params) {
        $orders = [];

        foreach ($params as $key => $param) {
            if (in_array($key, $this->_fillable) && isset($param)) {
                $orders[] = [$key, $param];
            }
        }
        if (empty($orders)) {
            $orders[] = ['order', 'asc'];
        }

        return $orders;
    }

    private function withAll()
    {
        return ['activity', 'direction', 'project', 'members'];
    }

    public function getFilteredPeriod($period)
    {
        $filtered = [];
        if (isset($period) && $period != '') {
            switch ($period) {
                case 'week':
                    $filtered [] = ['date', '>=', Carbon::today()];
                    $filtered [] = ['date', '<=', Carbon::today()->addWeek()];
                    break;
                case 'two_weeks':
                    $filtered [] = ['date', '>=', Carbon::today()];
                    $filtered [] = ['date', '<=', Carbon::today()->addWeeks(2)];
                    break;
                case 'month':
                    $filtered [] = ['date', '>=', Carbon::today()];
                    $filtered [] = ['date', '<=', Carbon::today()->addMonth()];
                    break;
                case 'year':
                    $filtered [] = ['date', '>=', Carbon::today()];
                    $filtered [] = ['date', '<=', Carbon::today()->addYear()];
                    break;
                default:
                    $filtered[] = ['date', '=', Carbon::today()];
                    break;
            }
        }
        return $filtered;

    }

    public function paginate($perPage = null)
    {
        $id = auth()->id();
        $params = [];
        $params[] = ['author_id', '=', $id];
        return $this->model->whereIn('assigned_id', [$id])->orWhere('assigned_id', $id)->with($this->withAll())->orderBy('order')->paginate($perPage ?? 4);
    }

    public function getTask($id)
    {
        return $this->model->whereId($id)->with(['events', 'project'])->get();
    }

    public function all()
    {
        return $this->model->where('user_id', auth()->id())->with(['events', 'project'])->get();
    }

    public function create(array $data = [])
    {
        $project = Project::find($data['project_id']);
        $data['author_id'] = $data['author_id'] ?? auth()->id();
        $data['priority'] = $data['priority'] ?? 0;
        if (!isset($data['parent_id']) || $data['parent_id'] == '') {
            $data['parent_id'] = 0;
        }
        $activity_id = Direction::find($project->direction_id)->activity_id;
        $lastProjectTask = $this->model->getLatestTaskInProject($project->id);

        $data['activity_id'] = $activity_id;
        $data['deadline'] = $data['deadline'] ?? $project->deadline;
        $data['direction_id'] = $project->direction_id;
        $data['estimate'] = $this->convertTime(explode('_', $data['estimate']));
        $data['key'] = '#' . (isset($lastProjectTask) ? (str_replace('#', '', $lastProjectTask->key) + 1) : 1);
        $data['rank'] = $this->setTaskRank($data);

        $task = parent::create(array_only($data, $this->_fillable));
//        $task->members()->create(['user_id' => $data['author_id'], 'task_id' => $task->id]);
        $task->members()->attach($task->id, ['user_id' => $data['author_id'], 'task_id' => $task->id]);

        if ($task->is_daily) {
            $event = CalendarEvent::create([
                'user_id'         => auth()->id(),
                'calendar_id'     => $project->activity->calendar_id,
                'date'            => $task->deadline,
                'available_hours' => '{}',
                'is_free'         => false,
                'is_for_task'     => true,
                'hours'           => $task->estimate,
            ]);
            $task->events()->attach($event->id);
        }

        return $task;
    }

    /**
     * @param int   $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        $task = $this->model->find($id);
        $project = Project::find($data['project_id']);
        $data['priority'] = $data['priority'] ?? 0;
        if (!isset($data['parent_id']) || $data['parent_id'] == '') {
            $data['parent_id'] = 0;
        }
        $activity_id = Direction::find($project->direction_id)->activity_id;

        $data['activity_id'] = $activity_id;
        $data['direction_id'] = $project->direction_id;
        $data['estimate'] = $this->convertTime(explode('_', $data['estimate']));
        if (isset($data['key']) && $task->key != $data['key']) {
            $data['key'] = $this->getProjectKey($project);
        }
        $data['rank'] = $this->setTaskRank($data);

        return parent::update($id, array_only($data, $this->_fillable));
    }

    public function updateRanks(array $data = [])
    {
        dd($data);

        $currentRank = $this->model->where('rank', $data['current']['rank'])->first();
        if (!isset($data['target']['id'])) {
            $targetRank = $this->model->orderBy('rank', 'DESC')->latest()->first();
        } else {
            $targetRank = $this->model->where('rank', $data['target']['rank'])->first();
        }
        $totalTargetRank = $targetRank->rank;
        $ranksArray = [];
        if ($currentRank && $targetRank) {
            if ($currentRank->rank - 1 === $targetRank->rank || $currentRank->rank + 1 === $targetRank->rank) {
                $currentRank->rank = $targetRank->rank;
                $targetRank->rank = $data['current']['rank'];
            } else {
                if ($currentRank->rank <= $targetRank->rank) {
                    // снизу вверх

                    for ($i = $currentRank->rank + 1; $i <= $targetRank->rank; $i++) {
                        $ranksArray[] = $i;
                    }
                    $rangeRanks = $this->model->whereIn('rank', $ranksArray)->get();
                    $currentRank->rank = $totalTargetRank;
                    $currentRank->save();

                    foreach ($rangeRanks as $rank) {
                        $rank->rank = $rank->rank - 1;
                        $rank->save();
                    }
                } else if ($currentRank->rank >= $targetRank->rank) {
                    // свверху вниз


                    for ($i = $targetRank->rank; $i < $currentRank->rank; $i++) {
                        $ranksArray[] = $i;
                    }
                    $rangeRanks = $this->model->whereIn('rank', $ranksArray)->get();
                    $currentRank->rank = $totalTargetRank;
                    $currentRank->save();
                    foreach ($rangeRanks as $rank) {
                        $rank->rank = $rank->rank + 1;
                        $rank->save();
                    }
                }
            }
        }
        $currentRank->save();
        $targetRank->save();

        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $task = parent::find($id);
        if ($task) {
            $task->events()->delete();
            $task->delete();
            return true;
        } else
            return false;
    }

    /**
     * @param $calendar_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @internal param $id
     */
    public function getTasksEvents($calendar_id)
    {
        return $this->model->with(['project', 'events' => function ($query) use ($calendar_id) {
            $query->where([['is_for_task', 1], ['calendar_id', $calendar_id]]);
        }])->get();
    }

    /**
     * @param $calendar_id
     * @param $date
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @internal param $id
     */
    public function getTasksEventsByDate($calendar_id, $date)
    {
        return $this->model/*->whereHas('events', function ($query) use ($date) {
            $query->where('date', $date);
        })*/->with(['project', 'events' => function ($query) use ($calendar_id, $date) {
            $query->where([['is_for_task', 1], ['calendar_id', $calendar_id], ['date', '=', Carbon::parse($date)->format('Y-m-d')]]);
        }])->pluck('id');
    }

    public function getTasksComments($id)
    {
        $model = $this->find($id);
        $comments = $model->comments()->where('parent_id', null)->with(['user', 'children'])->orderBy('created_at', 'DESC')->get();

        return $comments;
    }

    public function getTaskWithFullInfo($id)
    {
        return $this->model->where('id', $id)->with(['attachments', 'assigned', 'author', 'project', 'activity', 'direction', 'events'])->withCount(['comments'])->first();
    }

    /**
     * @param array $time
     * @param int   $userWorkTime
     * @return float|int|mixed
     */
    private function convertTime(array $time = [], $userWorkTime = 8)
    {
        if (isset($time[1])) {
            switch ($time[1]) {
                case 'm':
                    $estimateForTask = $time[0] / 60;
                    break;
                case 'd':
                    $estimateForTask = $time[0] * $userWorkTime;
                    break;
                default:
                    $estimateForTask = $time[0];
                    break;
            }
        } else {
            $estimateForTask = $time[0];
        }

        return $estimateForTask;

    }

    /**
     * @param $project
     * @return string
     */
    private function getProjectKey($project)
    {
        $lastProjectTask = $this->model->getLatestTaskInProject($project->id);
        if (isset($lastProjectTask))
            $key = (str_replace('#', '', $lastProjectTask->key) + 1);
        else
            $key = 1;

        return '#' . $key;
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function setTaskRank($data)
    {
        $query = [];
        $project = Project::find($data['project_id']);
        if ($project) {
            $query[] = ['task_priority', '=', $data['priority'] ?? 1];
            $query[] = ['project_priority', '=', $project->priority];
            if ($project->direction) {
                $query[] = ['direction_priority', '=', $project->direction->priority];
            }
            if ($project->activity) {
                $activity = $project->activity;
                $query[] = ['activity_priority', '=', $project->activity->priority];
                if ($activity->calendar) {
                    $query[] = ['calendar_priority', '=', $activity->calendar->priority];
                }
            }
        }
        $rank = Rank::where($query)->get()->first();

        $totalRank = $rank->total_rank;
        $dateStart = null;
        $dateDeadline = null;
        $diffDates = null;

        $today = Carbon::today();
        if (isset($data['date_start']) && $data['date_start'] != '' && Carbon::now() <= $data['date_start']) {
            $diffStart = Carbon::parse($data['date_start']);
            $dateStart = $diffStart->diffInDays($today);
        }
        if (isset($data['deadline'])) {
            $diffDeadline = Carbon::parse($data['deadline']);
            $dateDeadline = $diffDeadline->diffInDays($today);
        }

        $totalRank = $totalRank + ($dateStart <= $dateDeadline ? $dateStart : $dateDeadline);

        return $totalRank;
    }


    /**
     * @param $id
     * @return Project|null
     */
    public function getTaskMembers($id)
    {
        $task = $this->model->find($id);

        if ($task)
            return $task->members;
        else
            return null;
    }

    /**
     * @param $id
     * @return User|null
     */
    public function getTaskMember($id)
    {
        $taskMember = TaskMember::find($id);
        if ($taskMember) {
            $member = User::find($taskMember->user_id);
            if ($member)
                return $member;
            else
                return null;
        } else
            return null;
    }

    /**
     * @param array $data
     * @return TaskMember|TasksRepository|bool
     */
    public function createTaskMember(array $data = [])
    {
        $member = TaskMember::create($data);
        if ($member)
            return $member;
        else
            return false;
    }

    /**
     * @param $id
     * @param array $data
     * @return TaskMember|bool
     */
    public function updateTaskMember($id, array $data = [])
    {
        $data['user_id'] = $data['user_id'] ?? auth()->id();
        $member = TaskMember::find($id);
        if ($member->update($data))
            return $member;
        else
            return false;
    }

    /**
     * @param $data
     * @return bool
     */
    public function deleteTaskMember($data): bool
    {
        $member = TaskMember::where($data)->first();
        if ($member->delete())
            return true;
        else
            return false;
    }

    public function getTasksStatistics(){
        $statistics = [];
        $tasks = $this->model->where('author_id', auth()->id())->get();
        if($tasks->count() > 0){
            $statistics['count_tasks'] = $tasks->count();
            $statistics['count_open'] = $tasks->where('status', self::FILTER_STATUSES['todo'])->count();
            $statistics['count_pending'] = $tasks->where('status', self::FILTER_STATUSES['in_progress'])->count();
            $statistics['count_closed'] = $tasks->where('status', self::FILTER_STATUSES['closed'])->count();
            $statistics['done_percent'] = round($statistics['count_closed'] / $statistics['count_tasks'], 2);
        } else {
            $statistics['count_tasks'] = 0;
            $statistics['count_open'] = 0;
            $statistics['count_pending'] = 0;
            $statistics['count_closed'] = 0;
            $statistics['done_percent'] = 0;
        }

        return $statistics;
    }

    public function getTasksStatisticsByActivity($id = null) {
        $statistics = [];

        $results = \Auth::user()->activities()->has('tasks')->with('tasks')->get();

        foreach($results as $key => $item) {
            $statistics[$key] = $item;
            $statistics[$key]['count_tasks'] = $item['tasks']->count();
        }

        return $statistics;
    }
}