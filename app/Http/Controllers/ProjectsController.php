<?php

namespace App\Http\Controllers;

use App\Events\RefreshTasksRanks;
use App\Events\TaskEventsNeedsRefresh;
use App\Http\Requests\Project\StoreProjectRequest;
use App\Http\Requests\Project\UpdateProjectRequest;
use App\Traits\ApiResponse;
use App\Gateways\ProjectsGateway;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;

class ProjectsController extends Controller
{
    protected $gateway;
    protected $request;
    private $user;

    /**
     * ProjectsController constructor.
     * @param ProjectsGateway $gateway
     * @param Request $request
     */
    public function __construct(ProjectsGateway $gateway, Request $request)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->gateway = $gateway;
        $this->request = $request;
    }

    public function index(Request $request)
    {
        $data = $this->gateway->getAll($request->except(['_token', '_method']));

        return $this->respondOk($data);
    }

    /**
     * @param StoreProjectRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreProjectRequest $request): JsonResponse
    {
        $project = $this->gateway->create($request->all());

        if ($project)
            return $this->respondCreated($project, trans('common.projects.created'));
        else
            return $this->respondBadRequest(trans('common.projects.not_created'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $project = $this->gateway->find($id);
        $this->authorize('view', $project);

        if (!$project || empty($project))
            return $this->respondNotFound();
        else
            return $this->respondOk($this->gateway->show($id));
    }

    public function getSubProjects($id)
    {
        $project = $this->gateway->find($id);
//        $this->authorize('view', $project);

        if ($project) {
            $children = $this->gateway->getSubProjects($this->request->all(), $id);

            return $this->respondOK($children);
        } else {
            return $this->respondNotFound();
        }
    }

    /**
     * @param $projectId
     * @return JsonResponse
     */
    public function getTasks($projectId): JsonResponse
    {
        $project = $this->gateway->find($projectId);
        $this->authorize('view', $project);

        $request = $this->request->all();

        if ($project) {
            $request['filter']['project_id'] = $projectId;
            if (isset($request['perPage'])) {
                $request['filter']['perPage'] = $request['perPage'];
            }
            $members = $this->gateway->getProjectMembers($this->user, $projectId);

            if ($members || $this->user->id === $project->project_lead) {
                if (!$project->tasks)
                    return $this->respondNotFound();
                else
                    $tasks = $this->gateway->getTasks($request, $project->children);
                return $this->respondOk($tasks);
            } else
                return $this->respondDenied();
        } else {
            return $this->respondNotFound();
        }
    }

    public function getComments($projectId): JsonResponse
    {
        $project = $this->gateway->find($projectId);
        $this->authorize('view', $project);

        $comments = $this->gateway->getComments($project);
        if (!$comments || empty($comments))
            return $this->respondNotFound();
        else
            return $this->respondOk($comments);
    }

    /**
     * @param UpdateProjectRequest $request
     * @param                      $id
     *
     * @return JsonResponse
     */
    public function update(UpdateProjectRequest $request, $id): JsonResponse
    {
        $data['id'] = $id;
        $data['request'] = $request->all();
        $project = $this->gateway->find($id);
        if ($project && isset($data['request']['priority']) && $project->priority != $data['request']['priority']) {
            $tasks = \Auth::user()->tasks;
            event(new RefreshTasksRanks($tasks));
        }

        $project = $this->gateway->update($data);

        if ($project) {
            if (isset($data['request']['priority']) || isset($data['request']['deadline'])) {
                event(new TaskEventsNeedsRefresh($this->gateway->getInstance(), $project));
            }
            return $this->respondOK($project, trans('common.projects.updated'));
        } else
            return $this->respondBadRequest(trans('common.projects.not_updated'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = $this->gateway->find($id);
        $this->authorize('delete', $data);

        if ($this->gateway->delete($id)) {
            return $this->respondOK(trans('common.projects.deleted'));
        } else
            return $this->respondNotFound();
    }

    public function attachments($projectId)
    {
        $attachments = $this->gateway->getAttachments($projectId);
        if ($attachments) {
            return $this->respondOK($attachments);
        } else {
            return $this->respondNotFound();
        }
    }

    public function storeAttachments($id)
    {
        $project = $this->gateway->find($id);
        $this->authorize('view', $project);

        $data = $this->request->all();
        if (isset($data['file'])) {
            $fileAttributes = $this->gateway->storeAttachments($project->id, $data);
            if (isset($fileAttributes[0]) && !empty($fileAttributes)) {
                $fileAttributes[0]['user_id'] = auth()->id();
                $attachment = $project->attachments()->create($fileAttributes[0]);
            } else {
                $attachment = null;
            }

            return $this->respondOK($attachment);
        } else {
            return $this->respondBadRequest();
        }
    }

    public function deleteAttachment($id, $attachmentId)
    {
        $project = $this->gateway->find($id);

        if ($project && $attachmentId) {
            $deleted = $this->gateway->deleteAttachments($project, $attachmentId);
            if ($deleted) {
                return $this->respondOK();
            } else {
                return $this->respondNotFound();
            }
        } else {
            return $this->respondNotFound();
        }
    }

}
