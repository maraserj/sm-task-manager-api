<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Propeller is a front-end responsive framework based on Material design & Bootstrap.">
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
    <title>Accordion - Style - Propeller</title>

    <!-- favicon -->
    <link rel="icon" href="http://propeller.in/assets/images/favicon.ico" type="image/x-icon">

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- Latest compiled and minified JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- Google Icon Font -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--<link href="../components/icons/css/google-icons.css" type="text/css" rel="stylesheet" /> -->

    <!-- Propeller  js -->
    <script type="text/javascript">
        /*!
         * Propeller v1.1.0 (http://propeller.in/)
         * Copyright 2016-2017 Digicorp, Inc
         * Licensed under MIT (http://propeller.in/LICENSE)
         */

        "use strict";$(document).ready(function(){$(".pmd-textfield-focused").remove(),$(".pmd-textfield .form-control").after('<span class="pmd-textfield-focused"></span>'),$(".pmd-textfield input.form-control").each(function(){""!==$(this).val()&&$(this).closest(".pmd-textfield").addClass("pmd-textfield-floating-label-completed")}),$(".pmd-textfield input.form-control").on("change",function(){""!==$(this).val()&&$(this).closest(".pmd-textfield").addClass("pmd-textfield-floating-label-completed")}),$("body").on("focus",".pmd-textfield .form-control",function(){$(this).closest(".pmd-textfield").addClass("pmd-textfield-floating-label-active pmd-textfield-floating-label-completed")}),$("body").on("focusout",".pmd-textfield .form-control",function(){""===$(this).val()&&$(this).closest(".pmd-textfield").removeClass("pmd-textfield-floating-label-completed"),$(this).closest(".pmd-textfield").removeClass("pmd-textfield-floating-label-active")})}),$(document).ready(function(){$(".pmd-checkbox input").after('<span class="pmd-checkbox-label">&nbsp;</span>'),$(".pmd-checkbox-ripple-effect").on("mousedown",function(a){var b=$(this);$(".ink").remove(),0===b.find(".ink").length&&b.append('<span class="ink"></span>');var c=b.find(".ink");c.removeClass("animate"),c.height()||c.width()||c.css({height:20,width:20});var d=a.pageX-b.offset().left-c.width()/2,e=a.pageY-b.offset().top-c.height()/2;c.css({top:e+"px",left:d+"px"}).addClass("animate"),setTimeout(function(){c.remove()},1500)})}),$(document).ready(function(){$(".pmd-radio input").after('<span class="pmd-radio-label">&nbsp;</span>'),$(".pmd-radio-ripple-effect").on("mousedown",function(a){var b=$(this);$(".ink").remove(),0===b.find(".ink").length&&b.append('<span class="ink"></span>');var c=b.find(".ink");c.removeClass("animate"),c.height()||c.width()||c.css({height:15,width:15});var d=a.pageX-b.offset().left-c.width()/2,e=a.pageY-b.offset().top-c.height()/2;c.css({top:e+"px",left:d+"px"}).addClass("animate"),setTimeout(function(){c.remove()},1500)})}),$(document).ready(function(){$(".pmd-ripple-effect").on("mousedown touchstart",function(a){var b=$(this);$(".ink").remove(),0===b.find(".ink").length&&b.append("<span class='ink'></span>");var c=b.find(".ink");if(c.removeClass("animate"),!c.height()&&!c.width()){var d=Math.max(b.outerWidth(),b.outerHeight());c.css({height:d,width:d})}var e=a.pageX-b.offset().left-c.width()/2,f=a.pageY-b.offset().top-c.height()/2;c.css({top:f+"px",left:e+"px"}).addClass("animate"),setTimeout(function(){c.remove()},1500)})}),$(document).ready(function(){$(".pmd-dropdown .dropdown-menu").wrap("<div class='pmd-dropdown-menu-container'></div>"),$(".pmd-dropdown .dropdown-menu").before('<div class="pmd-dropdown-menu-bg"></div>');var a=$(".pmd-dropdown"),b=function(){if($(window).width()<767){var b=a.find(".dropdown-menu").outerWidth(),c=a.find(".dropdown-menu").outerHeight();a.find(".dropdown-menu-right").css("clip","rect(0 "+b+"px 0 "+b+"px)"),a.find(".pmd-dropdown-menu-top-left").css("clip","rect("+c+"px 0 "+c+"px 0)"),a.find(".pmd-dropdown-menu-top-right").css("clip","rect("+c+"px "+b+"px "+c+"px "+b+"px)"),a.off("show.bs.dropdown"),a.on("show.bs.dropdown",function(){var a=$(this).find(".dropdown-menu"),b=a.outerWidth(),c=a.outerHeight(),d=a.closest(".pmd-dropdown-menu-container"),e=d.find(".pmd-dropdown-menu-bg"),f=$(this).find(".dropdown-toggle").attr("data-sidebar"),g=a.hasClass("pmd-dropdown-menu-center");"true"==f?(a.first().stop(!0,!0).slideDown(300),$(this).addClass("pmd-sidebar-dropdown")):g?($(".dropdown-menu").removeAttr("style"),a.first().stop(!0,!0).slideDown(300)):(d.css({width:b+"px",height:c+"px"}),e.css({width:b+"px",height:c+"px"}),a.hasClass("dropdown-menu-right")?(setTimeout(function(){a.css("clip","rect(0 "+b+"px "+c+"px 0)")},10),e.addClass("pmd-dropdown-menu-bg-right"),d.css({right:"0",left:"auto"})):a.hasClass("pmd-dropdown-menu-top-left")?(setTimeout(function(){a.css("clip","rect(0 "+b+"px "+c+"px 0)")},10),e.addClass("pmd-dropdown-menu-bg-bottom-left")):a.hasClass("pmd-dropdown-menu-top-right")?(setTimeout(function(){a.css("clip","rect(0 "+b+"px "+c+"px 0)")},10),e.addClass("pmd-dropdown-menu-bg-bottom-right"),d.css({right:"0",left:"auto"})):setTimeout(function(){a.css("clip","rect(0 "+b+"px "+c+"px 0)")},10))}),a.off("hide.bs.dropdown"),a.on("hide.bs.dropdown",function(){var a=$(this).find(".dropdown-toggle").attr("data-sidebar"),b=$(this).find(".dropdown-menu").hasClass("pmd-dropdown-menu-center"),c=$(this).find(".dropdown-menu"),d=c.outerWidth(),e=c.outerHeight(),f=c.closest(".pmd-dropdown-menu-container"),g=f.find(".pmd-dropdown-menu-bg");"true"==a?c.first().stop(!0,!0).slideUp(300):b?($(".dropdown-menu").removeAttr("style"),c.first().stop(!0,!0).slideUp(300)):(c.css("clip","rect(0 0 0 0)"),f.removeAttr("style"),g.removeAttr("style"),c.hasClass("dropdown-menu-right")?c.css("clip","rect(0 "+d+"px 0 "+d+"px)"):c.hasClass("pmd-dropdown-menu-top-left")?c.css("clip","rect("+e+"px 0 "+e+"px 0)"):c.hasClass("pmd-dropdown-menu-top-right")&&c.css("clip","rect("+e+"px "+d+"px "+e+"px "+d+"px)"))})}else{$(".dropdown-menu").removeAttr("style");var d=a.find(".dropdown-menu").outerWidth(),e=a.find(".dropdown-menu").outerHeight();a.find(".dropdown-menu-right").css("clip","rect(0 "+d+"px 0 "+d+"px)"),a.find(".pmd-dropdown-menu-top-left").css("clip","rect("+e+"px 0 "+e+"px 0)"),a.find(".pmd-dropdown-menu-top-right").css("clip","rect("+e+"px "+d+"px "+e+"px "+d+"px)"),a.off("show.bs.dropdown"),a.on("show.bs.dropdown",function(){var a=$(this).closest(".pmd-sidebar").hasClass("pmd-sidebar"),b=$(this).find(".dropdown-menu").hasClass("pmd-dropdown-menu-center"),c=$(this).find(".dropdown-menu"),d=c.outerWidth(),e=c.outerHeight(),f=c.closest(".pmd-dropdown-menu-container"),g=f.find(".pmd-dropdown-menu-bg");a?c.first().stop(!0,!0).slideDown():b?($(this).parents().hasClass("pmd-sidebar")||$(".dropdown-menu").removeAttr("style"),c.first().stop(!0,!0).slideDown()):(f.css({width:d+"px",height:e+"px"}),g.css({width:d+"px",height:e+"px"}),c.hasClass("dropdown-menu-right")?(setTimeout(function(){c.css("clip","rect(0 "+d+"px "+e+"px 0)")},10),g.addClass("pmd-dropdown-menu-bg-right"),f.css({right:"0",left:"auto"})):c.hasClass("pmd-dropdown-menu-top-left")?(setTimeout(function(){c.css("clip","rect(0 "+d+"px "+e+"px 0)")},10),g.addClass("pmd-dropdown-menu-bg-bottom-left")):c.hasClass("pmd-dropdown-menu-top-right")?(setTimeout(function(){c.css("clip","rect(0 "+d+"px "+e+"px 0)")},10),g.addClass("pmd-dropdown-menu-bg-bottom-right"),f.css({right:"0",left:"auto"})):setTimeout(function(){c.css("clip","rect(0 "+d+"px "+e+"px 0)")},10)),this.closable=!1}),a.on("click",function(){$(this).closest(".pmd-sidebar").hasClass("pmd-sidebar")&&!$(this).hasClass("open")?(a.removeClass("open"),$(".dropdown-menu").slideUp(300)):$(this).parents("aside").hasClass("pmd-sidebar")&&$(".dropdown-menu").slideUp(300),this.closable=!0}),a.off("hide.bs.dropdown"),a.on("hide.bs.dropdown",function(){if($(this).parents("aside").hasClass("pmd-sidebar"))return this.closable;var a=$(this).closest(".pmd-sidebar").hasClass("pmd-sidebar"),b=$(this).find(".dropdown-menu").hasClass("pmd-dropdown-menu-center"),c=$(this).find(".dropdown-menu"),d=c.outerWidth(),e=c.outerHeight(),f=c.closest(".pmd-dropdown-menu-container"),g=f.find(".pmd-dropdown-menu-bg");a?c.first().stop(!0,!0).slideUp(300):b?($(this).parents().hasClass("pmd-sidebar")||$(".dropdown-menu").removeAttr("style"),c.first().stop(!0,!0).slideUp(300)):(c.css("clip","rect(0 0 0 0)"),f.removeAttr("style"),g.removeAttr("style"),c.hasClass("dropdown-menu-right")?c.css("clip","rect(0 "+d+"px 0 "+d+"px)"):c.hasClass("pmd-dropdown-menu-top-left")?c.css("clip","rect("+e+"px 0 "+e+"px 0)"):c.hasClass("pmd-dropdown-menu-top-right")&&c.css("clip","rect("+e+"px "+d+"px "+e+"px "+d+"px)"))})}};b(),$(window).resize(function(){b()})}),$(document).ready(function(){$(function(){$(".collapse.in").parents(".panel").addClass("active"),$('a[data-toggle="collapse"]').on("click",function(){var a=$(this).attr("href");"true"==$(this).attr("data-expandable")&&($(a).hasClass("in")?$(a).collapse("hide"):$(a).collapse("show"));var b=$(this).attr("data-expandable"),c=$(this).attr("aria-expanded"),d=$(this).parent().parent().parent().parent().attr("id");"false"==b&&("true"==c?$("#"+d+" .active").removeClass("active"):($("#"+d+" .active").removeClass("active"),$(this).parents(".panel").addClass("active"))),"true"==b&&("true"==c?$(this).parents(".panel").addClass("active"):$(this).parents(".panel").removeClass("active"))}),$("#expandAll").on("click",function(){var a=$(this).attr("data-target");$("#"+a+' a[data-toggle="collapse"]').each(function(){var a=$(this).attr("href");!1===$(a).hasClass("in")&&($(a).collapse("show"),$(a).parent().addClass("active"))})}),$("#collapseAll").on("click",function(){var a=$(this).attr("data-target");$("#"+a+' a[data-toggle="collapse"]').each(function(){var a=$(this).attr("href");$(a).collapse("hide"),$(a).parent().removeClass("active")})})})}),$(document).ready(function(){$(".pmd-alert-toggle").click(function(){var a,b=$(this).attr("data-positionX"),c=$(this).attr("data-positionY"),d=$(this).attr("data-effect"),e=$(this).attr("data-message"),f=$(this).attr("data-type"),g=$(this).attr("data-action-text"),h=$(this).attr("data-action");b=$(window).width()<768?"center":b,$(".pmd-alert-container."+b+"."+c).length||$("body").append("<div class='pmd-alert-container "+b+" "+c+"'></div>");var i=$(".pmd-alert-container."+b+"."+c),j=function(){return j="true"==h?null==g?"<div class='pmd-alert' data-action='true'>"+e+"<a href='javascript:void(0)' class='pmd-alert-close'>Г—</a></div>":"<div class='pmd-alert' data-action='true'>"+e+"<a href='javascript:void(0)' class='pmd-alert-close'>"+g+"</a></div>":null==g?"<div class='pmd-alert' data-action='false'>"+e+"</div>":"<div class='pmd-alert' data-action='false'>"+e+"<a href='javascript:void(0)' class='pmd-alert-close'>"+g+"</a></div>"}(),k=$(".pmd-alert-container."+b+"."+c+" .pmd-alert").length;a=void 0!==$(this).attr("data-duration")?$(this).attr("data-duration"):3e3,k>0?("top"==c?i.append(j):i.prepend(j),i.width($(".pmd-alert").outerWidth()),"true"==h?i.children("[data-action='true']").addClass("visible "+d):i.children("[data-action='false']").addClass("visible "+d).delay(a).slideUp(function(){$(this).removeClass("visible "+d).remove()}),i.children(".pmd-alert").eq(k).addClass(f)):(i.append(j),i.width($(".pmd-alert").outerWidth()),"true"==h?i.children("[data-action='true']").addClass("visible "+d):i.children("[data-action='false']").addClass("visible "+d).delay(a).slideUp(function(){$(this).removeClass("visible "+d).remove()}),i.children(".pmd-alert").eq(k).addClass(f));var l=$(".pmd-alert").outerWidth()/2;$(".pmd-alert-container.center").css("marginLeft","-"+l+"px")}),$(document).on("click",".pmd-alert-close",function(){var a=$(this).attr("data-effect");$(this).parents(".pmd-alert").slideUp(function(){$(this).removeClass("visible "+a).remove()})})}),$(document).ready(function(){$('.popover-html[data-toggle="popover"]').popover({html:!0,content:function(){var a=$(this).attr("data-id");return $(a).html()},placement:function(a,b){var c=$(b).attr("data-placement"),d=$(window).scrollTop(),e=$(window).width(),f=$(window).height(),g=$(b).outerWidth(),h=$(b).outerHeight(),i=$(b).offset().top,j=$(b).offset().left,k=i-d,l=j,m=e-l-g,n=f-k-h;return"left"==c&&l<=200?"right":"right"==c&&m<=200?"left":"top"==c&&k<=200?"bottom":"bottom"==c&&n<=200?"top":c}});var a={placement:function(a,b){var c=$(b).attr("data-placement"),d=$(window).scrollTop(),e=$(window).width(),f=$(window).height(),g=$(b).outerWidth(),h=$(b).outerHeight(),i=$(b).offset().top,j=$(b).offset().left,k=i-d,l=j,m=e-l-g,n=f-k-h;return"left"==c&&l<=200?"right":"right"==c&&m<=200?"left":"top"==c&&k<=200?"bottom":"bottom"==c&&n<=200?"top":c}};$('[data-toggle="popover"]').popover(a),$('[data-toggle="popover"]').on("shown.bs.popover",function(){var a=$(this).attr("data-color");$(".popover").addClass(a)}).on("hidden.bs.popover",function(){var a=$(this).attr("data-color");$(".popover").removeClass(a)})}),function(a){a.fn.pmdTab=function(){return this.each(function(){var b=a(this),c=a(".pmd-tabs-scroll-container"),d=function(){var c=0;return b.find("li").each(function(){var b=a(this)[0].getBoundingClientRect().width;c+=b}),c},e=function(){var c=b.attr("class");a("."+c+":hidden").not("script").addClass("notVisible"),b.parents(":hidden").not("script").find("ul.nav-tabs").addClass("notVisible"),b.find("ul.nav-tabs").hasClass("nav-justified")?b.find("ul.nav-tabs").width("100%"):b.hasClass("notVisible")||b.find("ul.nav-tabs").width(d())},f=function(){return b.find("ul.nav-tabs").position().left},g=function(){if(b.outerWidth()<d()){var a=b.find(".pmd-tabs-scroll-container").scrollLeft(),c=b.width();b.find(".nav-tabs").width()-a-c>0&&b.find(".pmd-tabs-scroll-right").show()}else b.find(".pmd-tabs-scroll-right").hide();if(f()<0){b.find(".pmd-tabs-scroll-container").scrollLeft()>0&&b.find(".pmd-tabs-scroll-left").show()}else b.find(".pmd-tabs-scroll-left").hide()},h=function(){var a=b.outerWidth(),c=a/2,d=b.offset().left,e=b.find("ul li.active"),f=e.outerWidth(),g=f/2,h=b.find(".pmd-tabs-scroll-container").scrollLeft(),i=b.find("ul li.active").offset().left,j=i-c-d+h+g;b.find(".pmd-tabs-scroll-container").animate({scrollLeft:j},1)};e(),g(),h(),a(window).on("resize",function(){setTimeout(function(){e(),g(),h()},150)});var i=function(){var c=b.find(".pmd-tab-active-bar"),d=b.find("ul li.active"),e=d.offset().left,f=b.find(".nav").offset().left,g=b.offset().left,h=e-g,i=g-f+e-g;f<g?c.width(d.width()+"px").css("left",i+"px"):c.width(d.width()+"px").css("left",h+"px"),b.find("ul li").click(function(){var b=a(this).width()+"px",d=a(this).offset().left-g,e=a(this).closest(".nav").offset().left;i=g-e+d,c.width(b).css("left",i+"px")})};i(),a(window).on("resize",function(){i()}),b.find(".pmd-tabs-scroll-right").on("click",function(){var c="",d=a(this).prev(".pmd-tabs-scroll-container"),e=d.find(".nav-tabs li"),f=a(this).outerWidth(),g=b.outerWidth(),h=b.offset().left+g;if(e.each(function(){var b=a(this).offset().left,d=a(this).offset().left+a(this).outerWidth();a(this).removeClass("prev-tab"),b<h&&d>h&&(c=d-h+f,a(this).addClass("last-tab"),a(this).prev().removeClass("last-tab"))}),0===d.find(".last-tab").next().length){var i=d.find(".last-tab").offset().left+d.find(".last-tab").outerWidth(),j=i-h;d.animate({scrollLeft:"+="+j}),a(this).fadeOut("slow")}else d.animate({scrollLeft:"+="+c});a(this).parents(".pmd-tabs").find(".pmd-tabs-scroll-left").fadeIn("slow")}),b.find(".pmd-tabs-scroll-left").on("click",function(){var c=a(this).next(".pmd-tabs-scroll-container"),d=c.find(".nav-tabs li"),e=a(this).outerWidth(),f=b.offset().left,g="";if(d.each(function(){var b=a(this).offset().left,c=a(this).offset().left+a(this).outerWidth();a(this).removeClass("last-tab"),b<f&&c>f&&(g=f-b+e,a(this).addClass("prev-tab"),a(this).next().removeClass("prev-tab"))}),0===c.find(".prev-tab").prev().length){var h=c.find(".prev-tab").offset().left,i=f-h;c.animate({scrollLeft:"-="+i}),a(this).fadeOut("slow")}else c.animate({scrollLeft:"-="+g});a(this).parents(".pmd-tabs").find(".pmd-tabs-scroll-right").fadeIn("slow")}),b.find("ul li").on("click",function(){c=a(this).closest(".pmd-tabs-scroll-container");var d=a(this).offset().left,e=a(this).offset().left+a(this).outerWidth(),f=b.outerWidth(),g=b.offset().left+f,h=a(".pmd-tabs-scroll-right").outerWidth(),i=b.offset().left,j=g-h,k=i+h;if(d<k&&e>k){var l=i-d+h;c.animate({scrollLeft:"-="+l}),a(this).parents(".pmd-tabs").find(".pmd-tabs-scroll-right").fadeIn("slow")}if(d<j&&e>j){var m=e-g+h;c.animate({scrollLeft:"+="+m}),a(this).parents(".pmd-tabs").find(".pmd-tabs-scroll-left").fadeIn("slow")}})})}}(jQuery);var overlay=$(".pmd-sidebar-overlay"),sidebar=$(".pmd-sidebar"),lsidebar=$(".pmd-sidebar-left"),rsidebar=$(".pmd-sidebar-right-fixed"),sidebar=$(".pmd-sidebar"),toggleButtons=$(".pmd-sidebar-toggle");$(document).ready(function(){var a=$(".pmd-sidebar-overlay"),b=$(".pmd-sidebar"),c=$(".pmd-sidebar-left"),d=$(".pmd-sidebar-right-fixed"),e=$(".pmd-navbar-sidebar"),f=$(".pmd-sidebar-toggle"),g=$(".topbar-fixed");$(".pmd-sidebar-toggle").on("click",function(){c.toggleClass("pmd-sidebar-open"),(c.hasClass("pmd-sidebar-left-fixed")||c.hasClass("pmd-sidebar-right-fixed"))&&c.hasClass("pmd-sidebar-open")?(a.addClass("pmd-sidebar-overlay-active"),$("body").addClass("pmd-body-open")):(a.removeClass("pmd-sidebar-overlay-active"),$("body").removeClass("pmd-body-open"))}),$(".pmd-sidebar .dropdown-menu, .pmd-sidebar-dropdown .dropdown-menu").click(function(a){a.stopPropagation()}),$(".pmd-sidebar-toggle-right").on("click",function(){d.toggleClass("pmd-sidebar-open"),d.hasClass("pmd-sidebar-right")&&d.hasClass("pmd-sidebar-open")?(a.addClass("pmd-sidebar-overlay-active"),$("body").addClass("pmd-body-open")):(a.removeClass("pmd-sidebar-overlay-active"),$("body").removeClass("pmd-body-open"))}),$(".pmd-topbar-toggle").on("click",function(){g.toggleClass("pmd-sidebar-open")}),$(".topbar-close").on("click",function(){g.removeClass("pmd-sidebar-open")}),$(".pmd-navbar-toggle").on("click",function(){e.toggleClass("pmd-sidebar-open"),e.hasClass("pmd-navbar-sidebar")&&e.hasClass("pmd-sidebar-open")?(a.addClass("pmd-sidebar-overlay-active"),$("body").addClass("pmd-body-open")):(a.removeClass("pmd-sidebar-overlay-active"),$("body").removeClass("pmd-body-open"))}),a.on("click",function(){$(this).removeClass("pmd-sidebar-overlay-active"),$(".pmd-sidebar").removeClass("pmd-sidebar-open"),$(".pmd-navbar-sidebar").removeClass("pmd-sidebar-open"),$("body").removeClass("pmd-body-open"),event.stopPropagation()}),$(window).width()<1200&&(b.removeClass("pmd-sidebar-open pmd-sidebar-slide-push"),c.addClass("pmd-sidebar-left-fixed"),d.addClass("pmd-sidebar-right"),f.css("display","inherit"),$("body").removeClass("pmd-body-open"))}),$(window).resize(function(){$(window).width()<1200?(sidebar.removeClass("pmd-sidebar-open pmd-sidebar-slide-push"),lsidebar.addClass("pmd-sidebar-left-fixed"),rsidebar.addClass("pmd-sidebar-right"),toggleButtons.css("display","inherit"),overlay.removeClass("pmd-sidebar-overlay-active"),$("body").removeClass("pmd-body-open")):(lsidebar.removeClass("pmd-sidebar-left-fixed").addClass("pmd-sidebar-open pmd-sidebar-slide-push"),rsidebar.removeClass("pmd-sidebar-right"),overlay.removeClass("pmd-sidebar-overlay-active"),$("body").removeClass("pmd-body-open"))}),function(a){jQuery.fn.removeClass=function(b){if(b&&"function"==typeof b.test)for(var c=0,d=this.length;c<d;c++){var e=this[c];if(1===e.nodeType&&e.className){for(var f=e.className.split(/\s+/),g=f.length;g--;)b.test(f[g])&&f.splice(g,1);e.className=jQuery.trim(f.join(" "))}}else a.call(this,b);return this}}(jQuery.fn.removeClass);
    </script>
    <style>
        /*!
 * Propeller v1.1.0 (http://propeller.in): floating-action-button.css
 * Copyright 2016-2017 Digicorp, Inc.
 * Licensed under MIT (http://propeller.in/LICENSE)
 */

        /*!
         * Propeller v1.1.0 (http://propeller.in): button.css
         * Copyright 2016-2017 Digicorp, Inc.
         * Licensed under MIT (http://propeller.in/LICENSE)
        */

        .btn { display: inline-block; padding: 6px 12px; margin-bottom: 0; text-align: center; white-space: nowrap; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; background-image: none; border: 1px solid transparent; border-radius: 4px; font-size: 14px; font-weight: 400; line-height: 1.1; text-transform: uppercase; letter-spacing: inherit; color: rgba(255, 255, 255, 0.87);}
        .btn-default, .btn-link { color: rgba(0, 0, 0, 0.87);}

        /* -- Buttons style ------------------------------------ */
        .btn { outline: 0; outline-offset: 0; border: 0; border-radius: 2px; transition: all 0.15s ease-in-out; -o-transition: all 0.15s ease-in-out; -moz-transition: all 0.15s ease-in-out; -webkit-transition: all 0.15s ease-in-out;}
        .btn:focus,.btn:active,.btn.active,.btn:active:focus,.btn.active:focus { outline: 0; outline-offset: 0; box-shadow: none;}

        /* -- Buttons varients -------------------------------- */
        /* -- Buttons raised --*/
        .pmd-btn-raised { box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24);}
        .pmd-btn-raised:active,.pmd-btn-raised.active,.pmd-btn-raised:active:focus,.pmd-btn-raised.active:focus { box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);}
        .pmd-btn-raised:focus { box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);}

        /* -- Buttons circle --*/
        .btn.pmd-btn-fab { padding: 0; border-radius: 50%;}
        .btn.pmd-btn-fab span,
        .btn.pmd-btn-fab i { line-height:56px;}

        /* -------------------------- Buttons colors -------------------------------- */

        /* -- Buttons Default --*/
        .btn-default,.dropdown-toggle.btn-default { background-color: #ffffff;}
        .btn-default:hover,.dropdown-toggle.btn-default:hover { background-color: #e5e5e5;}
        .btn-default:active,.dropdown-toggle.btn-default:active,.btn-default.active,.dropdown-toggle.btn-default.active {background-color: #e5e5e5;}
        .btn-default:focus,.dropdown-toggle.btn-default:focus { background-color: #cccccc;}
        .btn-default:disabled,.dropdown-toggle.btn-default:disabled,.btn-default.disabled,.dropdown-toggle.btn-default.disabled,
        .btn-default[disabled],.dropdown-toggle.btn-default[disabled] { background-color: #b3b3b3;}
        .btn-default .ink,.dropdown-toggle.btn-default .ink { background-color: #b8b8b8;}

        /* -- Buttons Default flat --*/
        .pmd-btn-flat.btn-default { color: #212121; background-color: transparent;}
        .pmd-btn-flat.btn-default:hover { color: #141414; background-color: #e5e5e5;}
        .pmd-btn-flat.btn-default:active,.pmd-btn-flat.btn-default.active { color: #020202; background-color: #cccccc;}
        .pmd-btn-flat.btn-default:focus { color: #000000; background-color: #cccccc;}
        .pmd-btn-flat.btn-default .ink { background-color: #808080;}

        /* -- Buttons Default outline --*/
        .btn-default.pmd-btn-outline{border:solid 1px #333333;}

        /* -- Buttons Primary --*/
        .btn-primary,.dropdown-toggle.btn-primary{ background-color: #4285f4;}
        .btn-primary:hover,.dropdown-toggle.btn-primary:hover { background-color: #4e6cef;}
        .btn-primary:active,.dropdown-toggle.btn-primary:active,.btn-primary.active,.dropdown-toggle.btn-primary.active {background-color: #4e6cef;}
        .btn-primary:focus,.dropdown-toggle.btn-primary:focus { background-color: #455ede;}
        .btn-primary:disabled,.dropdown-toggle.btn-primary:disabled,.btn-primary.disabled,.dropdown-toggle.btn-primary.disabled,

        .btn-primary[disabled],.dropdown-toggle.btn-primary[disabled] { background-color: #b3b3b3;}
        .btn-primary .ink,.dropdown-toggle.btn-primary .ink { background-color: #3b50ce;}

        /* -- Buttons primary flat --*/
        .pmd-btn-flat.btn-primary { color: #4285f4; background-color: transparent;}
        .pmd-btn-flat.btn-primary:hover { color: #4e6cef; background-color: #e5e5e5;}
        .pmd-btn-flat.btn-primary:active,.pmd-btn-flat.btn-primary.active { color: #455ede; background-color: #cccccc;}
        .pmd-btn-flat.btn-primary:focus { color: #3b50ce; background-color: #cccccc;}
        .pmd-btn-flat.btn-primary .ink { background-color: #808080;}

        /* -- Buttons primary outline --*/
        .pmd-btn-outline.btn-primary{ border: solid 1px #4285f4; background-color:transparent; color:#4285f4;}
        .pmd-btn-outline.btn-primary:hover, .pmd-btn-outline.btn-primary:focus { border: solid 1px #4285f4; background-color:#4285f4; color:#fff;}

        /* -- Buttons Success --*/
        .btn-success,
        .dropdown-toggle.btn-success { background-color: #259b24;}
        .btn-success:hover,.dropdown-toggle.btn-success:hover { background-color: #0a8f08;}
        .btn-success:active,.dropdown-toggle.btn-success:active,.btn-success.active,.dropdown-toggle.btn-success.active { background-color: #0a8f08;}
        .btn-success:focus,.dropdown-toggle.btn-success:focus { background-color: #0a7e07;}
        .btn-success:disabled,.dropdown-toggle.btn-success:disabled,.btn-success.disabled,.dropdown-toggle.btn-success.disabled,
        .btn-success[disabled],.dropdown-toggle.btn-success[disabled] { background-color: #b3b3b3;}
        .btn-success .ink,.dropdown-toggle.btn-success .ink { background-color: #056f00;}

        /* -- Buttons flat Success --*/
        .pmd-btn-flat.btn-success { color: #259b24; background-color: transparent;}
        .pmd-btn-flat.btn-success:hover { color: #0a8f08; background-color: #e5e5e5;}
        .pmd-btn-flat.btn-success:active,.pmd-btn-flat.btn-success.active { color: #0a7e07; background-color: #cccccc;}
        .pmd-btn-flat.btn-success:focus { color: #056f00; background-color: #cccccc;}
        .pmd-btn-flat.btn-success .ink { background-color: #808080;}

        /*-- Button success outline --*/
        .pmd-btn-outline.btn-success{ border: solid 1px #259b24; background-color:transparent; color:#259b24;}
        .pmd-btn-outline.btn-success:hover, .pmd-btn-outline.btn-success:focus { border: solid 1px #259b24; background-color:#259b24; color:#fff;}

        /* -- Buttons Info --*/
        .btn-info,.dropdown-toggle.btn-info { background-color: #03a9f4;}
        .btn-info:hover,.dropdown-toggle.btn-info:hover { background-color: #039be5;}
        .btn-info:active,.dropdown-toggle.btn-info:active,.btn-info.active,.dropdown-toggle.btn-info.active { background-color: #039be5;}
        .btn-info:focus,.dropdown-toggle.btn-info:focus { background-color: #0288d1;}
        .btn-info:disabled,.dropdown-toggle.btn-info:disabled,.btn-info.disabled,.dropdown-toggle.btn-info.disabled,.btn-info[disabled],.dropdown-toggle.btn-info[disabled] { background-color: #b3b3b3;}
        .btn-info .ink,.dropdown-toggle.btn-info .ink { background-color: #0277bd;}

        /* -- Buttons Info flat--*/
        .pmd-btn-flat.btn-info { color: #03a9f4; background-color: transparent;}
        .pmd-btn-flat.btn-info:hover { color: #039be5; background-color: #e5e5e5;}
        .pmd-btn-flat.btn-info:active,.pmd-btn-flat.btn-info.active { color: #0288d1; background-color: #cccccc;}
        .pmd-btn-flat.btn-info:focus { color: #0277bd; background-color: #cccccc;}
        .pmd-btn-flat.btn-info .ink { background-color: #808080;}

        /* -- Button Info outline --*/
        .pmd-btn-outline.btn-info{ border: solid 1px #03a9f4; background-color:transparent; color:#03a9f4;}
        .pmd-btn-outline.btn-info:hover, .pmd-btn-outline.btn-info:focus { border: solid 1px #03a9f4; background-color:#03a9f4; color:#fff;}

        /* -- Buttons Warning --*/
        .btn-warning,.dropdown-toggle.btn-warning { background-color: #ffc107;}
        .btn-warning:hover,.dropdown-toggle.btn-warning:hover { background-color: #ffb300;}
        .btn-warning:active,.dropdown-toggle.btn-warning:active,.btn-warning.active,.dropdown-toggle.btn-warning.active { background-color: #ffb300;}
        .btn-warning:focus,.dropdown-toggle.btn-warning:focus { background-color: #ffa000;}
        .btn-warning:disabled,.dropdown-toggle.btn-warning:disabled,.btn-warning.disabled,.dropdown-toggle.btn-warning.disabled, .btn-warning[disabled],.dropdown-toggle.btn-warning[disabled] { background-color: #b3b3b3;}
        .btn-warning .ink,.dropdown-toggle.btn-warning .ink { background-color: #ff8f00;}

        /* -- Buttons flat Warning --*/
        .pmd-btn-flat.btn-warning { color: #ffc107; background-color: transparent;}
        .pmd-btn-flat.btn-warning:hover { color: #ffb300; background-color: #e5e5e5;}
        .pmd-btn-flat.btn-warning:active,.pmd-btn-flat.btn-warning.active { color: #ffa000; background-color: #cccccc;}
        .pmd-btn-flat.btn-warning:focus { color: #ff8f00; background-color: #cccccc;}
        .pmd-btn-flat.btn-warning .ink { background-color: #808080;}

        /*-- Button warning outline --*/
        .pmd-btn-outline.btn-warning{ border: solid 1px #ffc107; background-color:transparent; color:#ffc107;}
        .pmd-btn-outline.btn-warning:hover, .pmd-btn-outline.btn-warning:focus { border: solid 1px #ffc107; background-color:#ffc107; color:#fff;}

        /* -- Buttons Danger --*/
        .btn-danger,.dropdown-toggle.btn-danger { background-color: #ff5722;}
        .btn-danger:hover,.dropdown-toggle.btn-danger:hover { background-color: #f4511e;}
        .btn-danger:active,.dropdown-toggle.btn-danger:active,.btn-danger.active,.dropdown-toggle.btn-danger.active { background-color: #f4511e;}
        .btn-danger:focus,.dropdown-toggle.btn-danger:focus { background-color: #e64a19;}
        .btn-danger:disabled,.dropdown-toggle.btn-danger:disabled,.btn-danger.disabled,.dropdown-toggle.btn-danger.disabled,.btn-danger[disabled],.dropdown-toggle.btn-danger[disabled] { background-color: #b3b3b3;}
        .btn-danger .ink,.dropdown-toggle.btn-danger .ink { background-color: #d84315;}

        /* -- Buttons flat Danger --*/
        .pmd-btn-flat.btn-danger { color: #ff5722; background-color: transparent;}
        .pmd-btn-flat.btn-danger:hover { color: #f4511e; background-color: #e5e5e5;}
        .pmd-btn-flat.btn-danger:active,.pmd-btn-flat.btn-danger.active { color: #e64a19; background-color: #cccccc;}
        .pmd-btn-flat.btn-danger:focus { color: #d84315; background-color: #cccccc;}
        .pmd-btn-flat.btn-danger .ink { background-color: #808080;}

        /*-- Button danger outline --*/
        .pmd-btn-outline.btn-danger{ border: solid 1px #ff5722; background-color:transparent; color:#ff5722;}
        .pmd-btn-outline.btn-danger:hover, .pmd-btn-outline.btn-danger:focus { border: solid 1px #ff5722; background-color:#ff5722; color:#fff;}

        /* -- Buttons sizes -------------------------------- */
        .btn { min-width: 88px; padding: 10px 14px;}
        .btn-lg,.btn-group-lg > .btn { min-width: 122px; padding: 10px 16px; font-size: 18px; line-height: 1.3;}
        .btn-sm,.btn-group-sm > .btn { min-width: 64px; padding: 4px 12px; font-size: 12px; line-height: 1.5;}
        .btn-xs,.btn-group-xs > .btn { min-width: 46px; padding: 2px 10px; font-size: 10px; line-height: 1.5;}

        /* -- Buttons circle sizes --*/
        .pmd-btn-fab { width: 56px; height: 56px; min-width: 56px;}
        .pmd-btn-fab span { line-height: 56px;}
        .pmd-btn-fab.btn-lg { width: 78px; height: 78px; min-width: 78px;}
        .pmd-btn-fab.btn-lg span { line-height: 78px;}
        .pmd-btn-fab.btn-sm { width: 40px; height: 40px; min-width: 40px;}
        .pmd-btn-fab.btn-sm span, .pmd-btn-fab.btn-sm i { line-height: 40px;}
        .pmd-btn-fab.btn-xs { width: 30px; height: 30px; min-width: 30px;}
        .pmd-btn-fab.btn-xs span, .pmd-btn-fab.btn-xs i { line-height: 30px;}

        /*---------------------------------- Button groups --------------------------------- */
        .btn-group .btn { border-radius: 2px;}
        .btn-group.open .dropdown-toggle { outline: 0; outline-offset: 0; box-shadow: none;}
        .btn-group .btn + .btn,.btn-group .btn + .btn-group,.btn-group .btn-group + .btn,.btn-group .btn-group + .btn-group { margin-left: 0;}
        .btn-group > .btn:hover,.btn-group-vertical > .btn:hover { z-index: 0;}
        .btn-group > .btn:focus:hover,.btn-group-vertical > .btn:focus:hover,.btn-group > .btn:active:hover,.btn-group-vertical > .btn:active:hover,.btn-group > .btn.active:hover,.btn-group-vertical > .btn.active:hover { z-index: 2;}

        /* --------------------------------- Ripple effect -------------------------------- */
        .pmd-ripple-effect { position: relative; overflow: hidden; -webkit-transform: translate3d(0, 0, 0);}
        .ink { display: block; position: absolute; pointer-events: none; border-radius: 50%; -ms-transform: scale(0); transform: scale(0); background: #fff; opacity: 1;}
        .ink.animate { animation: ripple .5s linear;}

        /*-- Button link outline --*/
        .pmd-btn-outline.btn-link{ border: solid 1px #333; background-color:transparent;}
        .pmd-btn-outline.btn-link:hover, .pmd-btn-outline.btn-link:focus { border: solid 1px #23527c; background-color:#23527c; color:#fff;}

        @keyframes ripple {100% { opacity: 0; transform: scale(2.5);}}
        .pmd-floating-action { bottom: 0; position: fixed;  margin:1em;  right: 0; z-index:1000;}
        .pmd-floating-action-btn { display:block; position: relative; transition: all .2s ease-out;}
        /*.pmd-floating-action-btn:before { bottom: 10%; content: attr(data-title); opacity: 0; position: absolute; right: 100%; transition: all .2s ease-out .5s;  white-space: nowrap; background-color:#fff; padding:6px 12px; border-radius:2px; color:#333; font-size:12px; margin-right:5px; display:inline-block; box-shadow: 0px 2px 3px -2px rgba(0, 0, 0, 0.18), 0px 2px 2px -7px rgba(0, 0, 0, 0.15);}*/
        .pmd-floating-action-btn:last-child:before { font-size: 14px; bottom: 25%;}
        .pmd-floating-action-btn:active, .pmd-floating-action-btn:focus, .pmd-floating-action-btn:hover {box-shadow: 0px 5px 11px -2px rgba(0, 0, 0, 0.18), 0px 4px 12px -7px rgba(0, 0, 0, 0.15);}
        .pmd-floating-action-btn:not(:last-child){ opacity: 0; -ms-transform: translateY(20px) scale(0.3); transform: translateY(20px) scale(0.3); margin-bottom:15px; margin-left:8px; position:absolute; bottom:0;}
        .pmd-floating-action-btn:not(:last-child):nth-last-child(1) { transition-delay: 50ms;}
        .pmd-floating-action-btn:not(:last-child):nth-last-child(2) { transition-delay: 100ms;}
        .pmd-floating-action-btn:not(:last-child):nth-last-child(3) { transition-delay: 150ms;}
        .pmd-floating-action-btn:not(:last-child):nth-last-child(4) { transition-delay: 200ms;}
        .pmd-floating-action-btn:not(:last-child):nth-last-child(5) { transition-delay: 250ms;}
        .pmd-floating-action-btn:not(:last-child):nth-last-child(6) { transition-delay: 300ms;}
        .pmd-floating-action:hover .pmd-floating-action-btn, .menu--floating--open .pmd-floating-action-btn { opacity: 1; -ms-transform: none; transform: none; position:relative; bottom:auto; }
        .pmd-floating-action:hover .pmd-floating-action-btn:before, .menu--floating--open .pmd-floating-action-btn:before { opacity: 1;}
        .pmd-floating-hidden{ display:none;}
        .pmd-floating-action-btn.btn:hover{ overflow:visible;}

        .pmd-floating-action-btn .ink{ width:50px; height:50px;}
        .hidden {display:none;}
    </style>

    <style>
        body { background:transparent;}
    </style>

</head>

<div class="menu pmd-floating-action" role="navigation">
    {{--<a href="javascript:void(0);" class="button_block hidden pmd-floating-action-btn btn btn-sm pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default">--}}
        {{--<img src="/images/icons/001-whatsapp.png" width="40">--}}
    {{--</a>--}}
    <a href="skype:sergey.intelweb?chat" class="button_block hidden pmd-floating-action-btn btn btn-sm pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default">
        <img src="/images/icons/002-skype.png" width="37">
    </a>
    <a href="https://m.me/sergey.mara" target="_blank" class="button_block hidden pmd-floating-action-btn btn btn-sm pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default">
        <img src="/images/icons/003-messenger.png" width="37">
    </a>
    <a href="https://telegram.me/serjmara" target="_blank" class="button_block hidden pmd-floating-action-btn btn btn-sm pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default">
        <img src="/images/icons/004-telegram.png" width="40">
    </a>
    {{--<a href="javascript:void(0);" class="button_block hidden pmd-floating-action-btn btn btn-sm pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default"><img src="/images/icons/005-vk.png" width="40"></a>--}}
    <a href="viber://chat?number=380961756726" class="button_block hidden pmd-floating-action-btn btn btn-sm pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-default">
        <img src="/images/icons/006-viber.png" width="40">
    </a>
    <a id="main-button" class="pmd-floating-action-btn was-hidden btn pmd-btn-fab pmd-btn-raised pmd-ripple-effect btn-primary">
        <span class="pmd-floating-hidden">Primary</span>
        <i class="material-icons pmd-sm">question_answer</i>
    </a>
</div>
<script>
    $(function() {
        $("#main-button").click(function(){
            console.log('ASF');
            $('#main_frame, #main_frame_div', window.parent.document).css({"height":"420px", "width":"250px"});
            $('.button_block').toggleClass('hidden');
            $('.menu :last-child').toggleClass('was-hidden');
            if (!$(this).hasClass('was-hidden')) {
                $(this).find('i.pmd-sm').remove();
                $(this).append('<i class="material-icons pmd-sm">highlight_off</i>');
            } else {
                $('#main_frame, #main_frame_div', window.parent.document).css({"height":"70px", "width":"70px"});
                $(this).find('i.pmd-sm').remove();
                $(this).append('<i class="material-icons pmd-sm">question_answer</i>');
            }
        });
    });
</script>
</body>
</html>