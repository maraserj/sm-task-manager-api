<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CalendarEvent;
use Illuminate\Auth\Access\HandlesAuthorization;

class CalendarEventsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the event.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\CalendarEvent $event
     *
     * @return boolean
     */
    public function view(User $user, CalendarEvent $event): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $event->user_id);
    }

    /**
     * Determine whether the user can create events.
     *
     * @return boolean
     */
    public function create(): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\CalendarEvent $event
     *
     * @return boolean
     */
    public function update(User $user, CalendarEvent $event): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $event->user_id);
    }

    /**
     * Determine whether the user can delete the event.
     *
     * @param  \App\Models\User          $user
     * @param  \App\Models\CalendarEvent $event
     *
     * @return boolean
     */
    public function delete(User $user, CalendarEvent $event): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $event->user_id);
    }
}
