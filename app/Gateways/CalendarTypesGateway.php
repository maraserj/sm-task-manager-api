<?php
namespace App\Gateways;

use App\Repositories\TypeCalendarsRepository;

class CalendarTypesGateway extends Gateway
{
    protected $repository;

    public function __construct(TypeCalendarsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        $data = $this->repository->all();

        return $data;
    }


    /**
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {
        $type = $this->repository->create($data);

        return $type;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        $calendarType = $this->repository->find($id);

        return $calendarType;
    }

    public function update(array $data = [])
    {
         $calendarType = $this->repository->update($data['id'], $data['request']);

         return $calendarType;
    }

    public function delete(int $id)
    {
        if ($this->repository->delete($id))
            return true;
        else
            return false;
    }


    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails()) return $validator;
        else return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails()) return $validator;
        else return null;
    }

}