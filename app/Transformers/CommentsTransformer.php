<?php

namespace App\Transformers;

use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Fractal;

class CommentsTransformer extends Transformer
{

    /**
     * @param $comment
     * @return mixed
     */
    public function transform($comment)
    {
        Carbon::setLocale('ru');

        return [
            'id'              => $comment->id,
            'text'            => $comment->text,
            'user'            => $comment->user,
            'children'        => $comment->children,
            'created_at'      => $comment->created_at->diffForHumans(),
            'updated_at'      => $comment->updated_at->diffForHumans(),
            'created'         => $comment->created_at->format('Y-m-d H:i:s'),
            'updated'         => $comment->updated_at->format('Y-m-d H:i:s'),
            'can_edit_delete' => $comment->user_id == auth()->id() ? true : false,
        ];
    }

}
