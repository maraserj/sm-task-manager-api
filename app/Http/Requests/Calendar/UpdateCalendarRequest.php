<?php

namespace App\Http\Requests\Calendar;

use App\Http\Requests\Request;
use App\Models\Calendar;

class UpdateCalendarRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $calendar = Calendar::find($this->route('calendar'));

        return $calendar && $this->user()->can('update', $calendar);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required|max:100',
//            'type_id' => 'required',
        ];
    }
}
