<?php

return [

    /*
     * Path to a json file containing the credentials of a Google Service account.
     */
    'client_secret_json' => storage_path('app/pm-soft-b687eb5b113a.json'),

    /*
     *  The id of the Google Calendar that will be used by default.
     */
    'calendar_id' => 'sergey.intelweb@gmail.com',

];
