<?php
namespace App\Gateways;

use App\Repositories\DirectionsRepository;

class DirectionGateway extends Gateway
{
    protected $repository;

    /**
     * DirectionGateway constructor.
     * @param DirectionsRepository $repository
     */
    public function __construct(DirectionsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        $data['directions'] = $this->repository->all();
        $data['base_directions'] = $this->repository->getBaseDirections();

        return $data;
    }


    public function create(array $data = [])
    {
        $direction = $this->repository->create($data);

        return $direction;
    }

    public function show(int $id)
    {
        $direction = $this->repository->find($id);

        return $direction;
    }

    public function update(array $data = [])
    {
        $direction = $this->repository->update($data['id'], $data['request']);

        return $direction;
    }

    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'activity_id' => 'required|numeric',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'activity_id' => 'required|numeric',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

}