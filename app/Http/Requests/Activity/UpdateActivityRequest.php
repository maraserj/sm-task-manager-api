<?php

namespace App\Http\Requests\Activity;

use App\Http\Requests\Request;
use App\Models\Activity;

class UpdateActivityRequest extends Request
{
    public function authorize()
    {
        $activity = Activity::find($this->route('activity'));

        return $activity && $this->user()->can('update', $activity);
    }

    public function rules()
    {
        return [
            'name' => 'required|max:100',
        ];
    }
}
