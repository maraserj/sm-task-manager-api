<?php

namespace App\Http\Requests\Project;

use App\Models\ProjectMember;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class InviteUserToProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    protected function getValidatorInstance()
    {
        $data = $this->getInputSource()->all();
        if (!empty($data['email']) && !empty($data['project_id'])) {
            $user = User::whereEmail($data['email'])->first();
            if ($user) {
                $projectMember = ProjectMember::where([['user_id', '=', $user->id], ['project_id', '=', $data['project_id']]])->first();
                if ($projectMember) {
                    $data['user_exists'] = 0;
                }
            }
        }
        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'user_exists' => 'not_in:0'
        ];
    }

    /**
     * @inheritDoc
     */
    public function messages()
    {
        $messages = parent::messages();

        $messages['email.exists'] = 'Пользователя с таким Email не найдено. Возможно, он еще не зарегистрирован в системе.';
        $messages['user_exists.not_in'] = 'Пользователь уже существует в проекте.';

        return $messages;
    }


}
