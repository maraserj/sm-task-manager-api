<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->request->user['old_password']) && $this->request->user['old_password'] != '') {
            if (\Hash::check($this->request->user['old_password'], $this->request->user['password'])) {
                $rules['user.password'] = 'required|min:6|confirmed';
                $data['user.old_incorrect_password'] = 0;
            } else {
                $data['user.old_incorrect_password'] = 1;
                $rules['user.old_incorrect_password'] = 'not_in:1';
            }
        }
        $rules = [
            'user.name' => 'required|max:100',
            'user.email' => [
                'sometimes',
//                'required',
                'email',
                'max:255',
//                Rule::unique('users')->ignore(auth()->id()),
            ],
        ];


        return $rules;
    }
}
