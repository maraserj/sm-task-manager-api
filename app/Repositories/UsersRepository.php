<?php
namespace App\Repositories;

use App\Models\ProjectMember;
use App\Models\Task;
use App\Models\TaskMember;
use App\Models\User;
use App\Models\UserInfo;
use Hash;
use Illuminate\Validation\Rule;

class UsersRepository extends BaseRepository
{
    protected $_fillable = [];

    /**
     * UsersRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    public function all()
    {
        return $this->model->where('id', '!=' , auth()->id())->get();
    }

    public function find($id)
    {
        return $this->model->where('id', $id)->with(['info', 'events', 'calendars'])->first();
    }

    public function findByEmail($email)
    {
        return $this->model->where('email', $email)->with(['info', 'events', 'calendars'])->first();
    }

    public function getAvailableForTask($taskId)
    {
        $task = Task::find($taskId);
        $inProject = collect(ProjectMember::where([['project_id', $task->project_id], ['user_id', '!=', auth()->id()]])->get())->pluck('user_id')->all();
        $inTask = collect(TaskMember::where([['task_id', $taskId], ['user_id', '!=', auth()->id()]])->get())->pluck('user_id')->all();

        $users = $this->model->whereIn('id', $inProject)->whereNotIn('id', $inTask)->get();
        return $users;
    }

    public function getAvailableForProject($projectId, $emailQuery = null)
    {
        if (!empty($emailQuery)) {
            $users = $this->model->where('email', 'like', '%' . $emailQuery .'%')->get();
        } else {
            $inProject = collect(ProjectMember::where([['project_id', $projectId], ['user_id', '!=', auth()->id()]])->get())->pluck('user_id')->all();
            $users = $this->model->whereIn('id', $inProject)->get();

        }

        return $users;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param $id
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate($id, array $data = [])
    {
        $user = $this->model->find($id);
        $rules = [
            'name' => 'required|max:100',
            'email' => [
                'email',
                'max:255',
                Rule::unique('users')->ignore($id),
            ],
        ];
        if (isset($data['old_password']) && $data['old_password'] != '') {
            if (Hash::check($data['old_password'], $user->password)) {
                $rules['password'] = 'required|min:6|confirmed';
                $data['old_incorrect_password'] = 0;
            } else {
                $data['old_incorrect_password'] = 1;
                $rules['old_incorrect_password'] = 'not_in:1';
            }
        }
        $validator = validator($data, $rules, ['old_incorrect_password.not_in' => 'Вы ввели неправильный старый пароль. Повторите попытку.']);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnAvatarUpload(array $data = [])
    {
        $rules = [
            'file' => 'required|image|max:2000',
        ];

        $validator = validator($data, $rules);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        $data['remember_token'] = $data['remember_token'] ?? str_random(10);
        !isset($data['password']) ?: $data['password'] = bcrypt($data['password']);
        $user = parent::create(array_only($data, $this->_fillable));
        UserInfo::firstOrCreate(['user_id' => $user->id]);
        return $user;
    }

    /**
     * @param int $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data = [])
    {

        !isset($data['user']['remember_token']) ?: $data['user']['remember_token'] = str_random(10);
        !isset($data['user']['password']) ?: $data['user']['password'] = bcrypt($data['user']['password']);

        $user = parent::update($id, array_only($data['user'], $this->_fillable));

        if (isset($data['info'])) {
            $data['info']['user_id'] = $id;

            $userInfo = UserInfo::firstOrCreate(['user_id' => $id]);
            $userInfo->fill($data['info']);
            $userInfo->save();

            $user->info = $userInfo;
        }


        return $user;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id): bool
    {
        $user = parent::find($id);
        if ($user && $user->delete()) {
            return true;
        } else
            return false;
    }

    public function uploadAvatar($user, $image)
    {
        $filesRepo = new FilesRepository();
        $avatarPath = $filesRepo->uploadUserAvatar($user->email, $image);
        return $avatarPath;
    }


}