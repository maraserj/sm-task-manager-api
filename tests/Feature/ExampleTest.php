<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\CreatesApplication;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use CreatesApplication, DatabaseTransactions;

    public function testApplication()
    {
        $user = User::find(2);

        $this->actingAs($user, 'api')->get('/api/v1/users')->assertStatus(200)->assertJson([
            'status' => 'OK',
            'data' => [
                'email' => $user->email
            ]
        ]);

    }
}
