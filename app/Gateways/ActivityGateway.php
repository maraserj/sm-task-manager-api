<?php
namespace App\Gateways;

use App\Repositories\ActivitiesRepository;

class ActivityGateway extends Gateway
{
    protected $repository;

    /**
     * ActivityGateway constructor.
     * @param ActivitiesRepository $repository
     */
    public function __construct(ActivitiesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $data['activities'] = $this->repository->all();

        return $data;
    }

    /**
     * @param $id
     * @return array
     */
    public function getDirections($id)
    {
        $directions = $this->repository->getDirectionsByActivityId($id);

        return $directions;
    }


    /**
     * @param array $data
     *
     * @return \App\Repositories\BaseRepository|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        $activity = $this->repository->create($data);

        return $activity;
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function show(int $id)
    {
        $activity = $this->repository->find($id);

        return $activity;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function update(array $data = [])
    {
        $activity = $this->repository->update($data['id'], $data['request']);

        return $activity;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id)
    {
        $deleted = $this->repository->delete($id);

        return $deleted;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'calendar_id' => 'required|numeric|not_in:0',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
        ]);

        if ($validator->fails())
            return $validator;
        else
            return null;
    }


}