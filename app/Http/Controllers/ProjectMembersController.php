<?php

namespace App\Http\Controllers;

use App\Gateways\ProjectMembersGateway;
use App\Http\Requests\Project\InviteUserToProjectRequest;
use App\Models\Project;
use App\Models\User;
use App\Notifications\InviteUserToProject;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProjectMembersController extends Controller
{
    protected $gateway;
    protected $user;

    /**
     * ProjectMembersController constructor.
     *
     * @param ProjectMembersGateway $gateway
     */
    public function __construct(ProjectMembersGateway $gateway)
    {

        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
        $this->gateway = $gateway;
    }

    /**
     * @param $projectId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($projectId): JsonResponse
    {
        $members = $this->gateway->getAll($projectId);

        return $this->respondOk($members);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id): JsonResponse
    {
        $member = $this->gateway->create($request->all(), $id);

        if ($member)
            return $this->respondOK($member);
        else
            return $this->respondBadRequest();
    }

    /**
     * @param InviteUserToProjectRequest $request
     * @param                            $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function invite(InviteUserToProjectRequest $request, $id): JsonResponse
    {
        $member = $this->gateway->inviteUser($request->all(), $id);

        if ($member) {
            $user = User::find($member->user_id);
            $project = Project::find($member->project_id);
            if ($user && $project) {
                $user->notify(new InviteUserToProject($project->lead, $project));
            }

            return $this->respondOK($member);
        } else {
            return $this->respondBadRequest();

        }
    }

    /**
     * @param $id
     * @param $memberId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $memberId): JsonResponse
    {
        $member = $this->gateway->show($memberId);
        if (!$member)
            return $this->respondNotFound();
        else
            return $this->respondOk($member);
    }

    /**
     * @param         $id
     *
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = [];
        if ($id) {
            $data['project_id'] = $id;
        }

        if (!isset($data['user_id'])) {
            $data['user_id'] = auth()->id();
        }
        $member = $this->gateway->delete($data);

        if (!$member)
            return $this->respondNotFound();
        else
            return $this->respondOk(null, trans('common.projects_members.deleted'));
    }

}
