<?php
//
//namespace App\Listeners;
//
//use App\Events\TaskEventsNeedsRefresh;
//use App\Models\CalendarEvent;
//use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Queue\ShouldQueue;
//
//class TasksEventListener
//{
//    /**
//     * Create the event listener.
//     *
//     * @return void
//     */
//
//    protected $calendarEvent;
//
//    public function __construct(CalendarEvent $calendarEvent)
//    {
//        $this->calendarEvent = $calendarEvent;
//    }
//
//    /**
//     * Handle the event.
//     *
//     * @param  TaskEventsNeedsRefresh $event
//     * @return void
//     */
//    public function handle(TaskEventsNeedsRefresh $event)
//    {
//        $project = null;
//        if ($event->project) {
//            $project = $event->project;
//
//            $this->refreshAllTasksEvents($project);
//        }
//    }
//
//    /**
//     * @param $project
//     */
//    private function refreshAllTasksEvents($project)
//    {
//        $countEstimate = null;
//        $minPart = null;
//        $allTasksEstimate = null;
//
//        $project->tasks = $project->tasks()->get();
//
//        $tasksCollection = collect($project->tasks);
//        $allTasksEstimate = $tasksCollection->sum('estimate');
//        $tasksCollection->sortByDesc('priority')->pluck('priority');
//        \DB::transaction(function () use ($project, $allTasksEstimate, $minPart) {
//            $this->calendarEvent->deleteEventsByDate($project);
//            info('$allTasksEstimate', [$allTasksEstimate]);
//
//            foreach ($project->tasks as $i => $projectTask) {
//                $events = $this->calendarEvent->getWorkTimeEvents($projectTask->deadline); // берем эвенты в диапазоне даты от текущей до дедлайна таска
//                $taskEstimate = $projectTask->estimate;
//                info('$i', [$i]);
//                info('$taskEstimate', [$taskEstimate]);
//
//                foreach ($events as $j => $event) {
//                    info('$j', [$j]);
//                    $dataCreating = [
//                        'user_id'         => auth()->id(),
//                        'calendar_id'     => $event['calendar_id'],
//                        'date'            => $event['date'],
//                        'available_hours' => '{}',
//                        'is_free'         => false,
//                        'is_for_task'     => true,
//                    ];
//
//                    if (isset($projectTask->min_parts_for_task)) $minPart = $projectTask->min_parts_for_task;
//
//                    $taskEventFromCalendar = $this->calendarEvent->getEventsHoursByDate($event['date']); // сколько часов рабочих на этот день
//                    $tasksHoursInDay = collect($taskEventFromCalendar)->pluck('hours')->sum(); // сколько занято под таски
//                    $freeTimeForWork = $event['hours'] - $tasksHoursInDay; // осталось свободных
////                    dump($freeTimeForWork . '  ||  ' . $minPart);
//                    info('$freeTimeForWork', [$freeTimeForWork]);
//
//                    if (($freeTimeForWork - $minPart) < 0 || $freeTimeForWork == 0) continue; // если времени нет вообще или мин часть таска не влезает, переходим на след день
//
//                    if (count($taskEventFromCalendar)) {
//                        if ($tasksHoursInDay > 0 && $tasksHoursInDay >= $event['hours']) {
//                            continue;
//                        }
//                    }
//                    info('$tasksHoursInDay', [$tasksHoursInDay]);
//                    info('$event[\'hours\']', [$event['hours']]);
//
//                    if ($allTasksEstimate <= 0) {
//                        break 2;
//                    } elseif ($taskEstimate >= 0 && $taskEstimate <= $minPart) {
//                        $dataCreating['hours'] = $taskEstimate >= 0 && $taskEstimate <= $minPart ? $taskEstimate : $minPart;
//                    } elseif ($allTasksEstimate >= 0 && $allTasksEstimate <= $minPart) {
//                        $dataCreating['hours'] = $allTasksEstimate >= 0 && $allTasksEstimate <= $minPart ? $allTasksEstimate : $minPart;
//                    } else {
//                        $dataCreating['hours'] = $taskEstimate >= 0 && $taskEstimate <= $minPart ? $taskEstimate : $minPart;
//                    }
//
//                    $taskEvent = $this->calendarEvent->create($dataCreating);
//                    $projectTask->events()->attach($taskEvent->id);
//                    $projectTask->touch();
//
//                    $taskEstimate = $taskEstimate - $minPart;
//                    $allTasksEstimate = $allTasksEstimate - $minPart;
//
//                    if ($taskEstimate <= 0) break;
//                    continue;
//                }
//            }
//        });
//    }
//}

namespace App\Listeners;

use App\Events\TaskEventsNeedsRefresh;
use App\Models\CalendarEvent;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TasksEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    protected $calendarEvent;

    public function __construct(CalendarEvent $calendarEvent)
    {
        $this->calendarEvent = $calendarEvent;
    }

    /**
     * Handle the event.
     *
     * @param  TaskEventsNeedsRefresh $event
     * @return void
     */
    public function handle(TaskEventsNeedsRefresh $event)
    {
        $project = null;
        if ($event->project) {
            $project = $event->project;

            $this->refreshAllTasksEvents($project);
        }
    }

    /**
     * @param $project
     */
    private function refreshAllTasksEvents($project)
    {
        $countEstimate = null;
        $minPart = null;
        $allTasksEstimate = null;
        $tasksFilter = [];
        $tasksFilter[] = ['urgent', '=' , 0];
        $tasksFilter[] = ['is_daily', '=' , 0];
        $tasksFilter[] = ['deadline', '>=' , Carbon::today()];
        $tasksFilter[] = ['status', '!=' , 'Closed'];

        $project->tasks = $project->tasks()->where($tasksFilter)->get();
        $deadline = !empty($project->deadline) ? $project->deadline : ($project->parentProject ? $project->parentProject->deadline : null);

        $tasksCollection = collect($project->tasks);
        $allTasksEstimate = $tasksCollection->sum('estimate');
        $tasksCollection->sortByDesc('priority')->pluck('priority');
        \DB::transaction(function () use ($project, $allTasksEstimate, $minPart, $deadline) {
            $this->calendarEvent->deleteEventsByDate($project->deadline ? $project : $project->parentProject);

//            $i = 0;
//            $j = 0;
            foreach ($project->tasks as $projectTask) {
//                $i++;
                $events = $this->calendarEvent->getWorkTimeEvents($projectTask->deadline ?? $deadline, $project->activity->calendar_id); // берем эвенты в диапазоне даты от текущей до дедлайна таска
                $taskEstimate = $projectTask->estimate;

                foreach ($events as $event) {
                    $dataCreating = [
                        'user_id'         => auth()->id(),
                        'calendar_id'     => $event['calendar_id'],
                        'date'            => $event['date'],
                        'available_hours' => '{}',
                        'is_free'         => false,
                        'is_for_task'     => true,
                    ];
//                    if ($data['estimate'] <= 0) break;
//                    if ($freeTimeForWork - $minPart >= $data['estimate']) continue;

//                    if ($data['estimate'] > $event['hours'] && $data['min_parts_for_task'] > $event['hours']) continue;
//                    else $data['too_big_estimate'] = 1;


                    if (isset($projectTask->min_parts_for_task)) $minPart = $projectTask->min_parts_for_task;

                    $taskEventFromCalendar = $this->calendarEvent->getEventsHoursByDate($event['date']); // сколько часов рабочих на этот день
                    $tasksHoursInDay = collect($taskEventFromCalendar)->pluck('hours')->sum(); // сколько занято под таски
                    $freeTimeForWork = $event['hours'] - $tasksHoursInDay; // осталось свободных

                    if ($freeTimeForWork <= 0) continue;
                    if ($freeTimeForWork < $minPart) continue;
                    $taskEstimate = $taskEstimate - $minPart;

//                    if (($freeTimeForWork - $minPart) < 0 || $freeTimeForWork == 0) continue; // если времени нет вообще или мин часть таска не влезает, переходим на след день
//
//                    if (count($taskEventFromCalendar)) {
//                        if ($tasksHoursInDay > 0 && $tasksHoursInDay >= $event['hours']) {
//                            continue;
//                        }
//                    }


                    if ($allTasksEstimate <= 0) {
                        break 2;
                    } elseif ($taskEstimate >= 0 && $taskEstimate <= $minPart) {
                        $dataCreating['hours'] = $taskEstimate > 0 && $taskEstimate <= $minPart ? $taskEstimate : $minPart ;
                    } elseif ($allTasksEstimate >= 0 && $allTasksEstimate <= $minPart) {
                        $dataCreating['hours'] = $allTasksEstimate > 0 && $allTasksEstimate <= $minPart ? $allTasksEstimate : $minPart;
                    } else {
                        $dataCreating['hours'] = $minPart;
                    }
//                    $j++;

//                    dump($dataCreating['hours']);
                    $taskEvent = $this->calendarEvent->create($dataCreating);
                    $projectTask->events()->attach($taskEvent->id);

                    $allTasksEstimate = $allTasksEstimate - $minPart;

                    if ($taskEstimate <= 0) break;
                    continue;
                }
            }
//            dump($i, $j);
        });
    }
}
