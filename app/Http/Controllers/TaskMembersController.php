<?php

namespace App\Http\Controllers;

use App\Gateways\TaskMembersGateway;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskMembersController extends Controller
{
    protected $gateway;
    protected $user;

    /**
     * ProjectMembersController constructor.
     * @param TaskMembersGateway $gateway
     */
    public function __construct(TaskMembersGateway $gateway)
    {

        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();
            return $next($request);
        });
        $this->gateway = $gateway;
    }

    /**
     * @param $projectId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($projectId): JsonResponse
    {
        $members = $this->gateway->getAll($projectId);

        return $this->respondOk($members);
    }

    /**
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $id): JsonResponse
    {
        $member = $this->gateway->create($request->all(), $id);

        if ($member)
            return $this->respondOK($member);
        else
            return $this->respondBadRequest();
    }

    /**
     * @param $id
     * @param $memberId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $memberId): JsonResponse
    {
        $member = $this->gateway->show($memberId);
        if (!$member)
            return $this->respondNotFound();
        else
            return $this->respondOk($member);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        $data = $request->all();
        $member = $this->gateway->delete($data);

        if (!$member)
            return $this->respondNotFound();
        else
            return $this->respondOk(null, trans('common.projects_members.deleted'));
    }
}
