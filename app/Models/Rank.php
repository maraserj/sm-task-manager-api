<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Rank
 *
 * @property int $id
 * @property int $calendar_priority
 * @property int $activity_priority
 * @property int $direction_priority
 * @property int $project_priority
 * @property int $task_priority
 * @property int $total_rank
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereActivityPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereCalendarPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereDirectionPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereProjectPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereTaskPriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereTotalRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Rank whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Rank extends Model
{
    protected $fillable = [
        'calendar_priority',
        'activity_priority',
        'direction_priority',
        'project_priority',
        'task_priority',
        'total_rank',
    ];
}
