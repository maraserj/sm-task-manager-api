<?php

namespace App\Notifications\Tasks;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskAssignedToUser extends Notification
{
    use Queueable;

    public $user;
    /**
     * @var
     */
    private $task;
    /**
     * @var Project
     */
    private $project;

    /**
     * Create a new notification instance.
     *
     * @param User    $user
     * @param Task    $task
     * @param Project $project
     */
    public function __construct(User $user, Task $task, Project $project)
    {
        $this->user = $user;
        $this->task = $task;
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.email.dear_user') . ', ' . $this->user->name)
            ->line(__('notification.email.task_assigned').' - ')
            ->action($this->task->title, env('FRONTEND_URL', 'estate.intelweb.com.ua') . '/tasks/'.$this->task->id)
            ->line(__('notification.email.follow_link'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->user->id,
            'user_name' => $this->user->name,
            'task_id' => $this->task->id,
            'project_id' => $this->project->id,
            'task_title' => $this->task->title,
            'project_name' => $this->project->name,
        ];
    }

}
