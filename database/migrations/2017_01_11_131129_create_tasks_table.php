<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->nullable();
            $table->unsignedInteger('activity_id')->nullable();
            $table->unsignedInteger('direction_id')->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('parent_id')->default(0);
            $table->unsignedInteger('assigned_id')->nullable();
            $table->unsignedInteger('author_id')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('estimate')->nullable();
            $table->integer('priority')->default(0);
            $table->dateTime('deadline')->nullable();
            $table->string('status')->default('To Do');
            $table->integer('min_parts_for_task')->nullable();
            $table->integer('max_parts_for_task')->nullable();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
