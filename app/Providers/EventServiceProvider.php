<?php

namespace App\Providers;

use App\Events\CreatingUserWidgetsEvent;
use App\Events\RefreshAllUserTasks;
use App\Events\RefreshTasksRanks;
use App\Events\TaskEventsNeedsRefresh;
use App\Listeners\CreatingUserWidgetsListener;
use App\Listeners\RefreshAllUserTasksEventListener;
use App\Listeners\RefreshTasksRanksListener;
use App\Listeners\TasksEventListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        TaskEventsNeedsRefresh::class => [
            TasksEventListener::class,
        ],
        RefreshAllUserTasks::class => [
            RefreshAllUserTasksEventListener::class
        ],
        RefreshTasksRanks::class => [
            RefreshTasksRanksListener::class
        ],
        CreatingUserWidgetsEvent::class => [
            CreatingUserWidgetsListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
