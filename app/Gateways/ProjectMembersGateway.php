<?php

namespace App\Gateways;

use App\Models\User;
use App\Notifications\UserExitFromProject;
use App\Repositories\ProjectsRepository;

class ProjectMembersGateway extends Gateway
{
    protected $repository;
    protected $user;

    public function __construct(ProjectsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll($projectId)
    {
        $members = $this->repository->getProjectMembers($projectId);

        return $members;
    }

    public function create(array $data = [], int $projectId)
    {
        $data['project_id'] = $projectId;
        $data['user_id'] = $data['user_id'] ?? auth()->id();
        $member = $this->repository->createProjectMember($data);

        return $member;
    }

    public function inviteUser(array $data = [], int $projectId)
    {
        $data['project_id'] = $projectId;

        if (!empty($data['email'])) {
            $data['user_id'] = User::whereEmail($data['email'])->first()->id;
            $member = $this->repository->createProjectMember($data);
        } else {
            $member = null;
        }

        return $member;
    }

    public function show(int $id, $user = null)
    {
        $member = $this->repository->getProjectMember($id);

        return $member;
    }

    public function delete($data)
    {
        $member = null;
        if (isset($data['user_id']) && isset($data['project_id'])) {
            $user = User::find($data['user_id']);
            $project = $this->repository->find($data['project_id']);

            $member = $this->repository->deleteProjectMember($data);
            if ($member) {
                $project->lead->notify(new UserExitFromProject($user, $project->lead, $project));
            }
        }

        return $member;
    }
}