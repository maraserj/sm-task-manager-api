<?php
namespace App\Gateways;

use App\Models\{ Project, Task, User };
use App\Notifications\Tasks\{ TaskAssignedToUser,  TaskCreated,  UserClosedTask,  UserFinishedTask, UserStartedTask };
use App\Repositories\{ FilesRepository,TasksRepository, CalendarsRepository };

class TaskGateway extends Gateway
{
    protected $repository;
    protected $filesRepo;
    protected $calendar;

    public function __construct(TasksRepository $repository, FilesRepository $filesRepository, CalendarsRepository $calendar)
    {
        $this->repository = $repository;
        $this->filesRepo = $filesRepository;
        $this->calendar = $calendar;
    }

    public function getAll($data)
    {
        if (isset($data)) {
            $data['filter']['perPage'] = $data['perPage'] ?? 10;
            $data = $this->repository->getFilteredTasks($data);
        } else {
            $data['perPage'] = $data['perPage'] ?? 10;
            $data = $this->repository->paginate($data['perPage']);
        }

        return $data;
    }

    public function create(array $data = [])
    {
        $task = $this->repository->create($data);
        if ($task->project->project_lead != $task->author_id) {
            $task->project->lead->notify(new TaskCreated($task->author, $task, $task->project));
        }

        return $task;
    }

    public function getProject($id)
    {
        return Project::find($id);
    }

    public function show(int $id)
    {
        $task = $this->repository->getTaskWithFullInfo($id);

        return $task;
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function update(array $data = [])
    {
        $oldAssignedId = null;
        $oldStatus = null;
        $newStatus = null;

        $oldTask = $this->repository->find($data['id']);
        $oldStatus = $oldTask->status;
        $task = $this->repository->update($data['id'], $data['request']);
        $newStatus = $task->status;
        $statuses = $this->repository->getTasksStatuses();

        if (isset($task->author) && $task->author_id != $task->assigned_id && $task->author_id != auth()->id()) {
            $assigned = $task->assigned;
            if (!$assigned) {
                $assigned = auth()->user();
            }
            if ($oldStatus == $statuses['todo'] && $newStatus == $statuses['in_progress']) {
                info($task->assigned);
                $task->author->notify(new UserStartedTask($assigned, $task, $task->project));
            } else if ($oldStatus == $statuses['in_progress'] && $newStatus == $statuses['done']) {
                $task->author->notify(new UserFinishedTask($assigned, $task, $task->project));
            } else if ($oldStatus == $statuses['done'] && $newStatus == $statuses['closed']) {
                $task->assigned->notify(new UserClosedTask($task->author, $task, $task->project));
            }
        }


        if (! empty($data['request']['assigned_id'])
            && $data['request']['assigned_id'] != $oldTask->assigned_id
            && $data['request']['assigned_id'] != auth()->id()
            && $oldTask->status == 'To Do'
            && !empty($data['request']['status'])
            && $data['request']['status'] == 'In progress') {
            if ($task) {
                $assignedId = $data['request']['assigned_id'];

                $user = User::find($task->project->lead->id);
                $user->notify(new TaskAssignedToUser($user, $task, $task->project));
            }
        }


        return $task;
    }

    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    public function updateRanks(array $data = [])
    {
        if (! empty($data)) {
            return $this->repository->updateRanks($data);
        } else {
            return null;
        }
    }

    public function getTasksEvents($id)
    {
        if(!$id) $id = $this->calendar->getMain()->id;

        $tasks = $this->repository->getTasksEvents($id);

        return $tasks;
    }

    public function getComments($id)
    {
        $comments = $this->repository->getTasksComments($id);

        return $comments;
    }

    public function storeAttachments(Task $task, array $data)
    {
        if ($data['file']) {
            $fileAttributes = $this->filesRepo->uploadAttachments($data['file'], $task->id, 'task');

            if (isset($fileAttributes[0]) && !empty($fileAttributes)) {
                $fileAttributes[0]['user_id'] = auth()->id();
                $attachment = $task->attachments()->create($fileAttributes[0]);
            } else {
                $attachment = null;
            }

            return $attachment;
        }
    }

    public function deleteAttachments(Task $task, $attachmentId)
    {
        $attachment = $task->attachments()->where('id', $attachmentId)->first();
        if ($attachment) {
            if ($this->filesRepo->deleteFile($attachment->path)) {
                $task->attachments()->where('id', $attachmentId)->delete();
                return true;
            }
        }
        return null;
    }

    public function getAttachments(int $id, array $data = null)
    {
        $task = $this->repository->find($id);

        return $task ? $task->attachments()->with('user')->orderBy('created_at', 'desc')->get() : null;
    }

    public function getAttachment(int $id, array $data = null)
    {
        return $this->filesRepo->getAttachments($id, 'task');
    }

    public function getTasksStatistics()
    {
        return $this->repository->getTasksStatistics();
    }

    public function getTasksStatisticsByActivity()
    {
        return $this->repository->getTasksStatisticsByActivity();
    }
}