<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Widget;

class WidgetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $widgets = [
            'ProjectsWithNearDeadline',
            'ProjectsLastComments',
            'TasksStatistics',
            'AllNotifications',
            'TasksLastComments',
            'RecommendedTodayTasks'
        ];

        $users = User::all();
        $widgetItems = Widget::all();


//        foreach ( $widgets as $widget){
//            DB::table('widgets')->insert([
//                'name' => $widget,
//            ]);
//        }

        foreach ($users as $user) {
            foreach ($widgetItems as $item) {
                DB::table('user_widgets')->insert([
                    'user_id' => $user->id,
                    'widget_id' => $item->id,
                    'selected' => 0
                ]);
            }
        }


    }
}
