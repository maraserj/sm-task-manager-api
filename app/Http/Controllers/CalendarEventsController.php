<?php

namespace App\Http\Controllers;

use App\Events\RefreshAllUserTasks;
use App\Gateways\CalendarEventGateway;
use App\Http\Requests\CalendarEvent\StoreCalendarEventRequest;
use App\Http\Requests\CalendarEvent\UpdateCalendarEventRequest;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CalendarEventsController extends Controller
{
    protected $gateway;
    private $user;

    /**
     * CalendarEventsController constructor.
     *
     * @param CalendarEventGateway $gateway
     */
    public function __construct(CalendarEventGateway $gateway)
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
        $this->gateway = $gateway;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->gateway->getAll();

        if (!$data)
            return $this->respondNotFound();
        else
            return $this->respondOk($data);
    }

    /**
     * @param StoreCalendarEventRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreCalendarEventRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $calendarEvent = $this->gateway->create($data);

        if ($calendarEvent)
            return $this->respondCreated($calendarEvent, trans('common.events.created'));
        else
            return $this->respondBadRequest(trans('common.events.not_created'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $calendarEvent = $this->gateway->show($id);
        $this->authorize('view', $calendarEvent);

        if (!$calendarEvent)
            return $this->respondNotFound();
        else
            return $this->respondOk($calendarEvent);
    }

    /**
     * @param Request $request
     * @param         $calendarId
     *
     * @return JsonResponse
     */
    public function getWorkTime(Request $request, $calendarId = null): JsonResponse
    {
        $data = $this->gateway->getWorkTime($this->user->id, $calendarId, $request->all());

        if (!$data)
            return $this->respondNotFound();
        else
            return $this->respondOk($data);
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return JsonResponse
     * @internal param Request $request
     */
    public function getUserTasksEvents(Request $request, $id = null): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $events = $this->gateway->getUserTasksEvents($id, $data);
        if (!$events) {
            return $this->respondNotFound();
        } else {
            return $this->respondOK($events);
        }
    }

    /**
     * @param UpdateCalendarEventRequest $request
     * @param                            $id
     *
     * @return JsonResponse
     */
    public function update(UpdateCalendarEventRequest $request, $id): JsonResponse
    {
        $data['id'] = $id;
        $data['request'] = $request->except(['_token', '_method']);

        $calendarEvent = $this->gateway->update($data);

        if ($calendarEvent)
            return $this->respondOK($calendarEvent, trans('common.events.updated'));
        else
            return $this->respondBadRequest(trans('common.events.not_updated'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = $this->gateway->show($id);

        $this->authorize('delete', $data);

        if ($this->gateway->delete($id)) {
            event(new RefreshAllUserTasks($this->user));

            return $this->respondOK(trans('common.events.deleted'));
        } else
            return $this->respondNotFound();
    }
}
