<?php
/**
 * @var \Illuminate\Routing\Router $router
 */
$router->post('/socials/{provider}', 'Auth\SocialAuthController@socialAuthByProvider');
$router->group(['middleware' => 'web'], function () use ($router) {
    $router->get('/facebook-redirect', 'Auth\SocialAuthController@facebookRedirect');
    $router->get('/facebook-callback', 'Auth\SocialAuthController@facebookCallback');

    $router->get('/google-redirect', 'Auth\SocialAuthController@googleRedirect');
    $router->get('/google-callback', 'Auth\SocialAuthController@googleCallback');
});
$router->post('users', ['as' => 'users.store', 'uses' => 'UsersController@store']);

$router->post('password/reset', ['as' => 'password.api.reset', 'uses' => 'Auth\ResetPasswordController@apiReset']);
$router->post('password/email', ['as' => 'password.email.send', 'uses' => 'Auth\ForgotPasswordController@apiSendResetLinkEmail']);

$router->group(['middleware' => 'auth:api'], function () use ($router) {
    $router->resource('google-calendar', 'GoogleCalendarController');
    $router->put('notifications', ['as' => 'notifications.update', 'uses' => 'NotificationsController@update']);
    $router->resource('notifications', 'NotificationsController', ['except' => ['create', 'store', 'show', 'update']]);

    $router->get('activities/{activity_id}/directions', ['as' => 'activities.directions', 'uses' => 'ActivitiesController@getDirections']);
    $router->resource('activities', 'ActivitiesController');

    $router->get('directions/{direction_id}/projects', ['as' => 'activities.projects', 'uses' => 'DirectionsController@getProjects']);
    $router->resource('directions', 'DirectionsController');

    $router->resource('calendar-types', 'CalendarTypesController');
    $router->resource('calendars', 'CalendarsController');

    $router->get('calendar-events/tasks/{id?}', 'TasksController@getTasksEvents');
    $router->get('calendar-events/work-time/{id?}', 'CalendarEventsController@getWorkTime');
    $router->get('calendar-events/tasks-events/{id?}', 'CalendarEventsController@getUserTasksEvents');
    $router->resource('calendar-events', 'CalendarEventsController');
    $router->delete('ranks/reset', 'RanksController@reset');
    $router->post('ranks/update-ranks', 'RanksController@updateRanks');
    $router->resource('ranks', 'RanksController');

    $router->get('tasks/{id}/members/{memberId}', ['as' => 'tasks.members.member', 'uses' => 'TaskMembersController@show']);
    $router->get('tasks/{id}/members', ['as' => 'tasks.members.all', 'uses' => 'TaskMembersController@index']);
    $router->post('tasks/{id}/members', ['as' => 'tasks.members.store', 'uses' => 'TaskMembersController@store']);
    $router->delete('tasks/{id}/members', ['as' => 'tasks.members.destroy', 'uses' => 'TaskMembersController@destroy']);

    $router->post('tasks/update-ranks', ['as' => 'tasks.ranks.update', 'uses' => 'TasksController@updateRanks']);
    $router->post('tasks/{id}/attachments', ['as' => 'tasks.attachments.store', 'uses' => 'TasksController@storeAttachments']);
    $router->delete('tasks/{id}/attachments/{attachmentId}', ['as' => 'tasks.attachments.delete', 'uses' => 'TasksController@deleteAttachment']);
    $router->get('tasks/{id}/attachments', ['as' => 'tasks.attachments.index', 'uses' => 'TasksController@attachments']);
    $router->get('tasks/{id}/comments', ['as' => 'tasks.comments.index', 'uses' => 'TasksController@getComments']);
    $router->resource('tasks', 'TasksController');

    $router->post('projects/{id}/attachments', ['as' => 'projects.attachments.store', 'uses' => 'ProjectsController@storeAttachments']);
    $router->delete('projects/{id}/attachments/{attachmentId}', ['as' => 'projects.attachments.delete', 'uses' => 'ProjectsController@deleteAttachment']);
    $router->get('projects/{id}/attachments', ['as' => 'projects.attachments.index', 'uses' => 'ProjectsController@attachments']);
    $router->get('projects/{id}/comments', ['as' => 'projects.comments.index', 'uses' => 'ProjectsController@getComments']);
    $router->get('projects/{id}/tasks-events', ['as' => 'projects.tasks.events', 'uses' => 'ProjectsController@tasksEvents']);
    $router->get('projects/{id}/members/{memberId}', ['as' => 'projects.members.member', 'uses' => 'ProjectMembersController@show']);
    $router->get('projects/{id}/members', ['as' => 'projects.members.all', 'uses' => 'ProjectMembersController@index']);
    $router->post('projects/{id}/members', ['as' => 'projects.members.store', 'uses' => 'ProjectMembersController@store']);
    $router->post('projects/{id}/members/invite', ['as' => 'projects.members.invite', 'uses' => 'ProjectMembersController@invite']);
    $router->delete('projects/{id}/members', ['as' => 'projects.members.destroy', 'uses' => 'ProjectMembersController@destroy']);

    $router->get('projects/{id}/tasks', ['as' => 'projects.tasks', 'uses' => 'ProjectsController@getTasks']);
    $router->get('projects/{id}/sub-projects', ['as' => 'projects.sub_projects', 'uses' => 'ProjectsController@getSubProjects']);
    $router->resource('projects', 'ProjectsController');

    $router->get('users/available', ['as' => 'users.available', 'uses' => 'UsersController@getAvailable']);
    $router->post('users/upload-avatar', ['as' => 'users.upload_avatar', 'uses' => 'UsersController@uploadAvatar']);
    $router->post('users/save-worktime', ['as' => 'users.save_worktime', 'uses' => 'UsersController@saveWorkingTime']);
    $router->put('users/{id?}', ['as' => 'users.update', 'uses' => 'UsersController@update']);
    $router->resource('users', 'UsersController', ['except' => ['create', 'store', 'update', 'show']]);

    $router->resource('comments', 'CommentsController');
    $router->get('send-notify', 'HomeController@send');

    $router->get('widget/tasks-statistics', ['as' => 'widget.tasks_statistics', 'uses' => 'TasksController@getTasksStatistics']);
    $router->get('widget/tasks-by-activity', ['as' => 'widget.tasks_by_activity', 'uses' => 'TasksController@getTasksStatisticsByActivity']);
    $router->get('user/widgets', ['as' => 'user.widgets', 'uses' => 'UsersController@getWidgets']);
    $router->get('profile/widgets', ['as' => 'profile.widgets', 'uses' => 'UsersController@getProfileWidgets']);
    $router->post('profile/widgets', ['as' => 'profile.widgets', 'uses' => 'UsersController@updateWidget']);
    // TODO add except
});
