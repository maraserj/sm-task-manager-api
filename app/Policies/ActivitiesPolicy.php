<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Activity;
use Illuminate\Auth\Access\HandlesAuthorization;

class ActivitiesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the activity.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Activity $activity
     *
     * @return boolean
     */
    public function view(User $user, Activity $activity): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || $user->id === $activity->user_id;
    }

    /**
     * Determine whether the user can create activities.
     *
     * @return mixed
     */
    public function create(): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the activity.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Activity $activity
     *
     * @return mixed
     */
    public function update(User $user, Activity $activity): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || $user->id === $activity->user_id;
    }

    /**
     * Determine whether the user can delete the activity.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Activity $activity
     *
     * @return mixed
     */
    public function delete(User $user, Activity $activity): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || $user->id === $activity->user_id;
    }
}
