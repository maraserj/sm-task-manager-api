<?php

namespace App\Http\Requests\Project;

use App\Http\Requests\Request;
use App\Models\CalendarEvent;
use App\Models\Project;

class UpdateProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $project = Project::find($this->route('project'));
        return $project && $this->user()->can('update', $project);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userLastWorkDay = CalendarEvent::latestUserEventDate();
        $rules = [
            'parent_id'    => 'numeric',
            'name'         => 'required',
            'direction_id' => 'required|numeric',
            'description'  => 'string|nullable',
        ];

        if (!isset($userLastWorkDay)) {
            $rules['deadline'] = 'required_without:parent_id|required_if:parent_id,0|date';
            $rules['need_to_fill_calendar'] = 'required';
        } else {
            $rules['deadline'] = 'required_without:parent_id|required_if:parent_id,0|date|before:' . $userLastWorkDay;

        }

        return $rules;
    }

    public function messages()
    {
        $messages = parent::messages();

        $messages['need_to_fill_calendar.required'] = 'Сначала необходимо заполнить рабочий календарь в профиле. Без дедлайна Вы не можете создать проект.';
        $messages['deadline.required_without'] = 'Поле конечная дата обязательно для заполнения.';

        return $messages;
    }
}
