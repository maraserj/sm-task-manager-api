<?php

namespace App\Transformers;

use App\Models\Project;
use Carbon\Carbon;

class ProjectTransformer extends Transformer
{

    protected $availableIncludes = [
        'tasks'
    ];

    /**
     * @param Project $project
     * @return array
     */
    public function transform(Project  $project)
    {
        Carbon::setLocale('ru');


        return [
            'id' => $project->id,
            'parent_id' => $project->parent_id,
            'name' => $project->name,
            'activity_id' => $project->activity_id,
            'direction_id' => $project->direction_id,
            'project_lead' => $project->project_lead,
            'description' => $project->description,
            'deadline' => $project->deadline->format('Y-m-d'),
            'status' => $project->status,
            'priority' => $project->priority,
            'color' => $project->color,
            'tasks' => $project->tasks,
            'tasks_count' => $project->tasks_count,
            'comments_count' => $project->comments_count,
            'created_at' => $project->created_at->format('Y-m-d'),
        ];
    }

    /**
     * @param Project $project
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTasks(Project $project)
    {
        $tasks = $project->tasks()->with([
            'events', 'activity', 'direction', 'project',
        ])->get();

        return $this->collection($tasks, new TaskTransformer);
    }
}