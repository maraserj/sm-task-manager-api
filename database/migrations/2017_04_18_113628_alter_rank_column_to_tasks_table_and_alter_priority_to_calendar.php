<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRankColumnToTasksTableAndAlterPriorityToCalendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->date('date_start')->after('priority')->nullable();
            $table->integer('rank')->after('priority')->nullable();
            $table->integer('order')->after('priority')->nullable();
        });
        Schema::table('calendars', function (Blueprint $table) {
            $table->integer('priority')->after('user_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('date_start');
            $table->dropColumn('rank');
            $table->dropColumn('order');
        });
        Schema::table('calendars', function (Blueprint $table) {
            $table->dropColumn('priority');
        });
    }
}
