<?php
namespace App\Repositories;

use App\Models\Calendar;
use Illuminate\Contracts\Validation\Validator;

class CalendarsRepository extends BaseRepository
{
    protected $_fillable = [];

    /**
     * CalendarsRepository constructor.
     * @param Calendar $model
     */
    public function __construct(Calendar $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->model->where('user_id', auth()->id())->with(['type'])->get();
    }

    public function getMain($id = null)
    {
        if(!$id) $id = auth()->id();

        return $this->model->where([['user_id', $id],['isMain', 1]])->first();
    }

    public function saveWorkingTime(array $data = [])
    {
        $data['user_id'] = $data['user_id'] ?? auth()->id();
        if (isset($data['calendar_id'])) {
            $main = $this->find($data['calendar_id']);
        } else {
            $main = $this->getMain($data['user_id']);
        }

        if($main) {
            return $this->update($main->id, array_only($data, $this->_fillable));
        } else {
            $data['isMain'] = 1;
            return $this->model->firstOrCreate(array_only($data, $this->_fillable));
        }
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'type_id' => 'required',
        ]);

        if ($validator->fails()) return $validator;
        else return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $validator = validator($data, [
            'name' => 'required|max:100',
            'type_id' => 'required',
        ]);

        if ($validator->fails()) return $validator;
        else return null;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function firstOrCreate(array $data = [])
    {
        isset($data['user_id']) ?: $data['user_id'] = auth()->id();
        return $this->model->firstOrCreate(array_only($data, $this->_fillable));
    }

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data = [])
    {
        isset($data['user_id']) ?: $data['user_id'] = auth()->id();
        $calendars = $this->model->where('user_id', $data['user_id'])->count();

        if ($calendars <= 0) {
            $data['isMain'] = 1;
        }

        return parent::create(array_only($data, $this->_fillable));
    }

    /**
     * @param int $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data = [])
    {
        isset($data['user_id']) ?: $data['user_id'] = auth()->id();
        if (isset($data['isMain']) && isset($data['type_id'])) {
            $calendars = $this->model->where([['user_id', '=', auth()->id()], ['isMain', '=', true], ['type_id', '=', $data['type_id']]])->get();

            foreach ($calendars as $calendar) {
                $calendar->update(['isMain' => false]);
            }
        }
        return parent::update($id, array_only($data, $this->_fillable));
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id): bool
    {
        $calendar = parent::find($id);
        if ($calendar && $calendar->delete()) {
            return true;
        } else return false;
    }

}