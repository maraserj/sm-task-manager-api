<?php
namespace App\Gateways;

use App\Models\Project;
use App\Models\Task;
use App\Transformers\ProjectTransformer;
use League\Fractal\Manager as FractalManager;
use App\Repositories\{
    FilesRepository, TasksRepository, ProjectsRepository
};

use League\Fractal\Resource\ {
    Collection as FractalCollection,
    Item as FractalItem
};

class ProjectsGateway extends Gateway
{
    protected $repository;
    protected $tasksRepo;
    protected $filesRepo;
    protected $user;
    protected $fractal;
    protected $fractalItem;
    protected $fractalCollection;
    protected $transformer;

    /**
     * ProjectsGateway constructor.
     * @param ProjectsRepository $repository
     * @param TasksRepository $tasksRepository
     * @param FilesRepository $filesRepository
     * @param ProjectTransformer $transformer
     * @param FractalCollection $fractalCollection
     * @param FractalItem $fractalItem
     */
    public function __construct(ProjectsRepository $repository,
                                TasksRepository $tasksRepository,
                                FilesRepository $filesRepository,
                                ProjectTransformer $transformer,
                                FractalCollection $fractalCollection,
                                FractalItem $fractalItem)
    {
        $this->repository = $repository;
        $this->tasksRepo = $tasksRepository;
        $this->filesRepo = $filesRepository;
        $this->fractalCollection = $fractalCollection;
        $this->transformer = $transformer;
        $this->fractalItem = $fractalItem;
        $this->fractal = new FractalManager();
    }

    public function getAll($data)
    {
        if (isset($data) && (isset($data['sort']) || isset($data['filter']))) {
            $data = $this->repository->getSortedProjects($data);
        } elseif (isset($data) && isset($data['for_deadline'])) {
            $data = $this->repository->getWithNearDeadline();
        } else {
            $data = $this->repository->all();
        }

//        $response = new $this->fractalCollection($data, new $this->transformer);
//        $this->fractal->parseIncludes(['tasks']);

        return $data;
    }

    public function create(array $data = [])
    {
        $project = $this->repository->create($data);

        return $project;
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function show(int $id)
    {
        $project = $this->repository->findWithMembers($id);

        return $project;
    }

    public function getSubProjects(array $data = [], int $id)
    {
        $project = $this->repository->getChildren($data, $id);

        return $project;
    }

    public function getProjectMembers($user, $id)
    {
        return $user->projects()->where('project_id', $id)->first();
    }

    public function getTasks($filterParams, $childrenProjects = null)
    {
        $tasks = $this->tasksRepo->getFilteredTasks($filterParams, $childrenProjects);

        return $tasks;
    }

    public function update(array $data = [])
    {
        $project = $this->repository->update($data['id'], $data['request']);

        return $project;
    }

    public function getInstance()
    {
        return new Task();
    }

    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    public function getComments($project)
    {
        $comments = $project->comments()->where('parent_id', null)->with(['user', 'children'])->orderBy('created_at', 'DESC')->get();
//        $data = new $this->fractalCollection($comments, new CommentsTransformer());

        return $comments;
    }

    public function storeAttachments(int $id, array $data)
    {
        if (!empty($data['file'])) {
            $fileAttributes = $this->filesRepo->uploadAttachments($data['file'], $id, 'project');

            return $fileAttributes;
        } else {
            return null;
        }
    }

    public function deleteAttachments(Project $project, $attachmentId)
    {
        $attachment = $project->attachments()->where('id', $attachmentId)->first();
        if ($attachment) {
            if ($this->filesRepo->deleteFile($attachment->path)) {
                $project->attachments()->where('id', $attachmentId)->delete();
                return true;
            }
        }
        return null;
    }

    public function getAttachments(int $id, array $data = null)
    {
        $project = $this->repository->find($id);

        return $project ? $project->attachments()->with('user')->orderBy('created_at', 'desc')->get() : null;
    }

    public function getAttachment(int $id, array $data = null)
    {
        return $this->filesRepo->getAttachments($id, 'project');
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnCreate(array $data = [])
    {
        $validator = validator($data, $this->repository->getCreateRules());

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator|null
     */
    public function validateOnUpdate(array $data = [])
    {
        $lastDayUserWorkTime = $this->repository->getLastUserEvent();
        $this->repository->setUpdateRules(['deadline' => 'before:'.$lastDayUserWorkTime]);
        $validator = validator($data, $this->repository->getUpdateRules());

        if ($validator->fails())
            return $validator;
        else
            return null;
    }

}