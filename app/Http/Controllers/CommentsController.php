<?php

namespace App\Http\Controllers;

use App\Gateways\CommentsGateway;
use App\Http\Requests\Comment\StoreCommentRequest;
use App\Http\Requests\Comment\UpdateCommentRequest;
use App\Models\Project;
use App\Models\Task;
use App\Notifications\NewCommentInProject;
use App\Notifications\NewCommentInTask;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class CommentsController extends Controller
{
    protected $gateway;
    protected $user;

    public function __construct(CommentsGateway $gateway)
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
        $this->gateway = $gateway;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $comments = $this->gateway->index(5, $this->user);

        return $this->respondOK($comments);
    }

    /**
     * @param StoreCommentRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCommentRequest $request): JsonResponse
    {
        $user = null;
        $targetClass = null;
        $data = $request->except(['_token', '_method']);

        if ($data['target'] == 'project') {
            $targetClass = NewCommentInProject::class;
            $target = Project::find($data['targetId']);
            $user = $target->lead;
        } elseif ($data['target'] == 'task') {
            $targetClass = NewCommentInTask::class;
            $target = Task::find($data['targetId']);
            $user = $target->author;
        }

        $comment = $this->gateway->create($data);

        if ($comment) {

            if ($user && $targetClass  && $comment->user_id != $user->id) {
                $user->notify(new $targetClass($user, $target));
            }

            return $this->respondCreated($comment, trans('common.comments.created'));
        } else {
            return $this->respondBadRequest(trans('common.comments.not_created'));
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $comment = $this->gateway->show($id);
        if (!$comment)
            return $this->respondNotFound();
        else
            return $this->respondOk($comment);
    }

    /**
     * @param UpdateCommentRequest $request
     * @param                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCommentRequest $request, $id): JsonResponse
    {
        $data['id'] = $id;
        $data['request'] = $request->all();

        $comment = $this->gateway->update($data);

        if ($comment)
            return $this->respondOk($comment, trans('common.comments.updated'));
        else
            return $this->respondBadRequest(trans('common.comments.not_updated'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $this->authorize('admin');
        $comment = $this->gateway->delete($id);

        if (!$comment)
            return $this->respondNotFound();
        else
            return $this->respondOk(null, trans('common.comments.deleted'));
    }
}
