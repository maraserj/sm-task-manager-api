<?php

namespace App\Notifications\Tasks;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TaskCreated extends Notification
{
    use Queueable;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Project
     */
    private $project;
    /**
     * @var Task
     */
    private $task;

    /**
     * Create a new notification instance.
     *
     * @param User    $user
     * @param Task    $task
     * @param Project $project
     */
    public function __construct(User $user, Task $task, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
        $this->task = $task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.email.dear_user') . ', ' . $this->user->name)
            ->line(__('notification.email.task_created', ['username' => $this->user->name,'projectname' => $this->project->name]).' - ')
            ->action($this->task->title, env('FRONTEND_URL', 'estate.intelweb.com.ua') . '/tasks/'.$this->task->id)
            ->line(__('notification.email.follow_link'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_name' => $this->user->name,
            'user_id' => $this->user->id,
            'task_id' => $this->task->id,
            'task_title' => $this->task->title,
            'project_id' => $this->project->id,
            'project_name' => $this->project->name,
        ];
    }
}
