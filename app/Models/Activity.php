<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Activity
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $calendar_id
 * @property string $name
 * @property string $slug
 * @property int $status
 * @property int|null $order
 * @property int|null $priority
 * @property string|null $group
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Calendar $calendar
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Direction[] $directions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Activity whereUserId($value)
 * @mixin \Eloquent
 */
class Activity extends Model
{
    protected $table = 'activities';

    protected $fillable = [
        'name',
        'user_id',
        'calendar_id',
        'slug',
        'status',
        'order',
        'group',
        'priority',
    ];

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return HasOne
     */
    public function calendar(): HasOne
    {
        return $this->hasOne(Calendar::class, 'id', 'calendar_id');
    }

    /**
     * @return HasMany
     */
    public function directions(): HasMany
    {
        return $this->hasMany(Direction::class, 'activity_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function projects(): HasMany
    {
        return $this->hasMany(Project::class, 'activity_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'activity_id', 'id');
    }
}
