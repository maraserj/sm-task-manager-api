<?php

namespace App\Http\Controllers;

use App\Events\CreatingUserWidgetsEvent;
use App\Events\RefreshAllUserTasks;
use App\Http\Requests\User\AvatarUploadUserRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Models\UserInfo;
use App\Repositories\{
    CalendarsRepository, CalendarEventsRepository, FilesRepository,UserWidgetsRepository
};
use App\Gateways\UserGateway;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UsersController extends Controller
{
    protected $user;
    protected $calendarRepo;
    protected $eventRepo;
    protected $filesRepo;
    protected $widgetsRepo;
    /**
     * @var UserGateway
     */
    private $gateway;

    /**
     * UsersController constructor.
     *
     * @param UserGateway $gateway
     * @param CalendarsRepository $calendar
     * @param CalendarEventsRepository $event
     * @param FilesRepository $filesRepository
     * @param UserWidgetsRepository $widgetRepository
     */
    public function __construct(
        UserGateway $gateway,
        CalendarsRepository $calendar,
        CalendarEventsRepository $event,
        FilesRepository $filesRepository,
        UserWidgetsRepository $widgetRepository
    )
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
        $this->calendarRepo = $calendar;
        $this->eventRepo = $event;
        $this->filesRepo = $filesRepository;
        $this->gateway = $gateway;
        $this->widgetsRepo = $widgetRepository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $users = $this->gateway->index($request->except(['_token', '_method']));

        return $this->respondOK($users);
    }

    public function getAvailable(Request $request)
    {
        $users = $this->gateway->getAvailable($request->except(['_token', '_method']));

        return $this->respondOK($users);
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $user = $this->gateway->create($data);

        if ($user) {
            event(new CreatingUserWidgetsEvent($user));
            return $this->respondCreated($user, trans('common.users.created'));
        } else
            return $this->respondBadRequest(trans('common.users.not_created'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id = null): JsonResponse
    {
        if (!$id) $id = auth()->id();
        $user = $this->gateway->show($id);

        return $this->respondOk($user);
    }

    /**
     * @param UpdateUserRequest $request
     * @param                   $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, $id = null): JsonResponse
    {
        $data['id'] = $id = $this->user->id;
        $data['user'] = $request['user'];
        $data['info'] = $request['info'];
        $user = $this->gateway->update($data);

        if ($user) {
            return $this->respondOK($user, trans('common.users.updated'));
        } else
            return $this->respondBadRequest(trans('common.users.not_updated'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $this->authorize('admin');
        if ($this->gateway->delete($id))
            return $this->respondOK(trans('common.users.deleted'));
        else
            return $this->respondNotFound(trans('common.users.not_deleted'));

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveWorkingTime(Request $request): JsonResponse
    {
        $responseData = [];
        $endOfYear = Carbon::parse($request->repeat_to ?? '2017-12-31');

        $startFrom = (isset($request->repeat_from) ? Carbon::parse($request->repeat_from) : null) ?? Carbon::today();

        $validator = validator($request->all(), [
            'days'        => 'required',
            'calendar_id' => 'required|not_in:0',
        ], [
            'calendar_id.not_in' => 'Выберите календарь из вашего списка календарей.',
        ]);

        if ($validator && $validator->fails()) {
            return $this->respondInvalidation($validator->errors());
        }

        if (isset($request->calendar_id) && $request->calendar_id != 0) {
            $calendar = $this->calendarRepo->find($request->calendar_id);
        }
        if (!isset($calendar)) {
            $data['calendar'] = $request->except(['_token', '_method']);
            $data['calendar']['user_id'] = $data['user_id'] ?? auth()->id();
            $data['calendar']['type_id'] = $data['type_id'] ?? 1;
            $data['calendar']['isMain'] = 1;
            $calendar = $this->calendarRepo->saveWorkingTime($data['calendar']);
        }

        $userInfo = UserInfo::firstOrCreate(['user_id' => auth()->id()]);
        $userInfo->fill([
            'work_days' => json_encode([
                'days'          => collect($request['days'])->toArray(),
                'calendar_id'   => $calendar->id,
                'calendar_name' => $calendar->name,
                'calendar_type' => $calendar->type_id,
                'repeat_to'     => $endOfYear->format('Y-m-d'),
                'hours'         => $request['hours'],
            ]),
        ]);
        $userInfo->save();

        $responseData['calendar'] = $calendar;
        $data['events']['calendar_id'] = $calendar->id;
        $data['events']['user_id'] = $this->user->id;
        $data['events']['hours'] = $request['hours'];
        $data['events']['is_free'] = $request['is_free'] ?? true;

        $validator = $this->eventRepo->validateOnCreate($data['events']);
        if ($validator && $validator->fails()) {
            return $this->respondInvalidation($validator->errors());
        } else {
            $workDaysOld = collect($request['days'])->toArray();
            $workDays = [];
            foreach ($workDaysOld as $workDay) {
                if ($workDay == 7) $workDay = 0;
                $workDays[] = $workDay;
            }

            $this->eventRepo->getModel()
                ->where('date', '>=', $startFrom)
                ->where('calendar_id', $calendar->id)
                ->whereDoesntHave('tasks', function ($query) {
                    $query->where('is_daily', 1);
                })
                ->whereHas('calendar', function ($query) {
                    $query->where('user_id', auth()->id());
                })
                ->delete();
            for ($dayStart = $startFrom; $dayStart <= $endOfYear; $dayStart->addDay()) {

                $dayOfWeek = Carbon::parse($dayStart)->dayOfWeek;
                if (in_array($dayOfWeek, $workDays)) {
                    $data['events']['date'] = $dayStart;
                    $event = $this->eventRepo->create($data['events']);
                    $responseData['events'][$event->id] = $event;
                }
            }
            event(new RefreshAllUserTasks($this->user));

            return $this->respondCreated($responseData, trans('common.users.work_time_saved'));
        }
    }

    /**
     * @param AvatarUploadUserRequest $request
     *
     * @return JsonResponse
     */
    public function uploadAvatar(AvatarUploadUserRequest $request): JsonResponse
    {
        $uploadedAvatar = $this->gateway->uploadAvatar($request->file('file'), $this->user);

        if ($uploadedAvatar) {
            return $this->respondOK($uploadedAvatar);
        } else
            return $this->respondBadRequest();
    }

    public function getWidgets(Request $request): JsonResponse
    {
        $response = [];
        $widgets = $this->widgetsRepo->all()->with('widgets')->where('selected', 1)->get();
        for($i=0; $i< $widgets->count(); $i++){
            $response[] = $widgets[$i];
            $response[$i]['name'] = $widgets[$i]->widgets[0]->name;
        }

        //dd($response);
//
//        foreach ($widgets as $widget){
//            $response = $widget;
//            $response['name'] = $widget->widgets[0]->name;
//        }
        return $this->respondOk($response);
    }

    public function getProfileWidgets()
    {
        $response = [];
        $widgets = $this->widgetsRepo->all()->with('widgets')->get();
        for($i=0; $i< $widgets->count(); $i++){
            $response[] = $widgets[$i];
            $response[$i]['name'] = $widgets[$i]->widgets[0]->name;
        }
        return $this->respondOk($response);
    }

    public function updateWidget(Request $request)
    {
        $widget = $request;
        $this->widgetsRepo->updateWidgetState($widget);
        return $this->respondOk("Виджет успешно обновлен");
    }
}
