<?php

namespace App\Http\Controllers;

use App\Gateways\RanksGateway;
use App\Models\Rank;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class RanksController extends Controller
{
    public $gateway;

    public function __construct(RanksGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function index()
    {
        $ranks = $this->gateway->index();

        return $this->respondOK($ranks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $rank = Rank::find($id);

        if ($rank) {
            $rank->fill($request->all());
            if ($rank->save()) {
                return $this->respondOK($rank);
            } else {
                return $this->respondBadRequest();
            }
        } else {
            return $this->respondNotFound();
        }
    }

    public function updateRanks(Request $request)
    {
        $ranks = $this->gateway->updateRanks($request->except(['_token', '_method']));
        if ($ranks) {
            return $this->respondOK($ranks);
        } else {
            return $this->respondBadRequest();
        }
    }

    public function reset()
    {
        \Artisan::call('db:seed', ['--class' => 'RanksTableSeeder']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
