<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ApiPasswordReset extends ResetPassword
{
    use Queueable;


    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(trans('notification.email.receive_mail'))
            ->action(trans('notification.email.reset_password'), env('PASSWORD_RESET_LINK', url('/password/reset/', $this->token)))
            ->line(trans('notification.email.incorrect_email'));
    }

}
