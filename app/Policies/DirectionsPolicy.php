<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Direction;
use Illuminate\Auth\Access\HandlesAuthorization;

class DirectionsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the direction.
     *
     * @param  \App\Models\User      $user
     * @param  \App\Models\Direction $direction
     *
     * @return mixed
     */
    public function view(User $user, Direction $direction): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || $user->id === $direction->user_id;
    }

    /**
     * Determine whether the user can create directions.
     *
     * @return mixed
     */
    public function create(): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the direction.
     *
     * @param  \App\Models\User      $user
     * @param  \App\Models\Direction $direction
     *
     * @return mixed
     */
    public function update(User $user, Direction $direction): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || $user->id === $direction->user_id;
    }

    /**
     * Determine whether the user can delete the direction.
     *
     * @param  \App\Models\User      $user
     * @param  \App\Models\Direction $direction
     *
     * @return mixed
     */
    public function delete(User $user, Direction $direction): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || $user->id === $direction->user_id;
    }
}
