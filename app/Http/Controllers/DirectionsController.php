<?php

namespace App\Http\Controllers;

use App\Events\RefreshTasksRanks;
use App\Gateways\DirectionGateway;
use App\Http\Requests\Direction\StoreDirectionRequest;
use App\Http\Requests\Direction\UpdateDirectionRequest;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DirectionsController extends Controller
{
    protected $gateway;

    /**
     * DirectionsController constructor.
     *
     * @param DirectionGateway $gateway
     */
    public function __construct(DirectionGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $directions = $this->gateway->getAll();

        if (!$directions)
            return $this->respondNotFound();
        else
            return $this->respondOk($directions['directions']);
    }

    /**
     * @param StoreDirectionRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreDirectionRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $direction = $this->gateway->create($data);
        if ($direction)
            return $this->respondCreated($direction, trans('common.directions.created'));
        else
            return $this->respondBadRequest(trans('common.directions.not_created'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $direction = $this->gateway->show($id);
        $this->authorize('view', $direction);

        if (!$direction)
            return $this->respondNotFound();
        else
            return $this->respondOk($direction);
    }

    /**
     * @param $directionId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjects($directionId): JsonResponse
    {
        $direction = $this->gateway->show($directionId);
        $this->authorize('view', $direction);

        if (!$direction)
            return $this->respondNotFound();
        else {
            if (isset($direction->projects)) {
                $data = $direction->projects()->withCount('tasks')->get()->toArray();

                return $this->respondOk($data);
            } else {
                return $this->respondOK();
            }
        }
    }

    /**
     * @param UpdateDirectionRequest $request
     * @param                        $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateDirectionRequest $request, $id): JsonResponse
    {
        $data['request'] = $request->all();
        $data['id'] = $id;

        $direction = $this->gateway->show($id);
        if ($direction && isset($request->priority) && $direction->priority != $request->priority) {
            $tasks = \Auth::user()->tasks;
            event(new RefreshTasksRanks($tasks));
        }

        $direction = $this->gateway->update($data);
        if ($direction)
            return $this->respondOK($direction, trans('common.directions.updated'));
        else
            return $this->respondBadRequest(trans('common.directions.not_updated'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $direction = $this->gateway->show($id);
        $this->authorize('delete', $direction);

        if ($this->gateway->delete($id))
            return $this->respondOK();
        else
            return $this->respondNotFound();
    }
}
