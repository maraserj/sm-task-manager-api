<?php

namespace App\Transformers;

use Carbon\Carbon;

class GoogleCalendarTransformer extends Transformer
{

    /**
     * @param $item
     *
     * @return mixed
     */
    public function transform($item)
    {
        $dateStart = Carbon::parse($item->start && isset($item->start['dateTime']) ? $item->start['dateTime'] : $item->start->date)->format('Y-m-d');
        $dateEnd = Carbon::parse($item->end && isset($item->end['dateTime']) ? $item->end['dateTime'] : $item->end->date)->format('Y-m-d');
        return [
            'id'            => $item->id,
            'name'          => $item->name ? $item->name : ($item->summary ? $item->summary : null),
            'startDateTime' => $dateStart,
            'endDateTime'   => $dateEnd,
        ];
    }
}