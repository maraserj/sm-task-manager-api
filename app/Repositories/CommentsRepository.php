<?php

namespace App\Repositories;

use App\Models\Comment;
use App\Models\Project;
use App\Models\Task;

class CommentsRepository extends BaseRepository
{
    protected $_fillable = [];
    protected $filesRepo;


    public function __construct(Comment $model, FilesRepository $filesRepository)
    {
        $this->filesRepo = $filesRepository;
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    public function allUsersComments($limit = 8, $user = null)
    {
        $response = [];
        if (!is_null($user)) {
            $projects = $user->projects()->pluck('project_id')->values();
            $response['projects'] = $this->model->whereIn('commentable_id', $projects)->limit($limit)->with(['user', 'commentable'])->orderBy('created_at', 'desc')->get();
            $tasks = $user->tasks()->pluck('id')->values();
            $response['tasks'] = $this->model->whereIn('commentable_id', $tasks)->limit($limit)->with(['user', 'commentable'])->orderBy('created_at', 'desc')->get();
        } else {
            $response = $this->model->with(['commentable' => function ($query) {
                $query->where();
            }])->with(['user'])->orderBy('created_at')->limit($limit)->get();
        }

        return $response;
    }

    public function getCreateRules()
    {
        return $this->model->getCreateRules();
    }

    public function getUpdateRules()
    {
        return $this->model->getUpdateRules();
    }

    public function create(array $data = [])
    {
        if ($data['target'] == 'project') {
            $target = Project::find($data['targetId']);
        } elseif ($data['target'] == 'task') {
            $target = Task::find($data['targetId']);
        } else {
            return null;
        }
        $data['user_id'] = auth()->id();

        $comment = $target->comments()->create(array_only($data, $this->_fillable));

        if (isset($data['files'])) {
            $attachments = $this->filesRepo->uploadCommentAttachments($data['files'], $comment->id);
            if (! empty($attachments)) {
                $comment = parent::update($comment->id, array_only(['attachments' => json_encode($attachments)], ['attachments']));
            }
        }

        return $comment;
    }

    public function update($id, array $data = [])
    {
        $data['user_id'] = $data['user_id'] ?? auth()->id();

        if (isset($data['files'])) {
            $attachments = $this->filesRepo->uploadCommentAttachments($data['files'], $id);
            if (! empty($attachments)) {
                $data['attachments'] = json_encode($attachments);
            }
        }
        return parent::update($id, array_only($data, ['user_id', 'text', 'attachments']));
    }

    public function delete($id)
    {
        $comment = $this->find($id);

        if ($comment) {

            $this->filesRepo->deleteAllCommentAttachments($id);

            if ($comment->delete())
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

}