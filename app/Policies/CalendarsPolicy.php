<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Calendar;
use Illuminate\Auth\Access\HandlesAuthorization;

class CalendarsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the calendar.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Calendar $calendar
     *
     * @return boolean
     */
    public function view(User $user, Calendar $calendar): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $calendar->user_id);
    }

    /**
     * Determine whether the user can create calendars.
     *
     * @return boolean
     */
    public function create(): bool
    {
        return true;
    }

    /**
     * Determine whether the user can update the calendar.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Calendar $calendar
     *
     * @return boolean
     */
    public function update(User $user, Calendar $calendar): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $calendar->user_id);
    }

    /**
     * Determine whether the user can delete the calendar.
     *
     * @param  \App\Models\User     $user
     * @param  \App\Models\Calendar $calendar
     *
     * @return boolean
     */
    public function delete(User $user, Calendar $calendar): bool
    {
        $isAdmin = $user->can('admin');

        return $isAdmin || ($user->id === $calendar->user_id);
    }
}
