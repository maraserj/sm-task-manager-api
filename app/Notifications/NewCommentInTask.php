<?php

namespace App\Notifications;

use App\Models\Task;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewCommentInTask extends Notification
{
    use Queueable;
    private $user;
    private $task;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param Task $task
     *
     * @internal param Project $project
     */
    public function __construct(User $user, Task $task)
    {
        $this->user = $user;
        $this->task = $task;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Новый комментарий в задаче: '.$this->task->title)
            ->greeting(__('notification.email.dear_user') . ', ' . $this->user->name)
            ->line(__('notification.email.new_comment_in_task').' - ')
            ->action($this->task->title, env('FRONTEND_URL', 'estate.intelweb.com.ua') . '/tasks/'.$this->task->id)
            ->line(__('notification.email.follow_link'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_name' => $this->user->name,
            'task_title' => $this->task->title,
            'task_id' => $this->task->id,
        ];
    }
}
