@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    {{--<passport-clients></passport-clients>--}}
                    {{--<passport-authorized-clients></passport-authorized-clients>--}}
                    {{--<passport-personal-access-tokens></passport-personal-access-tokens>--}}
                </div>
            </div>
        </div>
    </div>
@stop
@push('scripts')
    <script src="{{ asset('js/online-consultant.js') }}"></script>
@endpush