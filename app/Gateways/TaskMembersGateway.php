<?php
namespace App\Gateways;

use App\Repositories\TasksRepository;

class TaskMembersGateway extends Gateway
{
    protected $repository;
    protected $user;

    public function __construct(TasksRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll($taskId)
    {
        $members = $this->repository->getTaskMembers($taskId);

        return $members;
    }

    public function create(array $data = [], int $taskId)
    {
        $data['project_id'] = $taskId;
        $data['user_id'] = $data['user_id'] ?? auth()->id();
        $member = $this->repository->createTaskMember($data);

        return $member;
    }

    public function show(int $id, $user = null)
    {
        $member = $this->repository->getTaskMember($id);

        return $member;
    }

    public function delete($data)
    {
        $member = null;
        if (isset($data['user_id']) && isset($data['task_id'])) {
            $member = $this->repository->deleteTaskMember($data);
        }

        return $member;
    }
}