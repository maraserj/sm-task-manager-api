<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\CalendarEvent
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $calendar_id
 * @property string|null $description
 * @property \Carbon\Carbon|null $date
 * @property int|null $hours
 * @property string $available_hours
 * @property int $is_free
 * @property int $is_for_task
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Calendar $calendar
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereAvailableHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereIsForTask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereIsFree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CalendarEvent whereUserId($value)
 * @mixin \Eloquent
 */
class CalendarEvent extends Model
{
    protected $table = 'calendar_events';

    protected $fillable = [
        'user_id',
        'calendar_id',
        'description',
        'date',
        'hours',
        'available_hours',
        'is_free',
        'is_for_task',
    ];

    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function calendar(): HasOne
    {
        return $this->hasOne(Calendar::class, 'id', 'calendar_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks(): BelongsToMany
    {
        return $this->belongsToMany(Task::class, 'task_event', 'event_id')->withTimestamps();
    }

    /**
     * @param $date
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getEventsHoursByDate($date): Collection
    {
        return self::where([
            ['date', $date],
            ['is_for_task', true],
        ])->get(['is_for_task', 'hours']);
    }


    /**
     * @param $project
     * @return bool|null
     */
    public function deleteEventsByDate($project): bool
    {
        $tasksIds = Task::where('is_daily', 1)->pluck('id')->values();
        return self::where([
            ['date', '>=', Carbon::today()],
            ['date', '<=', $project->deadline],
            ['is_for_task', true],
            ['user_id', auth()->id()]
        ])->whereHas('tasks', function ($query) use ($project, $tasksIds) {
            $query->whereNotIn('task_id', $tasksIds);

            $query->whereHas('project', function ($q) use ($project) {
                $q->where('id', $project->id);
            });
        })->delete();
    }

    public function getWorkTimeEvents($deadline, $calendarId): array
    {
        return self::where([
            ['user_id', '=', auth()->id()],
            ['date', '>=', Carbon::today()],
            ['date', '<=', $deadline],
            ['is_for_task', '=', false],
            ['calendar_id', '=', $calendarId],
        ])->get(['id', 'date', 'hours', 'calendar_id'])->toArray();
    }

    public function getLatestUserEventDate()
    {
        $model = self::where([['user_id', '=', auth()->id()]])->latest('date')->first(['date']);
        if ($model) {
            $model->toArray();
        } else $model = null;

        return $model;
    }

    /**
     * @return array
     */
    public static function latestUserEventDate()
    {
        $event = self::where([['user_id', '=', auth()->id()]])->latest('date')->first(['date']);

        return isset($event) ? $event->toArray()['date'] : null;
    }
}
