<?php
/**
 * @var \Illuminate\Routing\Router $router
 */
Auth::routes();
$router->group(['middleware' => 'can:admin', 'prefix' => 'admin', 'as' => 'admin.'], function() use ($router) {
    $router->get('/', ['as' => 'index', 'uses' => 'Backend\MainController@index']);
    $router->resource('activities', 'Backend\ActivitiesController');
    $router->resource('directions', 'Backend\DirectionsController');
    $router->resource('users', 'Backend\UsersController');
    $router->resource('projects', 'ProjectsController');
});

$router->get('/', function () {
    return view('welcome');
});
$router->get('dd', function () {

    $text = '«Инструктора», стрелявшего в оператора Вячеслава Волка, во время командно-штабных учений в Кривом Роге выпустили под залог.

Информацию о внесении залога в 400 тысяч гривен редакции подтвердил прокурор Алексей Пустоваров. 

Напомним, что ранее судья Иван Быканов принял решение о том, что 60 суток под стражей проведет стрелок, ранивший оператора в Кривом Роге

Сейчас человек, тяжело ранивший Славу может практически свободно передвигаться по городу.

Все новости, связанные с ранением Вячеслава Волка читайте тут.

Кто именно внес такую сумму за подозреваемого не сообщается. 

Семья, друзья и коллеги Славы ошарашены такой новостью. 

На данный момент известно, что непосредственно от обвиняемого семье на лечение Славы было переданы в первый день 8 тысяч гривен. О передаче других денег семье Славы неизвестно.

«Весь город собирает по 50, 100 гривен на лечение Славы, а тут «бац» и 400 тысяч залог. Это как?», - говорит один из друзей тяжело раненного Славы.

На данный момент Вячеслав находится в стабильном, но тяжелом состоянии в больнице имени Мечникова в городе Днепр.

';
    $matched = [];
    foreach (explode(' ', $text) as $item) {
        if (strlen($item) < 4) continue;
        $matched[$item];
    }
    dd(strlen($text));
    dd(\App\Models\Task::groupBy('direction_id')->pluck('title'));
});

$router->get('/form', function () {
    return view('form');
});

