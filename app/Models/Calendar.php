<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Calendar
 *
 * @property int $id
 * @property int $isMain
 * @property string $name
 * @property int|null $type_id
 * @property int|null $user_id
 * @property int $priority
 * @property string|null $color
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CalendarEvent[] $events
 * @property-read \App\Models\CalendarType $type
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereIsMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Calendar whereUserId($value)
 * @mixin \Eloquent
 */
class Calendar extends Model
{
    protected $table = 'calendars';

    protected $fillable = [
        'isMain',
        'name',
        'type_id',
        'user_id',
        'priority',
        'color',
    ];

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return HasOne
     */
    public function type(): HasOne
    {
        return $this->hasOne(CalendarType::class, 'id', 'type_id');
    }

    /**
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(CalendarEvent::class, 'calendar_id', 'id');
    }

    public static function getMainCalendar($userId)
    {
        return self::where([['user_id', '=', $userId], ['isMain', '=', true]])->first();
    }
}
