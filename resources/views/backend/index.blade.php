@extends('backend.layouts.admin')

@section('content')



@endsection
@push('scripts')
    <script src="{{ asset('assets/plugins/interactjs/interact.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/moment/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('pages/js/pages.min.js') }}"></script>
    <script src="{{ asset('pages/js/pages.calendar.min.js') }}"></script>


    <script src="{{ asset('assets/js/calendar_month.js') }}" type="text/javascript"></script>
@endpush