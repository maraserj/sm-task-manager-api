<?php

namespace App\Http\Requests\CalendarEvent;

use App\Http\Requests\Request;
use App\Models\CalendarEvent;

class UpdateCalendarEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $event = CalendarEvent::find($this->route('calendar_event'));

        return $event && $this->user()->can('update', $event);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
