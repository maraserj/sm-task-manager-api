<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        if ($request->exists('all')) {
            $read = $this->user->readNotifications()->get();
            $unread = $this->user->unreadNotifications()->get();
        } elseif ($request->exists('limit')) {
            $read = $this->user->readNotifications()->limit($request->get('limit', 5))->get();
            $unread = $this->user->unreadNotifications()->limit($request->get('limit', 5))->get();
        } else {
            $read = $this->user->readNotifications()->paginate(10);
            $unread = $this->user->unreadNotifications;
        }

        return $this->respondOK(['read' => $read, 'unread' => $unread]);
    }

    public function update(Request $request, $id = null)
    {
        $request = $request->all();
        if (! empty($request['params']['id'])) {
            $notification = $this->user->notifications()->where('id', $request['params']['id'])->first();
            $notification->markAsRead();
            if ($notification->read()) {
                return $this->respondOK($notification);
            } else {
                return $this->respondBadRequest();
            }
        } else {
            return $this->respondNotFound();
        }
    }

    public function destroy($id)
    {
        //
    }
}
