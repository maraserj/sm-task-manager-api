<?php

namespace App\Repositories;

use App\Models\Rank;

class RanksRepository extends BaseRepository
{
    protected $_fillable = [];

    public function __construct(Rank $model)
    {
        $this->_fillable = $model->getFillable();
        parent::__construct($model);
    }

    public function all()
    {
        return $this->model->orderBy('total_rank')->get();
    }

    public function updateRanks(array $data = [])
    {
        $currentRank = $this->model->where('total_rank', $data['current']['rank'])->first();
        if (!isset($data['target']['id'])) {
            $targetRank = $this->model->orderBy('total_rank', 'DESC')->latest()->first();
        } else {
            $targetRank = $this->model->where('total_rank', $data['target']['rank'])->first();
        }
        $totalTargetRank = $targetRank->total_rank;
        $ranksArray = [];
        if ($currentRank && $targetRank) {
            if ($currentRank->total_rank - 1 === $targetRank->total_rank || $currentRank->total_rank + 1 === $targetRank->total_rank) {
                $currentRank->total_rank = $targetRank->total_rank;
                $targetRank->total_rank = $data['current']['rank'];
            } else {
                if ($currentRank->total_rank <= $targetRank->total_rank) {
                    // снизу вверх

                    for ($i = $currentRank->total_rank + 1; $i <= $targetRank->total_rank; $i++) {
                        $ranksArray[] = $i;
                    }
                    $rangeRanks = $this->model->whereIn('total_rank', $ranksArray)->get();
                    $currentRank->total_rank = $totalTargetRank;
                    $currentRank->save();

                    foreach ($rangeRanks as $rank) {
                        $rank->total_rank = $rank->total_rank - 1;
                        $rank->save();
                    }
                } else if ($currentRank->total_rank >= $targetRank->total_rank) {
                    // свверху вниз

                    for ($i = $targetRank->total_rank; $i < $currentRank->total_rank; $i++) {
                        $ranksArray[] = $i;
                    }
                    $rangeRanks = $this->model->whereIn('total_rank', $ranksArray)->get();
                    $currentRank->total_rank = $totalTargetRank;
                    $currentRank->save();
                    foreach ($rangeRanks as $rank) {
                        $rank->total_rank = $rank->total_rank + 1;
                        $rank->save();
                    }
                }
            }
        }
        $currentRank->save();
        $targetRank->save();

        return true;

    }


}