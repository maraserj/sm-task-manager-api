<?php

namespace App\Notifications;

use App\Models\Project;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserExitFromProject extends Notification
{
    use Queueable;
    private $user;
    private $exitedUser;
    private $project;

    /**
     * Create a new notification instance.
     *
     * @param User    $user
     * @param User    $exitedUser
     * @param Project $project
     */
    public function __construct(User $user, User $exitedUser, Project $project)
    {
        $this->user = $user;
        $this->exitedUser = $exitedUser;
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting(__('notification.email.dear_user') . ', ' . $this->user->name)
            ->line(__('notification.email.user_exit_from_project', ['username' => $this->exitedUser->name, 'projectname' => $this->project->name]).' - ')
            ->action($this->project->name, env('FRONTEND_URL', 'estate.intelweb.com.ua') . '/projects/'.$this->project->id.'/show')
            ->line(__('notification.email.follow_link'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_name' => $this->user->name,
            'project_name' => $this->project->name,
            'project_id' => $this->project->id,
        ];
    }
}
