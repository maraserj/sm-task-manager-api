<?php

namespace App\Gateways;

use App\Repositories\RanksRepository;

class RanksGateway
{
    protected $repository;

    public function __construct(RanksRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->repository->all();
    }

    public function create(array $data = [])
    {
        //
    }

    public function show(int $id)
    {
        //
    }

    public function updateRanks(array $data = [])
    {
        if (isset($data['current']) && isset($data['target'])) {
            return $this->repository->updateRanks($data);
        } else {
            return null;
        }
    }

    public function delete(int $id)
    {
        //
    }

}