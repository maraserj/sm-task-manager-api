<?php

namespace App\Http\Requests\Task;

use App\Http\Requests\Request;
use App\Models\CalendarEvent;
use App\Models\Direction;
use App\Models\Project;
use Carbon\Carbon;

class StoreTaskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getValidatorInstance()
    {
        $calendarEvent = new CalendarEvent();
        $data = $this->getInputSource()->all();

        $estimateForTask = null;
        $workTime = null;
        $minPart = null;

        $projectDeadlineValidation = 0;
        $data['current_day_validation'] = 1;
        $data['hours_sum'] = 0; // сумма часов для тасков
        $data['work_time'] = 0; // рабочее время
        $data['last_work_day'] = 0; // последний рабочий день
        $data['task_validation'] = 0;
        $data['task_date_validation'] = 0;
        $data['task_have_big_size_part'] = 0;
        $data['too_big_estimate'] = 1;
        $data['deadline'] = $data['deadline'] ?? null;
        $data['project_deadline_date'] = null;
        if (!isset($data['project_id'])) {
            $this->getInputSource()->replace($data);
            return parent::getValidatorInstance();
        }
        $project = Project::with('tasks')->find($data['project_id']);
        $calendarId = $project->activity->calendar_id;

        if (isset($data['is_daily'])) { // если таск для сегодняшнего дня
            $projectDeadlineValidation = 1;
            $data['hours_sum'] = 1;
            $data['work_time'] = 1;
            $data['last_work_day'] = 1;
            $data['task_validation'] = 1;
            $data['task_date_validation'] = 1;
            $data['task_have_big_size_part'] = 1;
            $data['too_big_estimate'] = 1;
            $data['deadline'] = $data['deadline'] ?? null;
            $data['project_deadline_date'] = null;
            $freeTimeOfDay = $calendarEvent->getWorkTimeEvents($data['deadline'], $calendarId);
            $workTimeInDay = collect($freeTimeOfDay)->pluck('hours')->sum();
            $freeTimeForWork = $workTimeInDay - $data['estimate'] ;
            if ($freeTimeForWork < 0) {
                $data['current_day_validation'] = 0;
            }
            $this->getInputSource()->replace($data);

            return parent::getValidatorInstance();
        }

        if (isset($data['estimate']) && isset($data['project_id'])) {
            $data['project_deadline_date'] = Carbon::parse($project->deadline)->format('Y-m-d');
            $data['deadline'] = isset($data['deadline']) ? str_replace('"', '', Carbon::parse($data['deadline'])) : ($project->deadline ? str_replace('"', '', Carbon::parse($data['deadline']))->addDays(-1) : Carbon::parse($project->parentProject->deadline)->addDays(-1));
            $tasksCollection = collect($project->tasks)->pluck('estimate')->all();
            $sum = collect($tasksCollection)->sum();

            $events = $calendarEvent->getWorkTimeEvents($data['deadline'], $calendarId);
            $eventsCollection = collect($events)->pluck('hours')->all();
            $lastWorkDay = $calendarEvent->getLatestUserEventDate();
            $activity_id = Direction::find($project->direction_id)->activity_id;


            $data['work_time'] = $workTime = collect($eventsCollection)->sum();
            $data['activity_id'] = $activity_id;
            $data['direction_id'] = $project->direction_id;
            $data['last_work_day'] = Carbon::parse($lastWorkDay['date'])->format('Y-m-d');
            $data['hours_sum'] = round($sum + $data['estimate']);
            $data['author_id'] = $data['author_id'] ?? auth()->id();
            $data['priority'] = $data['priority'] ?? 0;

            if ($data['deadline'] < $lastWorkDay['date'])
                $data['task_validation'] = 1;
            if (str_replace('"', '', Carbon::parse($data['deadline'])) > Carbon::now())
                $data['task_date_validation'] = 1;
            if ($project && $project->deadline) {
                if (Carbon::parse($project->deadline) >= $data['deadline']) {
                    $projectDeadlineValidation = 1;
                }
            } elseif ($project->parentProject && $project->parentProject->deadline) {
                if (Carbon::parse($project->parentProject->deadline) >= $data['deadline']) {
                    $projectDeadlineValidation = 1;
                }
            }
            if ($data['estimate'] < $sum)
                $data['task_validation'] = 1;
            if (isset($data['min_parts_for_task']))
                $minPart = $data['min_parts_for_task'];
            if (!isset($data['parent_id']) || $data['parent_id'] == '')
                $data['parent_id'] = 0;
            $data['old_estimate'] = $data['estimate'];

            foreach ($events as $event) {
                $taskEventFromCalendar = $calendarEvent->getEventsHoursByDate($event['date']);
                $tasksHoursInDay = collect($taskEventFromCalendar)->pluck('hours')->sum();
                $freeTimeForWork = $event['hours'] - $tasksHoursInDay;
                if ($freeTimeForWork <= 0) continue;
                if ($freeTimeForWork < $minPart) continue;

                $data['old_estimate'] = $data['old_estimate'] - $minPart;
                if ($data['old_estimate'] <= 0) break;
                if ($freeTimeForWork - $minPart >= $data['old_estimate']) continue;

//                if ($data['estimate'] > $event['hours'] && $data['min_parts_for_task'] > $event['hours']) continue;
//                else $data['too_big_estimate'] = 1;

            }
            if ($data['old_estimate'] <= 0) $data['task_have_big_size_part'] = 1;
            else $data['task_have_big_size_part'] = 0;

        } else $sum = null;

        $data['project_deadline'] = $projectDeadlineValidation;
        $this->getInputSource()->replace($data);

        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = $this->getInputSource()->all();


        $data['deadline'] = $data['deadline'] ?? null;

        return [
            'project_id'              => 'bail|required|numeric',
            'title'                   => 'required',
            'parent_id'               => 'numeric',
            'priority'                => 'required|numeric|not_in:0',
            'estimate'                => 'required',
            'deadline'                => 'date',
//            'date_start'              => 'nullable|after_or_equal:'.Carbon::today()->addDays(1).'|before:'.(isset($data['deadline']) ? Carbon::parse($data['deadline'])->addDays(-1)->format('Y-m-d') :  Carbon::today()->addDays(-1)),
            'description'             => 'string|nullable',
            'hours_sum'               => 'numeric|between:0,' . (isset($data['work_time']) ? $data['work_time'] : 1),
            'project_deadline'        => 'not_in:0',
            'task_validation'         => 'not_in:0',
            'task_date_validation'    => 'not_in:0',
            'task_have_big_size_part' => 'not_in:0',
            'too_big_estimate'        => 'not_in:0',
            'current_day_validation'  => 'not_in:0',
        ];
    }

    public function messages()
    {
        $data = $this->getInputSource()->all();

        return [
            'current_day_validation.not_in'  => trans('validation.custom.current_day_validation.not_in'),
            'hours_sum.between'      => trans('validation.custom.hours_sum.between', ['hours_sum' => isset($data['hours_sum']) ? $data['hours_sum'] : 0, 'work_time' => isset($data['work_time']) ? $data['work_time'] : 0]),
            'task_validation.not_in' => trans('validation.custom.task_validation.not_in', ['deadline' => isset($data['deadline']) ? $data['deadline'] : 0, 'last_work_day' => isset($data['last_work_day']) ? $data['last_work_day'] : 0]),
            'project_deadline.not_in' => trans('validation.custom.project_deadline.not_in', ['deadline' => isset($data['project_deadline_date']) ? $data['project_deadline_date'] : 0]),
        ];
    }


}
