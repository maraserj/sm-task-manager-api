<?php

return [
    "email" => [
        "receive_mail"           => "Вы получили это письмо, потому что мы получили запрос на изменение пароля для учетной записи.",
        "reset_password"         => "Сброс пароля",
        "incorrect_email"        => "Если вы не отправляли запрос на изменение пароля, не требуется никаких дополнительных действий.",
        "dear_user"              => "Уважаемый",
        "invite"                 => "Вас пригласили в проект",
        "invited"                => "Теперь вы являетесь участником данного проекта.",
        "task_assigned"          => "Пользователь приступил к выполнению",
        "task_created"           => "Пользователь :username создал новую задачу в проекте :projectname",
        "user_closed_task"       => "Пользователь :username закрыл задачу в проекте :projectname",
        "user_finished_task"     => "Пользователь :username завершил выполение задачи в проекте :projectname",
        "user_reopen_task"       => "Пользователь :username снова открыл задачу в проекте :projectname",
        "user_started_task"      => "Пользователь :username начал выполнять задачу в проекте :projectname",
        "new_comment_in_project" => "В проекте новый комментарий",
        "new_comment_in_task"    => "В задаче новый комментарий",
        "follow_link"            => "Перейдите по ссылке, для просмотра",

        "user_exit_from_project" => "Пользователь :username, вышел из проекта: :projectname"
    ],
];