<?php

namespace App\Listeners;

use App\Events\RefreshTasksRanks;
use App\Repositories\TasksRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshTasksRanksListener
{
    /**
     * @var TasksRepository
     */
    private $tasksRepository;

    /**
     * Create the event listener.
     *
     * @param TasksRepository $tasksRepository
     */
    public function __construct(TasksRepository $tasksRepository)
    {
        $this->tasksRepository = $tasksRepository;
    }

    /**
     * Handle the event.
     *
     * @param  RefreshTasksRanks $event
     *
     * @return void
     */
    public function handle(RefreshTasksRanks $event)
    {
        if ($event->tasks) {
            $i = 1;
            $tasks = $event->tasks->sortByDesc('rank')->all();

            foreach ($tasks as $task) {
                $update = ['order' => $i];
                $update['rank'] = $this->tasksRepository->setTaskRank($task->toArray());
                $task->update($update);
                $i++;
            }
        }
    }
}
