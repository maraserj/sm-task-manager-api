<?php
namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Repositories\UsersRepository;
use App\Traits\ApiResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Bridge\UserRepository;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $user;

    public function __construct(UsersRepository $user)
    {
        $this->user = $user;
    }

    public function socialAuthByProvider(Request $request, $provider)
    {
        $user = null;
        $response = [];
        if ($request->email) {
            $user = $this->user->findByEmail($request->email);

            $avatar = '';
            if (isset($request->picture) && (isset($request->picture->data) && isset($request->picture->data->url))) {
                $avatar = $request->picture->data->url;
            } else if (isset($request->picture) && (isset($request->picture['data']) && isset($request->picture['data']['url']))) {
                $avatar = $request->picture['data']['url'];
            }
            if ($user) {
                $response['new_user'] = false;
            } else {
                $response['new_user'] = true;

                $user = $this->user->create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt('qwerty'),
                ]);
            }
            $user->info()->update([$provider. '_id' => $request->id]);
            if ($user) {
                $uploadedAvatar = $this->user->uploadAvatar($user, $avatar);
                if ($uploadedAvatar) {
                    if ($user->avatar === '' || !$user->avatar) {
                        $user->update(['avatar' => $uploadedAvatar]);
                    }
                }
            }
            $token = $user->createToken("Token, {$provider} login", ['*'])->accessToken;
            $response['access_token'] = $token;
            $response['user'] = $user;
        }

        return $this->respondOK($response);
    }

    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback()
    {
        $user = Socialite::driver('facebook')->stateless()->user();
        if ($user) {
            $grantedUser = User::whereEmail($user->getEmail())->first();
            if ($grantedUser) {
                auth()->login($grantedUser);
            } else {
                $newUser = User::create([
                    'name'     => $user->getName(),
                    'email'    => $user->getEmail(),
                    'password' => bcrypt(str_random()),
                ]);
                auth()->login($newUser);
            }
        }
    }

    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        if ($user) {
            $grantedUser = User::whereEmail($user->getEmail())->first();
            if ($grantedUser) {
                auth()->login($grantedUser);
            } else {
                $newUser = User::create([
                    'name'     => $user->getName(),
                    'email'    => $user->getEmail(),
                    'password' => bcrypt(str_random()),
                ]);
                auth()->login($newUser);
            }
        }
    }
}
