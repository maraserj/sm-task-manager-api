<?php

namespace App\Gateways;

use App\Events\CreatingUserWidgetsEvent;
use App\Repositories\{ FilesRepository, UsersRepository };
use App\Transformers\UserTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\{
    Collection as FractalCollection,
    Item as FractalItem
};

class UserGateway extends Gateway
{
    protected $repository;
    protected $filesRepository;
    protected $transformer;
    protected $fractal;
    protected $fractalCollection;
    protected $fractalItem;

    public function __construct(UsersRepository $repository,FilesRepository $filesRepository, UserTransformer $transformer, FractalCollection $fractalCollection, FractalItem $fractalItem)
    {
        $this->repository = $repository;
        $this->filesRepository = $filesRepository;
        $this->fractalCollection = $fractalCollection;
        $this->transformer = $transformer;
        $this->fractalItem = $fractalItem;
        $this->fractal = new Manager();
    }

    public function index($request)
    {
        if (isset($request['for_project_member']) || isset($request['for_task_member'])) {
            return $this->repository->all();
        }

        if (isset($request['with_count']) && isset($request['id'])) {
            $user = $this->repository->with(['info'])->withCount(['calendars','projects','tasks','activities','directions', 'comments'])->whereId($request['id'])->first();
        } else {
            $user = $this->repository->with(['info'])->whereId(auth()->id())->first();
        }

//        $users = $this->repository->all();
//        $data = new $this->fractalCollection($users, new UserTransformer);
//        $this->fractal->parseIncludes(['projects', 'tasks']);
//        return $this->respondOk($this->fractal->createData($data)->toArray());
        return $user;
    }

    public function getAvailable($data)
    {
        $response = null;

        if (isset($data['for_project_member']) || isset($data['for_task_member'])) {
            if (isset($data['target'])) {
                if ($data['target'] =='task') {
                    $response = $this->repository->getAvailableForTask($data['target_id']);
                }
                if ($data['target'] =='project') {
                    $response = $this->repository->getAvailableForProject($data['target_id'], isset($data['email']) ? $data['email'] : null);
                }
            }
        } else {
            $response = $this->repository->with(['info'])->whereId(auth()->id())->first();
        }

        return $response;
    }

    public function getAll()
    {
        return $this->repository->all();
    }

    public function show($id)
    {
        $user = $this->repository->find($id);
        $data = new $this->fractalItem($user, $this->transformer);

        return $data;
    }

    public function validateOnCreate($data)
    {
        return $this->repository->validateOnCreate($data);
    }

    public function validateOnUpdate($id, $data)
    {
        return $this->repository->validateOnUpdate($id, $data);
    }

    public function create(array $data = [])
    {
        $user = $this->repository->create($data);
        return $user;
    }

    public function update(array $data = [])
    {
        $user = $this->repository->update($data['id'], $data);

        return $user;
    }

    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }

    public function validateOnAvatarUpload($data)
    {
        return $this->repository->validateOnAvatarUpload($data);
    }

    /**
     * @param $file
     * @param $user
     *
     * @return bool|null|string
     */
    public function uploadAvatar($file, $user)
    {
        $avatarPath = $this->filesRepository->uploadUserAvatar($user->email, $file);

        if ($avatarPath) {
            $user->avatar = $avatarPath;
            if ($user->save()) {
                return $avatarPath;
            } else {
                return null;
            }
        } else
            return null;
    }

}