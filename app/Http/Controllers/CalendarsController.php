<?php

namespace App\Http\Controllers;

use App\Events\RefreshAllUserTasks;
use App\Gateways\CalendarGateway;
use App\Http\Requests\Calendar\StoreCalendarRequest;
use App\Http\Requests\Calendar\UpdateCalendarRequest;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;

class CalendarsController extends Controller
{
    protected $gateway;
    private $user;

    /**
     * CalendarsController constructor.
     *
     * @param CalendarGateway $gateway
     */
    public function __construct(CalendarGateway $gateway)
    {
        $this->middleware(function ($request, $next) {
            $this->user = \Auth::user();

            return $next($request);
        });
        $this->gateway = $gateway;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->gateway->getAll();

        if (!$data)
            return $this->respondNotFound();
        else
            return $this->respondOk($data);
    }

    /**
     * @param StoreCalendarRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCalendarRequest $request): JsonResponse
    {
        $data = $request->except(['_token', '_method']);

        $calendar = $this->gateway->create($data);
        if ($calendar)
            return $this->respondCreated($calendar, trans('common.calendars.created'));
        else
            return $this->respondBadRequest(trans('common.calendars.not_created'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        $calendar = $this->gateway->show($id);
        $this->authorize('view', $calendar);

        if (!$calendar)
            return $this->respondNotFound();
        else
            return $this->respondOk($calendar);
    }

    /**
     * @param UpdateCalendarRequest $request
     * @param                       $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateCalendarRequest $request, $id): JsonResponse
    {
        $data['request'] = $request->all();
        $data['id'] = $id;

        $calendar = $this->gateway->update($data);
        if ($calendar)
            return $this->respondOK($calendar, trans('common.calendars.updated'));
        else
            return $this->respondBadRequest(trans('common.calendars.not_updated'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $data = $this->gateway->show($id);
        $this->authorize('delete', $data);

        if ($data->isMain == 1)
            return $this->respondLocked();

        if ($this->gateway->delete($id)) {
            event(new RefreshAllUserTasks($this->user));
            return $this->respondOK(trans('common.calendars.deleted'));
        } else
            return $this->respondNotFound();
    }
}
