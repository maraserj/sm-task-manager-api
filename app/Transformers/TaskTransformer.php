<?php

namespace App\Transformers;

use App\Models\Task;
use Carbon\Carbon;
use Fractal;

class TaskTransformer extends Transformer
{

    public function transform(Task $task)
    {
        Carbon::setLocale('ru');

        return [
            'title' => $task->key,
            'color' => $task->project->color ?? '#eaeaea',

            'id' => (int) $task->id,
            'key' => $task->key,
            'activity_id' => $task->activity->name,
            'direction_id' => $task->direction->name,
            'project_id' => $task->project->name,
            'parent_id' => $task->parent_id,
            'assigned_id' => isset($task->assigned_id) ? $task->assigned->email : null,
            'author_id' => $task->author->name,
            'description' => $task->description,
            'estimate' => (int) $task->estimate,
            'priority' => (int) $task->priority,
            'deadline' => isset($task->deadline) ? $task->deadline->diffForHumans() : null,
            'status' => (int) $task->status,
            'min_parts_for_task' => (int) $task->min_parts_for_task,
            'max_parts_for_task' => (int) $task->max_parts_for_task,
            'start_at' => ($task->start_at ? $task->start_at->diffForHumans() : null) ?? null,
            'end_at' => ($task->end_at ? $task->end_at->diffForHumans() : null) ?? null,
            'created_at' => $task->created_at->diffForHumans(),
        ];
    }
}