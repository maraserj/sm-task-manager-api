<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ranks', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('calendar_priority');
            $table->tinyInteger('activity_priority');
            $table->tinyInteger('direction_priority');
            $table->tinyInteger('project_priority');
            $table->tinyInteger('task_priority');
            $table->integer('total_rank');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ranks');
    }
}
