<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
// use Illuminate\Database\Eloquent\Relations\HasOne;

class UserWidget extends Model
{
    protected $table = 'user_widgets';

    protected $fillable = [
        'user_id',
        'widget_id',
        'selected'
    ];

    public function widgets(): HasMany
    {
        return $this->hasMany(Widget::class,  'id', 'widget_id');
    }

}
