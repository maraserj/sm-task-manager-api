<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserInfo
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $sex
 * @property string|null $birthday
 * @property string|null $main_language
 * @property string|null $languages
 * @property string|null $country
 * @property string|null $state
 * @property string|null $city
 * @property string|null $address
 * @property string|null $work_days
 * @property string|null $google_calendar_id
 * @property string|null $google_id
 * @property string|null $facebook_id
 * @property string|null $twitter_id
 * @property string|null $instagram_id
 * @property string|null $youtube_id
 * @property string|null $vk_id
 * @property string|null $yandex_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereFacebookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereGoogleCalendarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereGoogleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereInstagramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereLanguages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereMainLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereTwitterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereVkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereWorkDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereYandexId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserInfo whereYoutubeId($value)
 * @mixin \Eloquent
 */
class UserInfo extends Model
{
    protected $fillable = [
        'user_id',
        'sex',
        'birthday',
        'main_language',
        'languages',
        'country',
        'state',
        'city',
        'address',
        'work_days',
        'google_calendar_id',
        'google_id',
        'facebook_id',
        'twitter_id',
        'instagram_id',
        'youtube_id',
        'vk_id',
        'yandex_id',
    ];

}
