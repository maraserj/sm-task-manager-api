<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * App\Models\Project
 *
 * @property int $id
 * @property int $parent_id
 * @property int|null $activity_id
 * @property int|null $direction_id
 * @property int|null $project_lead
 * @property string|null $name
 * @property string|null $description
 * @property \Carbon\Carbon|null $deadline
 * @property \Carbon\Carbon|null $start_at
 * @property \Carbon\Carbon|null $end_at
 * @property string $status
 * @property int|null $priority
 * @property string|null $color
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Activity $activity
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attachment[] $attachments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read \App\Models\Direction $direction
 * @property-read \App\Models\User $lead
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $members
 * @property-read \App\Models\Project $parentProject
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereActivityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereDirectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereProjectLead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = [
        'parent_id',
        'name',
        'activity_id',
        'direction_id',
        'project_lead',
        'description',
        'deadline',
        'status',
        'priority',
        'color',
        'end_at',
        'start_at',
    ];

    protected $dates = [
        'deadline',
        'created_at',
        'updated_at',
        'end_at',
        'start_at',
    ];

    private $createRules = [
        'parent_id'    => 'numeric',
        'name'         => 'required',
        'direction_id' => 'required|numeric',
        'deadline'     => 'required|date',
        'description'  => 'string|nullable',
    ];

    private $updateRules = [
        'parent_id'    => 'numeric',
        'name'         => 'required',
        'direction_id' => 'required|numeric',
        'deadline'     => 'required|date',
        'description'  => 'string|nullable',
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }


    /**
     * @return array
     */
    public function getCreateRules(): array
    {
        return $this->createRules;
    }

    /**
     * @return array
     */
    public function getUpdateRules(): array
    {
        return $this->updateRules;
    }


    /**
     * @param array $attributes
     */
    public function setCreateRules(array $attributes = [])
    {
        foreach ($attributes as $key => $attribute) {
           $this->createRules[$key] = $attribute;
        }
    }

    /**
     * @param array $attributes
     */
    public function setUpdateRules(array $attributes = [])
    {
        foreach ($attributes as $key => $attribute) {
            $this->updateRules[$key] = $attribute;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lead(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'project_lead');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activity(): HasOne
    {
        return $this->hasOne(Activity::class, 'id', 'activity_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function direction(): HasOne
    {
        return $this->hasOne(Direction::class, 'id', 'direction_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parentProject(): HasOne
    {
        return $this->hasOne(Project::class, 'id', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'project_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'project_members', 'project_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function attachments(): MorphMany
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(Project::class, 'parent_id', 'id');
    }


    /**
     * @param $id
     * @return array|Model|null|\stdClass|static
     */
    public static function getProjectWithEventsAndTasks($id)
    {
        return self::with(['tasks'])->whereId($id)->first();
    }
}
